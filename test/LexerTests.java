package test;

import org.norecess.antlr.LexerTestCase;
import java.io.*;
import java.util.Iterator;

import antlr.TokenStream;
import antlr.TokenStreamException;
import src.*;

public class LexerTests extends LexerTestCase implements ScalaLexerTokenTypes {

	@Override
	protected TokenStream makeLexer(String input) {
		System.out.println(input);
		return new ScalaLexer(new StringReader(input));
	}
	
	
	/*
	public void testnonzeroDigitToken() throws TokenStreamException {
		for(int i = 1; i<10; i++){
			assertToken(NONZERODIGIT, i+"", i+"");
		}				
	}
	
	public void testdigitsToken() throws TokenStreamException {
		for(int i = 0; i<10; i++){
			assertToken(DIGIT, i+"", i+"");
		}				
	}
	
	
	public void testDecimalToken() throws TokenStreamException {
		
		//single char check
		//conflicts with the octal for 0
		for(int i = 0; i<10; i++){
			assertToken(DECIMAL, i+"", i+"");
		}
		
		//for integers starting with a one
		for(int i = 1; i<10; i++){
			for(int j = 0; j<10; j++){
				assertToken(DECIMAL, i+""+j, i+""+j);
			}		
		}
	}
	
	public void testoctalToken() throws TokenStreamException {
		for (int i = 0; i < 8; i++) {
			assertToken(OCTAL, "0"+i, "0"+i);
		}
	}
	
	
	public void testhexdigitToken() throws TokenStreamException {		
		assertToken(HEX, "0x2A", "0x2A");				
	}
	
	
	*/
	
	/*
	public void testintliteralToken() throws TokenStreamException {
		assertToken(INTLITERAL, "125l", "125l");
		assertToken(INTLITERAL, "00", "00");
		assertToken(INTLITERAL, "0xAB", "0xAB");		
		assertToken(INTLITERAL, "0xABL", "0xABL");
	}
	*/
	/*
	public void testexponentpartToken() throws TokenStreamException {
		assertToken(EXP, "E+10");
		assertToken(EXP, "E-10");
		assertToken(EXP, "e+10");
		assertToken(EXP, "e-10");
	}
	*/
	
	
	public void testNumberliteralToken() throws TokenStreamException {
		assertToken(NUMBERLITERAL, "124.0", "124.0");
		assertToken(NUMBERLITERAL, "123E20", "123E20");
		assertToken(NUMBERLITERAL, ".1", ".1");
		assertToken(NUMBERLITERAL, "0.1E20", "0.1E20");
		assertToken(NUMBERLITERAL, "0.1E20F", "0.1E20F");
		assertToken(NUMBERLITERAL, "2F", "2F");
		assertToken(NUMBERLITERAL, "2.F", "2.F");
		assertToken(NUMBERLITERAL, "123", "123");
		assertToken(NUMBERLITERAL, "0123", "0123");
		assertToken(NUMBERLITERAL, "2.5", "2.5");
		assertToken(NUMBERLITERAL, "25L", "25L");
		assertToken(NUMBERLITERAL, "0xFE", "0xFE");
		assertToken(NUMBERLITERAL, "0x12", "0x12");
		assertToken(NUMBERLITERAL, "1.0e-100", "1.0e-100");
	}
	
	public void testBoolLiteralToken() throws TokenStreamException {
		assertToken(BOOLLIT, "true", "true");
		assertToken(BOOLLIT, "false", "false");
	}
	
	/*
	public void testIdToken() throws TokenStreamException {
		assertToken(PLAINID, "big_bob", "big_bob");
		assertToken(PLAINID, "big_bob++=", "big_bob++=");
		assertToken(PLAINID, "x", "x");
		assertToken(PLAINID, "Object", "Object");
		assertToken(PLAINID, "αρετηλ", "αρετηλ");
		assertToken(PLAINID, "maxIndex", "maxIndex");
		assertToken(PLAINID, "p2p", "p2p");
		assertToken(PLAINID, "empty_?", "empty_?");
		assertToken(PLAINID, "__y", "__y");
		assertToken(PLAINID, "+", "+");
		assertToken(PLAINID, "dot_product_*", "dot_product_*");
		assertToken(PLAINID, "_system", "_system");
		assertToken(PLAINID, "_MAX_LEN_", "_MAX_LEN_");
		
	}
	*/
	/*
	public void testCharEscSeqToken() throws TokenStreamException {
		assertToken(CHARESCSEQ, "\\n", "\\n");
		assertToken(CHARESCSEQ, "\\t", "\\t");
		assertToken(CHARESCSEQ, "\\f", "\\f");
		assertToken(CHARESCSEQ, "\\b", "\\b");
		assertToken(CHARESCSEQ, "\\'", "\\'");
		assertToken(CHARESCSEQ, "\\r", "\\r");
		assertToken(CHARESCSEQ, "\\\"", "\\\"");
		assertToken(CHARESCSEQ, "\\\\", "\\\\");
	}*/
	
	public void testCharLiteralToken() throws TokenStreamException {
		assertToken(CHARLIT, "'a'", "'a'");
		assertToken(CHARLIT, "'\u0041'", "'\u0041'");
		assertToken(CHARLIT, "'\\t'", "'\\t'");
		assertToken(CHARLIT, "'\\r'", "'\\r'");
	}
	
	public void testStringLiteralToken() throws TokenStreamException {
		assertToken(STRINGLIT, "\"My Name is Khan\"","\"My Name is Khan\"" );
		assertToken(STRINGLIT, "\"\"", "\"\"");
		assertToken(STRINGLIT, "\"->\"", "\"->\"");
		assertToken(STRINGLIT, "\"\\\\\"", "\"\\\\\"");
		assertToken(STRINGLIT, "\"He said, \\\"I want to be writer!\\\"\"", "\"He said, \\\"I want to be writer!\\\"\"");
		assertToken(STRINGLIT, "\"\\\"\"", "\"\\\"\"");
	}
	
	/*
	public void testSymbolLiteralToken() throws TokenStreamException {
		assertToken(SYMBOLLIT, "\'jamil", "\'jamil");
	}*/
	
	public void testSingleLineCommentToken() throws TokenStreamException {
		assertToken(SLCOMMENT, "//adfadsf", "//adfadsf");
	}
	
	public void testMultiLineCommentToken() throws TokenStreamException {
		assertToken(MLCOMMENT, "/*adf**a/*dsf*/", "/*adf**a/*dsf*/");
	}
	
	public void testLbrace() throws TokenStreamException {
		assertToken(LBRACE, "{", "{");
		assertToken(HASH, "#", "#");
	}
	
	
	
}
