# Scala Lexer

1. You need to have antlr testin Jar on the path
	* Get it from [HERE][1] 
2. You need Junit 3.8.1 Jar on the Path
	* Get it form [HERE][2]

[1]: http://ignum.dl.sourceforge.net/project/antlr-testing/antlr-testing/0.6/antlrtesting-0.6.jar 
[2]: http://mirrors.ibiblio.org/maven2/junit/junit/3.8.1/junit-3.8.1.jar
