// $ANTLR : "gram.g" -> "P.java"$
package src; 
import antlr.TokenBuffer;
import antlr.TokenStreamException;
import antlr.TokenStreamIOException;
import antlr.ANTLRException;
import antlr.LLkParser;
import antlr.Token;
import antlr.TokenStream;
import antlr.RecognitionException;
import antlr.NoViableAltException;
import antlr.MismatchedTokenException;
import antlr.SemanticException;
import antlr.ParserSharedInputState;
import antlr.collections.impl.BitSet;

public class P extends antlr.LLkParser       implements ScalaLexerTokenTypes
 {

protected P(TokenBuffer tokenBuf, int k) {
  super(tokenBuf,k);
  tokenNames = _tokenNames;
}

public P(TokenBuffer tokenBuf) {
  this(tokenBuf,1);
}

protected P(TokenStream lexer, int k) {
  super(lexer,k);
  tokenNames = _tokenNames;
}

public P(TokenStream lexer) {
  this(lexer,1);
}

public P(ParserSharedInputState state) {
  super(state,1);
  tokenNames = _tokenNames;
}

	public final void startRule() throws RecognitionException, TokenStreamException {
		
		Token  n = null;
		
		try {      // for error handling
			n = LT(1);
			match(INT);
			System.out.println("Hi There, " + n.getText());
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_0);
		}
	}
	
	
	public static final String[] _tokenNames = {
		"<0>",
		"EOF",
		"<2>",
		"NULL_TREE_LOOKAHEAD",
		"\"abstract\"",
		"\"case\"",
		"\"catch\"",
		"\"class\"",
		"\"def\"",
		"\"do\"",
		"\"else\"",
		"\"extends\"",
		"\"final\"",
		"\"finally\"",
		"\"for\"",
		"\"forSome\"",
		"\"if\"",
		"\"implicit\"",
		"\"import\"",
		"\"lazy\"",
		"\"match\"",
		"\"new\"",
		"\"null\"",
		"\"object\"",
		"\"override\"",
		"\"package\"",
		"\"private\"",
		"\"protected\"",
		"\"return\"",
		"\"sealed\"",
		"\"super\"",
		"\"this\"",
		"\"throw\"",
		"\"trait\"",
		"\"try\"",
		"\"type\"",
		"\"val\"",
		"\"var\"",
		"\"while\"",
		"\"with\"",
		"\"yield\"",
		"\"_\"",
		"\":\"",
		"\"=\"",
		"\"=>\"",
		"\"<-\"",
		"\"<:\"",
		"\"<%\"",
		"\">:\"",
		"\"#\"",
		"\"@\"",
		"LPAREN",
		"RPAREN",
		"LBRACE",
		"RBRACE",
		"LSQBRACKET",
		"RSQBRACKET",
		"WS",
		"MLCOMMENT",
		"SLCOMMENT",
		"ID",
		"STRINGLIT",
		"STRINGELEMENT",
		"CHARLIT",
		"CHARESCSEQ",
		"BOOLLIT",
		"PLAINID",
		"IDREST",
		"DELIMCHAR",
		"PARENS",
		"OPCHAR",
		"LETTER",
		"UPPER",
		"LOWER",
		"NUMBERLITERAL",
		"EXPFLOAT",
		"FLOATTYPE",
		"EXP",
		"OCTAL",
		"DECIMAL",
		"HEX",
		"HEXDIGIT",
		"DIGIT",
		"NONZERODIGIT",
		"OCTALDIGIT",
		"UNICODE_CLASS_LL",
		"UNICODE_CLASS_LO",
		"UNICODE_CLASS_LT",
		"UNICODE_CLASS_LU",
		"UNICODE_CLASS_NL",
		"INT"
	};
	
	private static final long[] mk_tokenSet_0() {
		long[] data = { 2L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_0 = new BitSet(mk_tokenSet_0());
	
	}
