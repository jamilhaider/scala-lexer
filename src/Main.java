package src;
import java.io.*;
import antlr.Token;
import antlr.TokenStreamException;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		final String prog = "object Main { def main(args: Array[String]) = {println(\"Hello World!\")} }";
		
		ScalaLexer lexer = new ScalaLexer(new StringBufferInputStream(prog));
		try {
			Token t = lexer.nextToken();
			while(t.getType()!=ScalaLexer.EOF){
				System.out.println(t.toString());
				t = lexer.nextToken();
			}
		}
		catch (TokenStreamException e ){
			System.out.println(e);
		}

	}

}
