// $ANTLR : "gram.g" -> "ScalaLexer.java"$
package src; 
import java.io.InputStream;
import antlr.TokenStreamException;
import antlr.TokenStreamIOException;
import antlr.TokenStreamRecognitionException;
import antlr.CharStreamException;
import antlr.CharStreamIOException;
import antlr.ANTLRException;
import java.io.Reader;
import java.util.Hashtable;
import antlr.CharScanner;
import antlr.InputBuffer;
import antlr.ByteBuffer;
import antlr.CharBuffer;
import antlr.Token;
import antlr.CommonToken;
import antlr.RecognitionException;
import antlr.NoViableAltForCharException;
import antlr.MismatchedCharException;
import antlr.TokenStream;
import antlr.ANTLRHashString;
import antlr.LexerSharedInputState;
import antlr.collections.impl.BitSet;
import antlr.SemanticException;

public class ScalaLexer extends antlr.CharScanner implements ScalaLexerTokenTypes, TokenStream
 {
public ScalaLexer(InputStream in) {
	this(new ByteBuffer(in));
}
public ScalaLexer(Reader in) {
	this(new CharBuffer(in));
}
public ScalaLexer(InputBuffer ib) {
	this(new LexerSharedInputState(ib));
}
public ScalaLexer(LexerSharedInputState state) {
	super(state);
	caseSensitiveLiterals = true;
	setCaseSensitive(true);
	literals = new Hashtable();
	literals.put(new ANTLRHashString("case", this), new Integer(5));
	literals.put(new ANTLRHashString("this", this), new Integer(31));
	literals.put(new ANTLRHashString("sealed", this), new Integer(29));
	literals.put(new ANTLRHashString("#", this), new Integer(49));
	literals.put(new ANTLRHashString("for", this), new Integer(14));
	literals.put(new ANTLRHashString("class", this), new Integer(7));
	literals.put(new ANTLRHashString("final", this), new Integer(12));
	literals.put(new ANTLRHashString("try", this), new Integer(34));
	literals.put(new ANTLRHashString("finally", this), new Integer(13));
	literals.put(new ANTLRHashString("def", this), new Integer(8));
	literals.put(new ANTLRHashString("_", this), new Integer(41));
	literals.put(new ANTLRHashString(":", this), new Integer(42));
	literals.put(new ANTLRHashString("import", this), new Integer(18));
	literals.put(new ANTLRHashString(">:", this), new Integer(48));
	literals.put(new ANTLRHashString("@", this), new Integer(50));
	literals.put(new ANTLRHashString("private", this), new Integer(26));
	literals.put(new ANTLRHashString("override", this), new Integer(24));
	literals.put(new ANTLRHashString("<:", this), new Integer(46));
	literals.put(new ANTLRHashString("throw", this), new Integer(32));
	literals.put(new ANTLRHashString("implicit", this), new Integer(17));
	literals.put(new ANTLRHashString("<-", this), new Integer(45));
	literals.put(new ANTLRHashString("match", this), new Integer(20));
	literals.put(new ANTLRHashString("do", this), new Integer(9));
	literals.put(new ANTLRHashString("type", this), new Integer(35));
	literals.put(new ANTLRHashString("null", this), new Integer(22));
	literals.put(new ANTLRHashString("extends", this), new Integer(11));
	literals.put(new ANTLRHashString("while", this), new Integer(38));
	literals.put(new ANTLRHashString("=>", this), new Integer(44));
	literals.put(new ANTLRHashString("abstract", this), new Integer(4));
	literals.put(new ANTLRHashString("protected", this), new Integer(27));
	literals.put(new ANTLRHashString("forSome", this), new Integer(15));
	literals.put(new ANTLRHashString("trait", this), new Integer(33));
	literals.put(new ANTLRHashString("new", this), new Integer(21));
	literals.put(new ANTLRHashString("<%", this), new Integer(47));
	literals.put(new ANTLRHashString("return", this), new Integer(28));
	literals.put(new ANTLRHashString("=", this), new Integer(43));
	literals.put(new ANTLRHashString("if", this), new Integer(16));
	literals.put(new ANTLRHashString("lazy", this), new Integer(19));
	literals.put(new ANTLRHashString("yield", this), new Integer(40));
	literals.put(new ANTLRHashString("super", this), new Integer(30));
	literals.put(new ANTLRHashString("val", this), new Integer(36));
	literals.put(new ANTLRHashString("package", this), new Integer(25));
	literals.put(new ANTLRHashString("object", this), new Integer(23));
	literals.put(new ANTLRHashString("else", this), new Integer(10));
	literals.put(new ANTLRHashString("var", this), new Integer(37));
	literals.put(new ANTLRHashString("with", this), new Integer(39));
	literals.put(new ANTLRHashString("catch", this), new Integer(6));
}

public Token nextToken() throws TokenStreamException {
	Token theRetToken=null;
tryAgain:
	for (;;) {
		Token _token = null;
		int _ttype = Token.INVALID_TYPE;
		resetText();
		try {   // for char stream error handling
			try {   // for lexical error handling
				switch ( LA(1)) {
				case '(':
				{
					mLPAREN(true);
					theRetToken=_returnToken;
					break;
				}
				case ')':
				{
					mRPAREN(true);
					theRetToken=_returnToken;
					break;
				}
				case '{':
				{
					mLBRACE(true);
					theRetToken=_returnToken;
					break;
				}
				case '}':
				{
					mRBRACE(true);
					theRetToken=_returnToken;
					break;
				}
				case '[':
				{
					mLSQBRACKET(true);
					theRetToken=_returnToken;
					break;
				}
				case ']':
				{
					mRSQBRACKET(true);
					theRetToken=_returnToken;
					break;
				}
				case '\t':  case '\n':  case '\u000c':  case '\r':
				case ' ':
				{
					mWS(true);
					theRetToken=_returnToken;
					break;
				}
				case '"':
				{
					mSTRINGLIT(true);
					theRetToken=_returnToken;
					break;
				}
				case '\'':
				{
					mCHARLIT(true);
					theRetToken=_returnToken;
					break;
				}
				case '\u01c5':  case '\u01c8':  case '\u01cb':  case '\u01f2':
				case '\u1f88':  case '\u1f89':  case '\u1f8a':  case '\u1f8b':
				case '\u1f8c':  case '\u1f8d':  case '\u1f8e':  case '\u1f8f':
				case '\u1f98':  case '\u1f99':  case '\u1f9a':  case '\u1f9b':
				case '\u1f9c':  case '\u1f9d':  case '\u1f9e':  case '\u1f9f':
				case '\u1fa8':  case '\u1fa9':  case '\u1faa':  case '\u1fab':
				case '\u1fac':  case '\u1fad':  case '\u1fae':  case '\u1faf':
				case '\u1fbc':  case '\u1fcc':  case '\u1ffc':
				{
					mUNICODE_CLASS_LT(true);
					theRetToken=_returnToken;
					break;
				}
				case '\u16ee':  case '\u16ef':  case '\u16f0':  case '\u2160':
				case '\u2161':  case '\u2162':  case '\u2163':  case '\u2164':
				case '\u2165':  case '\u2166':  case '\u2167':  case '\u2168':
				case '\u2169':  case '\u216a':  case '\u216b':  case '\u216c':
				case '\u216d':  case '\u216e':  case '\u216f':  case '\u2170':
				case '\u2171':  case '\u2172':  case '\u2173':  case '\u2174':
				case '\u2175':  case '\u2176':  case '\u2177':  case '\u2178':
				case '\u2179':  case '\u217a':  case '\u217b':  case '\u217c':
				case '\u217d':  case '\u217e':  case '\u217f':  case '\u2180':
				case '\u2181':  case '\u2182':  case '\u2185':  case '\u2186':
				case '\u2187':  case '\u2188':  case '\u3007':  case '\u3021':
				case '\u3022':  case '\u3023':  case '\u3024':  case '\u3025':
				case '\u3026':  case '\u3027':  case '\u3028':  case '\u3029':
				case '\u3038':  case '\u3039':  case '\u303a':  case '\ua6e6':
				case '\ua6e7':  case '\ua6e8':  case '\ua6e9':  case '\ua6ea':
				case '\ua6eb':  case '\ua6ec':  case '\ua6ed':  case '\ua6ee':
				case '\ua6ef':
				{
					mUNICODE_CLASS_NL(true);
					theRetToken=_returnToken;
					break;
				}
				case '.':  case '0':  case '1':  case '2':
				case '3':  case '4':  case '5':  case '6':
				case '7':  case '8':  case '9':
				{
					mNUMBERLITERAL(true);
					theRetToken=_returnToken;
					break;
				}
				default:
					if ((LA(1)=='/') && (LA(2)=='*')) {
						mMLCOMMENT(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='/') && (LA(2)=='/')) {
						mSLCOMMENT(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='f'||LA(1)=='t') && (LA(2)=='a'||LA(2)=='r')) {
						mBOOLLIT(true);
						theRetToken=_returnToken;
					}
					else if ((_tokenSet_0.member(LA(1))) && (true)) {
						mID(true);
						theRetToken=_returnToken;
					}
					else if ((_tokenSet_1.member(LA(1)))) {
						mUNICODE_CLASS_LO(true);
						theRetToken=_returnToken;
					}
					else if ((_tokenSet_2.member(LA(1))) && (true)) {
						mUNICODE_CLASS_LU(true);
						theRetToken=_returnToken;
					}
					else if ((_tokenSet_3.member(LA(1))) && (true)) {
						mUNICODE_CLASS_LL(true);
						theRetToken=_returnToken;
					}
					else if ((_tokenSet_4.member(LA(1))) && (true)) {
						mEXPFLOAT(true);
						theRetToken=_returnToken;
					}
				else {
					if (LA(1)==EOF_CHAR) {uponEOF(); _returnToken = makeToken(Token.EOF_TYPE);}
				else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());}
				}
				}
				if ( _returnToken==null ) continue tryAgain; // found SKIP token
				_ttype = _returnToken.getType();
				_ttype = testLiteralsTable(_ttype);
				_returnToken.setType(_ttype);
				return _returnToken;
			}
			catch (RecognitionException e) {
				throw new TokenStreamRecognitionException(e);
			}
		}
		catch (CharStreamException cse) {
			if ( cse instanceof CharStreamIOException ) {
				throw new TokenStreamIOException(((CharStreamIOException)cse).io);
			}
			else {
				throw new TokenStreamException(cse.getMessage());
			}
		}
	}
}

	public final void mLPAREN(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = LPAREN;
		int _saveIndex;
		
		match("(");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mRPAREN(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = RPAREN;
		int _saveIndex;
		
		match(")");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mLBRACE(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = LBRACE;
		int _saveIndex;
		
		match("{");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mRBRACE(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = RBRACE;
		int _saveIndex;
		
		match("}");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mLSQBRACKET(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = LSQBRACKET;
		int _saveIndex;
		
		match("[");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mRSQBRACKET(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = RSQBRACKET;
		int _saveIndex;
		
		match("]");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mWS(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = WS;
		int _saveIndex;
		
		{
		int _cnt5893=0;
		_loop5893:
		do {
			switch ( LA(1)) {
			case ' ':
			{
				match(' ');
				break;
			}
			case '\t':
			{
				match('\t');
				break;
			}
			case '\u000c':
			{
				match('\f');
				break;
			}
			case '\n':  case '\r':
			{
				{
				if ((LA(1)=='\r') && (LA(2)=='\n')) {
					match("\r\n");
				}
				else if ((LA(1)=='\r') && (true)) {
					match('\r');
				}
				else if ((LA(1)=='\n')) {
					match('\n');
				}
				else {
					throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
				}
				
				}
				newline();
				break;
			}
			default:
			{
				if ( _cnt5893>=1 ) { break _loop5893; } else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());}
			}
			}
			_cnt5893++;
		} while (true);
		}
		_ttype = Token.SKIP;
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mMLCOMMENT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = MLCOMMENT;
		int _saveIndex;
		
		match("/*");
		{
		_loop5897:
		do {
			if (((LA(1)=='*') && (_tokenSet_5.member(LA(2))))&&( LA(2)!='/' )) {
				match('*');
			}
			else if ((LA(1)=='\r') && (LA(2)=='\n')) {
				match('\r');
				match('\n');
				newline();
			}
			else if ((LA(1)=='\r') && (_tokenSet_5.member(LA(2)))) {
				match('\r');
				newline();
			}
			else if ((LA(1)=='\n')) {
				match('\n');
				newline();
			}
			else if ((_tokenSet_6.member(LA(1)))) {
				{
				match(_tokenSet_6);
				}
			}
			else {
				break _loop5897;
			}
			
		} while (true);
		}
		match("*/");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mSLCOMMENT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = SLCOMMENT;
		int _saveIndex;
		
		match("//");
		{
		_loop5901:
		do {
			if ((_tokenSet_7.member(LA(1)))) {
				{
				match(_tokenSet_7);
				}
			}
			else {
				break _loop5901;
			}
			
		} while (true);
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mID(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = ID;
		int _saveIndex;
		
		if ((_tokenSet_8.member(LA(1)))) {
			mPLAINID(false);
		}
		else if ((LA(1)=='`')) {
			match('`');
			mSTRINGLIT(false);
			match('`');
		}
		else {
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mPLAINID(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = PLAINID;
		int _saveIndex;
		
		if ((_tokenSet_9.member(LA(1)))) {
			{
			if ((_tokenSet_2.member(LA(1)))) {
				mUPPER(false);
			}
			else if ((_tokenSet_3.member(LA(1)))) {
				mLOWER(false);
			}
			else if ((LA(1)=='_')) {
				match('_');
			}
			else {
				throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
			}
			
			}
			mIDREST(false);
		}
		else if ((_tokenSet_10.member(LA(1)))) {
			{
			int _cnt5919=0;
			_loop5919:
			do {
				if ((_tokenSet_10.member(LA(1)))) {
					mOPCHAR(false);
				}
				else {
					if ( _cnt5919>=1 ) { break _loop5919; } else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());}
				}
				
				_cnt5919++;
			} while (true);
			}
		}
		else {
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mSTRINGLIT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = STRINGLIT;
		int _saveIndex;
		
		match('"');
		{
		_loop5906:
		do {
			if ((LA(1)=='\\')) {
				mCHARESCSEQ(false);
			}
			else if ((_tokenSet_11.member(LA(1)))) {
				{
				mSTRINGELEMENT(false);
				}
			}
			else {
				break _loop5906;
			}
			
		} while (true);
		}
		match('"');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mCHARESCSEQ(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = CHARESCSEQ;
		int _saveIndex;
		
		if ((LA(1)=='\\') && (LA(2)=='b')) {
			match("\\b");
		}
		else if ((LA(1)=='\\') && (LA(2)=='t')) {
			match("\\t");
		}
		else if ((LA(1)=='\\') && (LA(2)=='n')) {
			match("\\n");
		}
		else if ((LA(1)=='\\') && (LA(2)=='f')) {
			match("\\f");
		}
		else if ((LA(1)=='\\') && (LA(2)=='r')) {
			match("\\r");
		}
		else if ((LA(1)=='\\') && (LA(2)=='\'')) {
			match("\\'");
		}
		else if ((LA(1)=='\\') && (LA(2)=='"')) {
			match('\\');
			match('"');
		}
		else if ((LA(1)=='\\') && (LA(2)=='\\')) {
			match("\\\\");
		}
		else {
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mSTRINGELEMENT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = STRINGELEMENT;
		int _saveIndex;
		
		{
		match(_tokenSet_11);
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mCHARLIT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = CHARLIT;
		int _saveIndex;
		
		match("\'");
		{
		switch ( LA(1)) {
		case '\\':
		{
			{
			{
			if ((LA(1)=='\\') && (_tokenSet_12.member(LA(2)))) {
				mCHARESCSEQ(false);
			}
			else if ((LA(1)=='\\') && (LA(2)=='u')) {
				{
				match("\\");
				match('u');
				mHEXDIGIT(false);
				mHEXDIGIT(false);
				mHEXDIGIT(false);
				mHEXDIGIT(false);
				}
			}
			else {
				throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
			}
			
			}
			}
			break;
		}
		case '\'':
		{
			break;
		}
		default:
			if ((_tokenSet_13.member(LA(1)))) {
				mLETTER(false);
			}
		else {
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		}
		match("\'");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mLETTER(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = LETTER;
		int _saveIndex;
		
		switch ( LA(1)) {
		case '\u01c5':  case '\u01c8':  case '\u01cb':  case '\u01f2':
		case '\u1f88':  case '\u1f89':  case '\u1f8a':  case '\u1f8b':
		case '\u1f8c':  case '\u1f8d':  case '\u1f8e':  case '\u1f8f':
		case '\u1f98':  case '\u1f99':  case '\u1f9a':  case '\u1f9b':
		case '\u1f9c':  case '\u1f9d':  case '\u1f9e':  case '\u1f9f':
		case '\u1fa8':  case '\u1fa9':  case '\u1faa':  case '\u1fab':
		case '\u1fac':  case '\u1fad':  case '\u1fae':  case '\u1faf':
		case '\u1fbc':  case '\u1fcc':  case '\u1ffc':
		{
			mUNICODE_CLASS_LT(false);
			break;
		}
		case '\u16ee':  case '\u16ef':  case '\u16f0':  case '\u2160':
		case '\u2161':  case '\u2162':  case '\u2163':  case '\u2164':
		case '\u2165':  case '\u2166':  case '\u2167':  case '\u2168':
		case '\u2169':  case '\u216a':  case '\u216b':  case '\u216c':
		case '\u216d':  case '\u216e':  case '\u216f':  case '\u2170':
		case '\u2171':  case '\u2172':  case '\u2173':  case '\u2174':
		case '\u2175':  case '\u2176':  case '\u2177':  case '\u2178':
		case '\u2179':  case '\u217a':  case '\u217b':  case '\u217c':
		case '\u217d':  case '\u217e':  case '\u217f':  case '\u2180':
		case '\u2181':  case '\u2182':  case '\u2185':  case '\u2186':
		case '\u2187':  case '\u2188':  case '\u3007':  case '\u3021':
		case '\u3022':  case '\u3023':  case '\u3024':  case '\u3025':
		case '\u3026':  case '\u3027':  case '\u3028':  case '\u3029':
		case '\u3038':  case '\u3039':  case '\u303a':  case '\ua6e6':
		case '\ua6e7':  case '\ua6e8':  case '\ua6e9':  case '\ua6ea':
		case '\ua6eb':  case '\ua6ec':  case '\ua6ed':  case '\ua6ee':
		case '\ua6ef':
		{
			mUNICODE_CLASS_NL(false);
			break;
		}
		default:
			if ((_tokenSet_2.member(LA(1)))) {
				mUPPER(false);
			}
			else if ((_tokenSet_3.member(LA(1)))) {
				mLOWER(false);
			}
			else if ((_tokenSet_1.member(LA(1)))) {
				mUNICODE_CLASS_LO(false);
			}
		else {
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mHEXDIGIT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = HEXDIGIT;
		int _saveIndex;
		
		switch ( LA(1)) {
		case '0':  case '1':  case '2':  case '3':
		case '4':  case '5':  case '6':  case '7':
		case '8':  case '9':
		{
			mDIGIT(false);
			break;
		}
		case 'a':  case 'b':  case 'c':  case 'd':
		case 'e':  case 'f':
		{
			{
			matchRange('a','f');
			}
			break;
		}
		case 'A':  case 'B':  case 'C':  case 'D':
		case 'E':  case 'F':
		{
			{
			matchRange('A','F');
			}
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mBOOLLIT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = BOOLLIT;
		int _saveIndex;
		
		switch ( LA(1)) {
		case 't':
		{
			match("true");
			break;
		}
		case 'f':
		{
			match("false");
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mUPPER(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = UPPER;
		int _saveIndex;
		
		mUNICODE_CLASS_LU(false);
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mLOWER(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = LOWER;
		int _saveIndex;
		
		mUNICODE_CLASS_LL(false);
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mIDREST(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = IDREST;
		int _saveIndex;
		
		{
		_loop5922:
		do {
			switch ( LA(1)) {
			case '0':  case '1':  case '2':  case '3':
			case '4':  case '5':  case '6':  case '7':
			case '8':  case '9':
			{
				mDIGIT(false);
				break;
			}
			case '_':
			{
				match('_');
				break;
			}
			default:
				if ((_tokenSet_13.member(LA(1)))) {
					mLETTER(false);
				}
			else {
				break _loop5922;
			}
			}
		} while (true);
		}
		{
		if ((_tokenSet_10.member(LA(1)))) {
			{
			int _cnt5925=0;
			_loop5925:
			do {
				if ((_tokenSet_10.member(LA(1)))) {
					mOPCHAR(false);
				}
				else {
					if ( _cnt5925>=1 ) { break _loop5925; } else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());}
				}
				
				_cnt5925++;
			} while (true);
			}
		}
		else {
		}
		
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mOPCHAR(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = OPCHAR;
		int _saveIndex;
		
		switch ( LA(1)) {
		case '!':
		{
			match('\u0021');
			break;
		}
		case '#':
		{
			match('\u0023');
			break;
		}
		case '%':
		{
			match('\u0025');
			break;
		}
		case '&':
		{
			match('\u0026');
			break;
		}
		case '*':
		{
			match('\u002A');
			break;
		}
		case '+':
		{
			match('\u002B');
			break;
		}
		case '-':
		{
			match('\u002D');
			break;
		}
		case '/':
		{
			match('\u002F');
			break;
		}
		case ':':
		{
			match('\u003A');
			break;
		}
		case '<':  case '=':  case '>':  case '?':
		case '@':
		{
			matchRange('\u003C','\u0040');
			break;
		}
		case '\\':
		{
			match('\\');
			break;
		}
		case '^':
		{
			match('\u005E');
			break;
		}
		case '|':
		{
			match('\u007C');
			break;
		}
		case '~':
		{
			match('\u007E');
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mDIGIT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = DIGIT;
		int _saveIndex;
		
		switch ( LA(1)) {
		case '1':  case '2':  case '3':  case '4':
		case '5':  case '6':  case '7':  case '8':
		case '9':
		{
			mNONZERODIGIT(false);
			break;
		}
		case '0':
		{
			match('0');
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mDELIMCHAR(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = DELIMCHAR;
		int _saveIndex;
		
		switch ( LA(1)) {
		case '\'':
		{
			match('\'');
			break;
		}
		case '`':
		{
			match('`');
			break;
		}
		case '"':
		{
			match('"');
			break;
		}
		case '.':
		{
			match('.');
			break;
		}
		case ',':
		{
			match(',');
			break;
		}
		case ';':
		{
			match(';');
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mPARENS(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = PARENS;
		int _saveIndex;
		
		switch ( LA(1)) {
		case '(':
		{
			match('(');
			break;
		}
		case ')':
		{
			match(')');
			break;
		}
		case '[':
		{
			match('[');
			break;
		}
		case ']':
		{
			match(']');
			break;
		}
		case '{':
		{
			match('{');
			break;
		}
		case '}':
		{
			match('}');
			matchNot(EOF_CHAR);
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mUNICODE_CLASS_LT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = UNICODE_CLASS_LT;
		int _saveIndex;
		
		switch ( LA(1)) {
		case '\u01c5':
		{
			match('\u01C5');
			break;
		}
		case '\u01c8':
		{
			match('\u01C8');
			break;
		}
		case '\u01cb':
		{
			match('\u01CB');
			break;
		}
		case '\u01f2':
		{
			match('\u01F2');
			break;
		}
		case '\u1f88':  case '\u1f89':  case '\u1f8a':  case '\u1f8b':
		case '\u1f8c':  case '\u1f8d':  case '\u1f8e':  case '\u1f8f':
		{
			matchRange('\u1F88','\u1F8F');
			break;
		}
		case '\u1f98':  case '\u1f99':  case '\u1f9a':  case '\u1f9b':
		case '\u1f9c':  case '\u1f9d':  case '\u1f9e':  case '\u1f9f':
		{
			matchRange('\u1F98','\u1F9F');
			break;
		}
		case '\u1fa8':  case '\u1fa9':  case '\u1faa':  case '\u1fab':
		case '\u1fac':  case '\u1fad':  case '\u1fae':  case '\u1faf':
		{
			matchRange('\u1FA8','\u1FAF');
			break;
		}
		case '\u1fbc':
		{
			match('\u1FBC');
			break;
		}
		case '\u1fcc':
		{
			match('\u1FCC');
			break;
		}
		case '\u1ffc':
		{
			match('\u1FFC');
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mUNICODE_CLASS_LO(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = UNICODE_CLASS_LO;
		int _saveIndex;
		
		switch ( LA(1)) {
		case '\u00aa':
		{
			match('\u00AA');
			break;
		}
		case '\u00ba':
		{
			match('\u00BA');
			break;
		}
		case '\u01bb':
		{
			match('\u01BB');
			break;
		}
		case '\u01c0':  case '\u01c1':  case '\u01c2':  case '\u01c3':
		{
			matchRange('\u01C0','\u01C3');
			break;
		}
		case '\u0294':
		{
			match('\u0294');
			break;
		}
		case '\u05d0':  case '\u05d1':  case '\u05d2':  case '\u05d3':
		case '\u05d4':  case '\u05d5':  case '\u05d6':  case '\u05d7':
		case '\u05d8':  case '\u05d9':  case '\u05da':  case '\u05db':
		case '\u05dc':  case '\u05dd':  case '\u05de':  case '\u05df':
		case '\u05e0':  case '\u05e1':  case '\u05e2':  case '\u05e3':
		case '\u05e4':  case '\u05e5':  case '\u05e6':  case '\u05e7':
		case '\u05e8':  case '\u05e9':  case '\u05ea':
		{
			matchRange('\u05D0','\u05EA');
			break;
		}
		case '\u05f0':  case '\u05f1':  case '\u05f2':
		{
			matchRange('\u05F0','\u05F2');
			break;
		}
		case '\u0620':  case '\u0621':  case '\u0622':  case '\u0623':
		case '\u0624':  case '\u0625':  case '\u0626':  case '\u0627':
		case '\u0628':  case '\u0629':  case '\u062a':  case '\u062b':
		case '\u062c':  case '\u062d':  case '\u062e':  case '\u062f':
		case '\u0630':  case '\u0631':  case '\u0632':  case '\u0633':
		case '\u0634':  case '\u0635':  case '\u0636':  case '\u0637':
		case '\u0638':  case '\u0639':  case '\u063a':  case '\u063b':
		case '\u063c':  case '\u063d':  case '\u063e':  case '\u063f':
		{
			matchRange('\u0620','\u063F');
			break;
		}
		case '\u0641':  case '\u0642':  case '\u0643':  case '\u0644':
		case '\u0645':  case '\u0646':  case '\u0647':  case '\u0648':
		case '\u0649':  case '\u064a':
		{
			matchRange('\u0641','\u064A');
			break;
		}
		case '\u066e':
		{
			match('\u066E');
			break;
		}
		case '\u066f':
		{
			match('\u066F');
			break;
		}
		case '\u0671':  case '\u0672':  case '\u0673':  case '\u0674':
		case '\u0675':  case '\u0676':  case '\u0677':  case '\u0678':
		case '\u0679':  case '\u067a':  case '\u067b':  case '\u067c':
		case '\u067d':  case '\u067e':  case '\u067f':  case '\u0680':
		case '\u0681':  case '\u0682':  case '\u0683':  case '\u0684':
		case '\u0685':  case '\u0686':  case '\u0687':  case '\u0688':
		case '\u0689':  case '\u068a':  case '\u068b':  case '\u068c':
		case '\u068d':  case '\u068e':  case '\u068f':  case '\u0690':
		case '\u0691':  case '\u0692':  case '\u0693':  case '\u0694':
		case '\u0695':  case '\u0696':  case '\u0697':  case '\u0698':
		case '\u0699':  case '\u069a':  case '\u069b':  case '\u069c':
		case '\u069d':  case '\u069e':  case '\u069f':  case '\u06a0':
		case '\u06a1':  case '\u06a2':  case '\u06a3':  case '\u06a4':
		case '\u06a5':  case '\u06a6':  case '\u06a7':  case '\u06a8':
		case '\u06a9':  case '\u06aa':  case '\u06ab':  case '\u06ac':
		case '\u06ad':  case '\u06ae':  case '\u06af':  case '\u06b0':
		case '\u06b1':  case '\u06b2':  case '\u06b3':  case '\u06b4':
		case '\u06b5':  case '\u06b6':  case '\u06b7':  case '\u06b8':
		case '\u06b9':  case '\u06ba':  case '\u06bb':  case '\u06bc':
		case '\u06bd':  case '\u06be':  case '\u06bf':  case '\u06c0':
		case '\u06c1':  case '\u06c2':  case '\u06c3':  case '\u06c4':
		case '\u06c5':  case '\u06c6':  case '\u06c7':  case '\u06c8':
		case '\u06c9':  case '\u06ca':  case '\u06cb':  case '\u06cc':
		case '\u06cd':  case '\u06ce':  case '\u06cf':  case '\u06d0':
		case '\u06d1':  case '\u06d2':  case '\u06d3':
		{
			matchRange('\u0671','\u06D3');
			break;
		}
		case '\u06d5':
		{
			match('\u06D5');
			break;
		}
		case '\u06ee':
		{
			match('\u06EE');
			break;
		}
		case '\u06ef':
		{
			match('\u06EF');
			break;
		}
		case '\u06fa':  case '\u06fb':  case '\u06fc':
		{
			matchRange('\u06FA','\u06FC');
			break;
		}
		case '\u06ff':
		{
			match('\u06FF');
			break;
		}
		case '\u0710':
		{
			match('\u0710');
			break;
		}
		case '\u0712':  case '\u0713':  case '\u0714':  case '\u0715':
		case '\u0716':  case '\u0717':  case '\u0718':  case '\u0719':
		case '\u071a':  case '\u071b':  case '\u071c':  case '\u071d':
		case '\u071e':  case '\u071f':  case '\u0720':  case '\u0721':
		case '\u0722':  case '\u0723':  case '\u0724':  case '\u0725':
		case '\u0726':  case '\u0727':  case '\u0728':  case '\u0729':
		case '\u072a':  case '\u072b':  case '\u072c':  case '\u072d':
		case '\u072e':  case '\u072f':
		{
			matchRange('\u0712','\u072F');
			break;
		}
		case '\u074d':  case '\u074e':  case '\u074f':  case '\u0750':
		case '\u0751':  case '\u0752':  case '\u0753':  case '\u0754':
		case '\u0755':  case '\u0756':  case '\u0757':  case '\u0758':
		case '\u0759':  case '\u075a':  case '\u075b':  case '\u075c':
		case '\u075d':  case '\u075e':  case '\u075f':  case '\u0760':
		case '\u0761':  case '\u0762':  case '\u0763':  case '\u0764':
		case '\u0765':  case '\u0766':  case '\u0767':  case '\u0768':
		case '\u0769':  case '\u076a':  case '\u076b':  case '\u076c':
		case '\u076d':  case '\u076e':  case '\u076f':  case '\u0770':
		case '\u0771':  case '\u0772':  case '\u0773':  case '\u0774':
		case '\u0775':  case '\u0776':  case '\u0777':  case '\u0778':
		case '\u0779':  case '\u077a':  case '\u077b':  case '\u077c':
		case '\u077d':  case '\u077e':  case '\u077f':  case '\u0780':
		case '\u0781':  case '\u0782':  case '\u0783':  case '\u0784':
		case '\u0785':  case '\u0786':  case '\u0787':  case '\u0788':
		case '\u0789':  case '\u078a':  case '\u078b':  case '\u078c':
		case '\u078d':  case '\u078e':  case '\u078f':  case '\u0790':
		case '\u0791':  case '\u0792':  case '\u0793':  case '\u0794':
		case '\u0795':  case '\u0796':  case '\u0797':  case '\u0798':
		case '\u0799':  case '\u079a':  case '\u079b':  case '\u079c':
		case '\u079d':  case '\u079e':  case '\u079f':  case '\u07a0':
		case '\u07a1':  case '\u07a2':  case '\u07a3':  case '\u07a4':
		case '\u07a5':
		{
			matchRange('\u074D','\u07A5');
			break;
		}
		case '\u07b1':
		{
			match('\u07B1');
			break;
		}
		case '\u07ca':  case '\u07cb':  case '\u07cc':  case '\u07cd':
		case '\u07ce':  case '\u07cf':  case '\u07d0':  case '\u07d1':
		case '\u07d2':  case '\u07d3':  case '\u07d4':  case '\u07d5':
		case '\u07d6':  case '\u07d7':  case '\u07d8':  case '\u07d9':
		case '\u07da':  case '\u07db':  case '\u07dc':  case '\u07dd':
		case '\u07de':  case '\u07df':  case '\u07e0':  case '\u07e1':
		case '\u07e2':  case '\u07e3':  case '\u07e4':  case '\u07e5':
		case '\u07e6':  case '\u07e7':  case '\u07e8':  case '\u07e9':
		case '\u07ea':
		{
			matchRange('\u07CA','\u07EA');
			break;
		}
		case '\u0800':  case '\u0801':  case '\u0802':  case '\u0803':
		case '\u0804':  case '\u0805':  case '\u0806':  case '\u0807':
		case '\u0808':  case '\u0809':  case '\u080a':  case '\u080b':
		case '\u080c':  case '\u080d':  case '\u080e':  case '\u080f':
		case '\u0810':  case '\u0811':  case '\u0812':  case '\u0813':
		case '\u0814':  case '\u0815':
		{
			matchRange('\u0800','\u0815');
			break;
		}
		case '\u0840':  case '\u0841':  case '\u0842':  case '\u0843':
		case '\u0844':  case '\u0845':  case '\u0846':  case '\u0847':
		case '\u0848':  case '\u0849':  case '\u084a':  case '\u084b':
		case '\u084c':  case '\u084d':  case '\u084e':  case '\u084f':
		case '\u0850':  case '\u0851':  case '\u0852':  case '\u0853':
		case '\u0854':  case '\u0855':  case '\u0856':  case '\u0857':
		case '\u0858':
		{
			matchRange('\u0840','\u0858');
			break;
		}
		case '\u08a0':
		{
			match('\u08A0');
			break;
		}
		case '\u08a2':  case '\u08a3':  case '\u08a4':  case '\u08a5':
		case '\u08a6':  case '\u08a7':  case '\u08a8':  case '\u08a9':
		case '\u08aa':  case '\u08ab':  case '\u08ac':
		{
			matchRange('\u08A2','\u08AC');
			break;
		}
		case '\u0904':  case '\u0905':  case '\u0906':  case '\u0907':
		case '\u0908':  case '\u0909':  case '\u090a':  case '\u090b':
		case '\u090c':  case '\u090d':  case '\u090e':  case '\u090f':
		case '\u0910':  case '\u0911':  case '\u0912':  case '\u0913':
		case '\u0914':  case '\u0915':  case '\u0916':  case '\u0917':
		case '\u0918':  case '\u0919':  case '\u091a':  case '\u091b':
		case '\u091c':  case '\u091d':  case '\u091e':  case '\u091f':
		case '\u0920':  case '\u0921':  case '\u0922':  case '\u0923':
		case '\u0924':  case '\u0925':  case '\u0926':  case '\u0927':
		case '\u0928':  case '\u0929':  case '\u092a':  case '\u092b':
		case '\u092c':  case '\u092d':  case '\u092e':  case '\u092f':
		case '\u0930':  case '\u0931':  case '\u0932':  case '\u0933':
		case '\u0934':  case '\u0935':  case '\u0936':  case '\u0937':
		case '\u0938':  case '\u0939':
		{
			matchRange('\u0904','\u0939');
			break;
		}
		case '\u093d':
		{
			match('\u093D');
			break;
		}
		case '\u0950':
		{
			match('\u0950');
			break;
		}
		case '\u0958':  case '\u0959':  case '\u095a':  case '\u095b':
		case '\u095c':  case '\u095d':  case '\u095e':  case '\u095f':
		case '\u0960':  case '\u0961':
		{
			matchRange('\u0958','\u0961');
			break;
		}
		case '\u0972':  case '\u0973':  case '\u0974':  case '\u0975':
		case '\u0976':  case '\u0977':
		{
			matchRange('\u0972','\u0977');
			break;
		}
		case '\u0979':  case '\u097a':  case '\u097b':  case '\u097c':
		case '\u097d':  case '\u097e':  case '\u097f':
		{
			matchRange('\u0979','\u097F');
			break;
		}
		case '\u0985':  case '\u0986':  case '\u0987':  case '\u0988':
		case '\u0989':  case '\u098a':  case '\u098b':  case '\u098c':
		{
			matchRange('\u0985','\u098C');
			break;
		}
		case '\u098f':
		{
			match('\u098F');
			break;
		}
		case '\u0990':
		{
			match('\u0990');
			break;
		}
		case '\u0993':  case '\u0994':  case '\u0995':  case '\u0996':
		case '\u0997':  case '\u0998':  case '\u0999':  case '\u099a':
		case '\u099b':  case '\u099c':  case '\u099d':  case '\u099e':
		case '\u099f':  case '\u09a0':  case '\u09a1':  case '\u09a2':
		case '\u09a3':  case '\u09a4':  case '\u09a5':  case '\u09a6':
		case '\u09a7':  case '\u09a8':
		{
			matchRange('\u0993','\u09A8');
			break;
		}
		case '\u09aa':  case '\u09ab':  case '\u09ac':  case '\u09ad':
		case '\u09ae':  case '\u09af':  case '\u09b0':
		{
			matchRange('\u09AA','\u09B0');
			break;
		}
		case '\u09b2':
		{
			match('\u09B2');
			break;
		}
		case '\u09b6':  case '\u09b7':  case '\u09b8':  case '\u09b9':
		{
			matchRange('\u09B6','\u09B9');
			break;
		}
		case '\u09bd':
		{
			match('\u09BD');
			break;
		}
		case '\u09ce':
		{
			match('\u09CE');
			break;
		}
		case '\u09dc':
		{
			match('\u09DC');
			break;
		}
		case '\u09dd':
		{
			match('\u09DD');
			break;
		}
		case '\u09df':  case '\u09e0':  case '\u09e1':
		{
			matchRange('\u09DF','\u09E1');
			break;
		}
		case '\u09f0':
		{
			match('\u09F0');
			break;
		}
		case '\u09f1':
		{
			match('\u09F1');
			break;
		}
		case '\u0a05':  case '\u0a06':  case '\u0a07':  case '\u0a08':
		case '\u0a09':  case '\u0a0a':
		{
			matchRange('\u0A05','\u0A0A');
			break;
		}
		case '\u0a0f':
		{
			match('\u0A0F');
			break;
		}
		case '\u0a10':
		{
			match('\u0A10');
			break;
		}
		case '\u0a13':  case '\u0a14':  case '\u0a15':  case '\u0a16':
		case '\u0a17':  case '\u0a18':  case '\u0a19':  case '\u0a1a':
		case '\u0a1b':  case '\u0a1c':  case '\u0a1d':  case '\u0a1e':
		case '\u0a1f':  case '\u0a20':  case '\u0a21':  case '\u0a22':
		case '\u0a23':  case '\u0a24':  case '\u0a25':  case '\u0a26':
		case '\u0a27':  case '\u0a28':
		{
			matchRange('\u0A13','\u0A28');
			break;
		}
		case '\u0a2a':  case '\u0a2b':  case '\u0a2c':  case '\u0a2d':
		case '\u0a2e':  case '\u0a2f':  case '\u0a30':
		{
			matchRange('\u0A2A','\u0A30');
			break;
		}
		case '\u0a32':
		{
			match('\u0A32');
			break;
		}
		case '\u0a33':
		{
			match('\u0A33');
			break;
		}
		case '\u0a35':
		{
			match('\u0A35');
			break;
		}
		case '\u0a36':
		{
			match('\u0A36');
			break;
		}
		case '\u0a38':
		{
			match('\u0A38');
			break;
		}
		case '\u0a39':
		{
			match('\u0A39');
			break;
		}
		case '\u0a59':  case '\u0a5a':  case '\u0a5b':  case '\u0a5c':
		{
			matchRange('\u0A59','\u0A5C');
			break;
		}
		case '\u0a5e':
		{
			match('\u0A5E');
			break;
		}
		case '\u0a72':  case '\u0a73':  case '\u0a74':
		{
			matchRange('\u0A72','\u0A74');
			break;
		}
		case '\u0a85':  case '\u0a86':  case '\u0a87':  case '\u0a88':
		case '\u0a89':  case '\u0a8a':  case '\u0a8b':  case '\u0a8c':
		case '\u0a8d':
		{
			matchRange('\u0A85','\u0A8D');
			break;
		}
		case '\u0a8f':  case '\u0a90':  case '\u0a91':
		{
			matchRange('\u0A8F','\u0A91');
			break;
		}
		case '\u0a93':  case '\u0a94':  case '\u0a95':  case '\u0a96':
		case '\u0a97':  case '\u0a98':  case '\u0a99':  case '\u0a9a':
		case '\u0a9b':  case '\u0a9c':  case '\u0a9d':  case '\u0a9e':
		case '\u0a9f':  case '\u0aa0':  case '\u0aa1':  case '\u0aa2':
		case '\u0aa3':  case '\u0aa4':  case '\u0aa5':  case '\u0aa6':
		case '\u0aa7':  case '\u0aa8':
		{
			matchRange('\u0A93','\u0AA8');
			break;
		}
		case '\u0aaa':  case '\u0aab':  case '\u0aac':  case '\u0aad':
		case '\u0aae':  case '\u0aaf':  case '\u0ab0':
		{
			matchRange('\u0AAA','\u0AB0');
			break;
		}
		case '\u0ab2':
		{
			match('\u0AB2');
			break;
		}
		case '\u0ab3':
		{
			match('\u0AB3');
			break;
		}
		case '\u0ab5':  case '\u0ab6':  case '\u0ab7':  case '\u0ab8':
		case '\u0ab9':
		{
			matchRange('\u0AB5','\u0AB9');
			break;
		}
		case '\u0abd':
		{
			match('\u0ABD');
			break;
		}
		case '\u0ad0':
		{
			match('\u0AD0');
			break;
		}
		case '\u0ae0':
		{
			match('\u0AE0');
			break;
		}
		case '\u0ae1':
		{
			match('\u0AE1');
			break;
		}
		case '\u0b05':  case '\u0b06':  case '\u0b07':  case '\u0b08':
		case '\u0b09':  case '\u0b0a':  case '\u0b0b':  case '\u0b0c':
		{
			matchRange('\u0B05','\u0B0C');
			break;
		}
		case '\u0b0f':
		{
			match('\u0B0F');
			break;
		}
		case '\u0b10':
		{
			match('\u0B10');
			break;
		}
		case '\u0b13':  case '\u0b14':  case '\u0b15':  case '\u0b16':
		case '\u0b17':  case '\u0b18':  case '\u0b19':  case '\u0b1a':
		case '\u0b1b':  case '\u0b1c':  case '\u0b1d':  case '\u0b1e':
		case '\u0b1f':  case '\u0b20':  case '\u0b21':  case '\u0b22':
		case '\u0b23':  case '\u0b24':  case '\u0b25':  case '\u0b26':
		case '\u0b27':  case '\u0b28':
		{
			matchRange('\u0B13','\u0B28');
			break;
		}
		case '\u0b2a':  case '\u0b2b':  case '\u0b2c':  case '\u0b2d':
		case '\u0b2e':  case '\u0b2f':  case '\u0b30':
		{
			matchRange('\u0B2A','\u0B30');
			break;
		}
		case '\u0b32':
		{
			match('\u0B32');
			break;
		}
		case '\u0b33':
		{
			match('\u0B33');
			break;
		}
		case '\u0b35':  case '\u0b36':  case '\u0b37':  case '\u0b38':
		case '\u0b39':
		{
			matchRange('\u0B35','\u0B39');
			break;
		}
		case '\u0b3d':
		{
			match('\u0B3D');
			break;
		}
		case '\u0b5c':
		{
			match('\u0B5C');
			break;
		}
		case '\u0b5d':
		{
			match('\u0B5D');
			break;
		}
		case '\u0b5f':  case '\u0b60':  case '\u0b61':
		{
			matchRange('\u0B5F','\u0B61');
			break;
		}
		case '\u0b71':
		{
			match('\u0B71');
			break;
		}
		case '\u0b83':
		{
			match('\u0B83');
			break;
		}
		case '\u0b85':  case '\u0b86':  case '\u0b87':  case '\u0b88':
		case '\u0b89':  case '\u0b8a':
		{
			matchRange('\u0B85','\u0B8A');
			break;
		}
		case '\u0b8e':  case '\u0b8f':  case '\u0b90':
		{
			matchRange('\u0B8E','\u0B90');
			break;
		}
		case '\u0b92':  case '\u0b93':  case '\u0b94':  case '\u0b95':
		{
			matchRange('\u0B92','\u0B95');
			break;
		}
		case '\u0b99':
		{
			match('\u0B99');
			break;
		}
		case '\u0b9a':
		{
			match('\u0B9A');
			break;
		}
		case '\u0b9c':
		{
			match('\u0B9C');
			break;
		}
		case '\u0b9e':
		{
			match('\u0B9E');
			break;
		}
		case '\u0b9f':
		{
			match('\u0B9F');
			break;
		}
		case '\u0ba3':
		{
			match('\u0BA3');
			break;
		}
		case '\u0ba4':
		{
			match('\u0BA4');
			break;
		}
		case '\u0ba8':  case '\u0ba9':  case '\u0baa':
		{
			matchRange('\u0BA8','\u0BAA');
			break;
		}
		case '\u0bae':  case '\u0baf':  case '\u0bb0':  case '\u0bb1':
		case '\u0bb2':  case '\u0bb3':  case '\u0bb4':  case '\u0bb5':
		case '\u0bb6':  case '\u0bb7':  case '\u0bb8':  case '\u0bb9':
		{
			matchRange('\u0BAE','\u0BB9');
			break;
		}
		case '\u0bd0':
		{
			match('\u0BD0');
			break;
		}
		case '\u0c05':  case '\u0c06':  case '\u0c07':  case '\u0c08':
		case '\u0c09':  case '\u0c0a':  case '\u0c0b':  case '\u0c0c':
		{
			matchRange('\u0C05','\u0C0C');
			break;
		}
		case '\u0c0e':  case '\u0c0f':  case '\u0c10':
		{
			matchRange('\u0C0E','\u0C10');
			break;
		}
		case '\u0c12':  case '\u0c13':  case '\u0c14':  case '\u0c15':
		case '\u0c16':  case '\u0c17':  case '\u0c18':  case '\u0c19':
		case '\u0c1a':  case '\u0c1b':  case '\u0c1c':  case '\u0c1d':
		case '\u0c1e':  case '\u0c1f':  case '\u0c20':  case '\u0c21':
		case '\u0c22':  case '\u0c23':  case '\u0c24':  case '\u0c25':
		case '\u0c26':  case '\u0c27':  case '\u0c28':
		{
			matchRange('\u0C12','\u0C28');
			break;
		}
		case '\u0c2a':  case '\u0c2b':  case '\u0c2c':  case '\u0c2d':
		case '\u0c2e':  case '\u0c2f':  case '\u0c30':  case '\u0c31':
		case '\u0c32':  case '\u0c33':
		{
			matchRange('\u0C2A','\u0C33');
			break;
		}
		case '\u0c35':  case '\u0c36':  case '\u0c37':  case '\u0c38':
		case '\u0c39':
		{
			matchRange('\u0C35','\u0C39');
			break;
		}
		case '\u0c3d':
		{
			match('\u0C3D');
			break;
		}
		case '\u0c58':
		{
			match('\u0C58');
			break;
		}
		case '\u0c59':
		{
			match('\u0C59');
			break;
		}
		case '\u0c60':
		{
			match('\u0C60');
			break;
		}
		case '\u0c61':
		{
			match('\u0C61');
			break;
		}
		case '\u0c85':  case '\u0c86':  case '\u0c87':  case '\u0c88':
		case '\u0c89':  case '\u0c8a':  case '\u0c8b':  case '\u0c8c':
		{
			matchRange('\u0C85','\u0C8C');
			break;
		}
		case '\u0c8e':  case '\u0c8f':  case '\u0c90':
		{
			matchRange('\u0C8E','\u0C90');
			break;
		}
		case '\u0c92':  case '\u0c93':  case '\u0c94':  case '\u0c95':
		case '\u0c96':  case '\u0c97':  case '\u0c98':  case '\u0c99':
		case '\u0c9a':  case '\u0c9b':  case '\u0c9c':  case '\u0c9d':
		case '\u0c9e':  case '\u0c9f':  case '\u0ca0':  case '\u0ca1':
		case '\u0ca2':  case '\u0ca3':  case '\u0ca4':  case '\u0ca5':
		case '\u0ca6':  case '\u0ca7':  case '\u0ca8':
		{
			matchRange('\u0C92','\u0CA8');
			break;
		}
		case '\u0caa':  case '\u0cab':  case '\u0cac':  case '\u0cad':
		case '\u0cae':  case '\u0caf':  case '\u0cb0':  case '\u0cb1':
		case '\u0cb2':  case '\u0cb3':
		{
			matchRange('\u0CAA','\u0CB3');
			break;
		}
		case '\u0cb5':  case '\u0cb6':  case '\u0cb7':  case '\u0cb8':
		case '\u0cb9':
		{
			matchRange('\u0CB5','\u0CB9');
			break;
		}
		case '\u0cbd':
		{
			match('\u0CBD');
			break;
		}
		case '\u0cde':
		{
			match('\u0CDE');
			break;
		}
		case '\u0ce0':
		{
			match('\u0CE0');
			break;
		}
		case '\u0ce1':
		{
			match('\u0CE1');
			break;
		}
		case '\u0cf1':
		{
			match('\u0CF1');
			break;
		}
		case '\u0cf2':
		{
			match('\u0CF2');
			break;
		}
		case '\u0d05':  case '\u0d06':  case '\u0d07':  case '\u0d08':
		case '\u0d09':  case '\u0d0a':  case '\u0d0b':  case '\u0d0c':
		{
			matchRange('\u0D05','\u0D0C');
			break;
		}
		case '\u0d0e':  case '\u0d0f':  case '\u0d10':
		{
			matchRange('\u0D0E','\u0D10');
			break;
		}
		case '\u0d12':  case '\u0d13':  case '\u0d14':  case '\u0d15':
		case '\u0d16':  case '\u0d17':  case '\u0d18':  case '\u0d19':
		case '\u0d1a':  case '\u0d1b':  case '\u0d1c':  case '\u0d1d':
		case '\u0d1e':  case '\u0d1f':  case '\u0d20':  case '\u0d21':
		case '\u0d22':  case '\u0d23':  case '\u0d24':  case '\u0d25':
		case '\u0d26':  case '\u0d27':  case '\u0d28':  case '\u0d29':
		case '\u0d2a':  case '\u0d2b':  case '\u0d2c':  case '\u0d2d':
		case '\u0d2e':  case '\u0d2f':  case '\u0d30':  case '\u0d31':
		case '\u0d32':  case '\u0d33':  case '\u0d34':  case '\u0d35':
		case '\u0d36':  case '\u0d37':  case '\u0d38':  case '\u0d39':
		case '\u0d3a':
		{
			matchRange('\u0D12','\u0D3A');
			break;
		}
		case '\u0d3d':
		{
			match('\u0D3D');
			break;
		}
		case '\u0d4e':
		{
			match('\u0D4E');
			break;
		}
		case '\u0d60':
		{
			match('\u0D60');
			break;
		}
		case '\u0d61':
		{
			match('\u0D61');
			break;
		}
		case '\u0d7a':  case '\u0d7b':  case '\u0d7c':  case '\u0d7d':
		case '\u0d7e':  case '\u0d7f':
		{
			matchRange('\u0D7A','\u0D7F');
			break;
		}
		case '\u0d85':  case '\u0d86':  case '\u0d87':  case '\u0d88':
		case '\u0d89':  case '\u0d8a':  case '\u0d8b':  case '\u0d8c':
		case '\u0d8d':  case '\u0d8e':  case '\u0d8f':  case '\u0d90':
		case '\u0d91':  case '\u0d92':  case '\u0d93':  case '\u0d94':
		case '\u0d95':  case '\u0d96':
		{
			matchRange('\u0D85','\u0D96');
			break;
		}
		case '\u0d9a':  case '\u0d9b':  case '\u0d9c':  case '\u0d9d':
		case '\u0d9e':  case '\u0d9f':  case '\u0da0':  case '\u0da1':
		case '\u0da2':  case '\u0da3':  case '\u0da4':  case '\u0da5':
		case '\u0da6':  case '\u0da7':  case '\u0da8':  case '\u0da9':
		case '\u0daa':  case '\u0dab':  case '\u0dac':  case '\u0dad':
		case '\u0dae':  case '\u0daf':  case '\u0db0':  case '\u0db1':
		{
			matchRange('\u0D9A','\u0DB1');
			break;
		}
		case '\u0db3':  case '\u0db4':  case '\u0db5':  case '\u0db6':
		case '\u0db7':  case '\u0db8':  case '\u0db9':  case '\u0dba':
		case '\u0dbb':
		{
			matchRange('\u0DB3','\u0DBB');
			break;
		}
		case '\u0dbd':
		{
			match('\u0DBD');
			break;
		}
		case '\u0dc0':  case '\u0dc1':  case '\u0dc2':  case '\u0dc3':
		case '\u0dc4':  case '\u0dc5':  case '\u0dc6':
		{
			matchRange('\u0DC0','\u0DC6');
			break;
		}
		case '\u0e01':  case '\u0e02':  case '\u0e03':  case '\u0e04':
		case '\u0e05':  case '\u0e06':  case '\u0e07':  case '\u0e08':
		case '\u0e09':  case '\u0e0a':  case '\u0e0b':  case '\u0e0c':
		case '\u0e0d':  case '\u0e0e':  case '\u0e0f':  case '\u0e10':
		case '\u0e11':  case '\u0e12':  case '\u0e13':  case '\u0e14':
		case '\u0e15':  case '\u0e16':  case '\u0e17':  case '\u0e18':
		case '\u0e19':  case '\u0e1a':  case '\u0e1b':  case '\u0e1c':
		case '\u0e1d':  case '\u0e1e':  case '\u0e1f':  case '\u0e20':
		case '\u0e21':  case '\u0e22':  case '\u0e23':  case '\u0e24':
		case '\u0e25':  case '\u0e26':  case '\u0e27':  case '\u0e28':
		case '\u0e29':  case '\u0e2a':  case '\u0e2b':  case '\u0e2c':
		case '\u0e2d':  case '\u0e2e':  case '\u0e2f':  case '\u0e30':
		{
			matchRange('\u0E01','\u0E30');
			break;
		}
		case '\u0e32':
		{
			match('\u0E32');
			break;
		}
		case '\u0e33':
		{
			match('\u0E33');
			break;
		}
		case '\u0e40':  case '\u0e41':  case '\u0e42':  case '\u0e43':
		case '\u0e44':  case '\u0e45':
		{
			matchRange('\u0E40','\u0E45');
			break;
		}
		case '\u0e81':
		{
			match('\u0E81');
			break;
		}
		case '\u0e82':
		{
			match('\u0E82');
			break;
		}
		case '\u0e84':
		{
			match('\u0E84');
			break;
		}
		case '\u0e87':
		{
			match('\u0E87');
			break;
		}
		case '\u0e88':
		{
			match('\u0E88');
			break;
		}
		case '\u0e8a':
		{
			match('\u0E8A');
			break;
		}
		case '\u0e8d':
		{
			match('\u0E8D');
			break;
		}
		case '\u0e94':  case '\u0e95':  case '\u0e96':  case '\u0e97':
		{
			matchRange('\u0E94','\u0E97');
			break;
		}
		case '\u0e99':  case '\u0e9a':  case '\u0e9b':  case '\u0e9c':
		case '\u0e9d':  case '\u0e9e':  case '\u0e9f':
		{
			matchRange('\u0E99','\u0E9F');
			break;
		}
		case '\u0ea1':  case '\u0ea2':  case '\u0ea3':
		{
			matchRange('\u0EA1','\u0EA3');
			break;
		}
		case '\u0ea5':
		{
			match('\u0EA5');
			break;
		}
		case '\u0ea7':
		{
			match('\u0EA7');
			break;
		}
		case '\u0eaa':
		{
			match('\u0EAA');
			break;
		}
		case '\u0eab':
		{
			match('\u0EAB');
			break;
		}
		case '\u0ead':  case '\u0eae':  case '\u0eaf':  case '\u0eb0':
		{
			matchRange('\u0EAD','\u0EB0');
			break;
		}
		case '\u0eb2':
		{
			match('\u0EB2');
			break;
		}
		case '\u0eb3':
		{
			match('\u0EB3');
			break;
		}
		case '\u0ebd':
		{
			match('\u0EBD');
			break;
		}
		case '\u0ec0':  case '\u0ec1':  case '\u0ec2':  case '\u0ec3':
		case '\u0ec4':
		{
			matchRange('\u0EC0','\u0EC4');
			break;
		}
		case '\u0edc':  case '\u0edd':  case '\u0ede':  case '\u0edf':
		{
			matchRange('\u0EDC','\u0EDF');
			break;
		}
		case '\u0f00':
		{
			match('\u0F00');
			break;
		}
		case '\u0f40':  case '\u0f41':  case '\u0f42':  case '\u0f43':
		case '\u0f44':  case '\u0f45':  case '\u0f46':  case '\u0f47':
		{
			matchRange('\u0F40','\u0F47');
			break;
		}
		case '\u0f49':  case '\u0f4a':  case '\u0f4b':  case '\u0f4c':
		case '\u0f4d':  case '\u0f4e':  case '\u0f4f':  case '\u0f50':
		case '\u0f51':  case '\u0f52':  case '\u0f53':  case '\u0f54':
		case '\u0f55':  case '\u0f56':  case '\u0f57':  case '\u0f58':
		case '\u0f59':  case '\u0f5a':  case '\u0f5b':  case '\u0f5c':
		case '\u0f5d':  case '\u0f5e':  case '\u0f5f':  case '\u0f60':
		case '\u0f61':  case '\u0f62':  case '\u0f63':  case '\u0f64':
		case '\u0f65':  case '\u0f66':  case '\u0f67':  case '\u0f68':
		case '\u0f69':  case '\u0f6a':  case '\u0f6b':  case '\u0f6c':
		{
			matchRange('\u0F49','\u0F6C');
			break;
		}
		case '\u0f88':  case '\u0f89':  case '\u0f8a':  case '\u0f8b':
		case '\u0f8c':
		{
			matchRange('\u0F88','\u0F8C');
			break;
		}
		case '\u1000':  case '\u1001':  case '\u1002':  case '\u1003':
		case '\u1004':  case '\u1005':  case '\u1006':  case '\u1007':
		case '\u1008':  case '\u1009':  case '\u100a':  case '\u100b':
		case '\u100c':  case '\u100d':  case '\u100e':  case '\u100f':
		case '\u1010':  case '\u1011':  case '\u1012':  case '\u1013':
		case '\u1014':  case '\u1015':  case '\u1016':  case '\u1017':
		case '\u1018':  case '\u1019':  case '\u101a':  case '\u101b':
		case '\u101c':  case '\u101d':  case '\u101e':  case '\u101f':
		case '\u1020':  case '\u1021':  case '\u1022':  case '\u1023':
		case '\u1024':  case '\u1025':  case '\u1026':  case '\u1027':
		case '\u1028':  case '\u1029':  case '\u102a':
		{
			matchRange('\u1000','\u102A');
			break;
		}
		case '\u103f':
		{
			match('\u103F');
			break;
		}
		case '\u1050':  case '\u1051':  case '\u1052':  case '\u1053':
		case '\u1054':  case '\u1055':
		{
			matchRange('\u1050','\u1055');
			break;
		}
		case '\u105a':  case '\u105b':  case '\u105c':  case '\u105d':
		{
			matchRange('\u105A','\u105D');
			break;
		}
		case '\u1061':
		{
			match('\u1061');
			break;
		}
		case '\u1065':
		{
			match('\u1065');
			break;
		}
		case '\u1066':
		{
			match('\u1066');
			break;
		}
		case '\u106e':  case '\u106f':  case '\u1070':
		{
			matchRange('\u106E','\u1070');
			break;
		}
		case '\u1075':  case '\u1076':  case '\u1077':  case '\u1078':
		case '\u1079':  case '\u107a':  case '\u107b':  case '\u107c':
		case '\u107d':  case '\u107e':  case '\u107f':  case '\u1080':
		case '\u1081':
		{
			matchRange('\u1075','\u1081');
			break;
		}
		case '\u108e':
		{
			match('\u108E');
			break;
		}
		case '\u10d0':  case '\u10d1':  case '\u10d2':  case '\u10d3':
		case '\u10d4':  case '\u10d5':  case '\u10d6':  case '\u10d7':
		case '\u10d8':  case '\u10d9':  case '\u10da':  case '\u10db':
		case '\u10dc':  case '\u10dd':  case '\u10de':  case '\u10df':
		case '\u10e0':  case '\u10e1':  case '\u10e2':  case '\u10e3':
		case '\u10e4':  case '\u10e5':  case '\u10e6':  case '\u10e7':
		case '\u10e8':  case '\u10e9':  case '\u10ea':  case '\u10eb':
		case '\u10ec':  case '\u10ed':  case '\u10ee':  case '\u10ef':
		case '\u10f0':  case '\u10f1':  case '\u10f2':  case '\u10f3':
		case '\u10f4':  case '\u10f5':  case '\u10f6':  case '\u10f7':
		case '\u10f8':  case '\u10f9':  case '\u10fa':
		{
			matchRange('\u10D0','\u10FA');
			break;
		}
		case '\u124a':  case '\u124b':  case '\u124c':  case '\u124d':
		{
			matchRange('\u124A','\u124D');
			break;
		}
		case '\u1250':  case '\u1251':  case '\u1252':  case '\u1253':
		case '\u1254':  case '\u1255':  case '\u1256':
		{
			matchRange('\u1250','\u1256');
			break;
		}
		case '\u1258':
		{
			match('\u1258');
			break;
		}
		case '\u125a':  case '\u125b':  case '\u125c':  case '\u125d':
		{
			matchRange('\u125A','\u125D');
			break;
		}
		case '\u1260':  case '\u1261':  case '\u1262':  case '\u1263':
		case '\u1264':  case '\u1265':  case '\u1266':  case '\u1267':
		case '\u1268':  case '\u1269':  case '\u126a':  case '\u126b':
		case '\u126c':  case '\u126d':  case '\u126e':  case '\u126f':
		case '\u1270':  case '\u1271':  case '\u1272':  case '\u1273':
		case '\u1274':  case '\u1275':  case '\u1276':  case '\u1277':
		case '\u1278':  case '\u1279':  case '\u127a':  case '\u127b':
		case '\u127c':  case '\u127d':  case '\u127e':  case '\u127f':
		case '\u1280':  case '\u1281':  case '\u1282':  case '\u1283':
		case '\u1284':  case '\u1285':  case '\u1286':  case '\u1287':
		case '\u1288':
		{
			matchRange('\u1260','\u1288');
			break;
		}
		case '\u128a':  case '\u128b':  case '\u128c':  case '\u128d':
		{
			matchRange('\u128A','\u128D');
			break;
		}
		case '\u1290':  case '\u1291':  case '\u1292':  case '\u1293':
		case '\u1294':  case '\u1295':  case '\u1296':  case '\u1297':
		case '\u1298':  case '\u1299':  case '\u129a':  case '\u129b':
		case '\u129c':  case '\u129d':  case '\u129e':  case '\u129f':
		case '\u12a0':  case '\u12a1':  case '\u12a2':  case '\u12a3':
		case '\u12a4':  case '\u12a5':  case '\u12a6':  case '\u12a7':
		case '\u12a8':  case '\u12a9':  case '\u12aa':  case '\u12ab':
		case '\u12ac':  case '\u12ad':  case '\u12ae':  case '\u12af':
		case '\u12b0':
		{
			matchRange('\u1290','\u12B0');
			break;
		}
		case '\u12b2':  case '\u12b3':  case '\u12b4':  case '\u12b5':
		{
			matchRange('\u12B2','\u12B5');
			break;
		}
		case '\u12b8':  case '\u12b9':  case '\u12ba':  case '\u12bb':
		case '\u12bc':  case '\u12bd':  case '\u12be':
		{
			matchRange('\u12B8','\u12BE');
			break;
		}
		case '\u12c0':
		{
			match('\u12C0');
			break;
		}
		case '\u12c2':  case '\u12c3':  case '\u12c4':  case '\u12c5':
		{
			matchRange('\u12C2','\u12C5');
			break;
		}
		case '\u12c8':  case '\u12c9':  case '\u12ca':  case '\u12cb':
		case '\u12cc':  case '\u12cd':  case '\u12ce':  case '\u12cf':
		case '\u12d0':  case '\u12d1':  case '\u12d2':  case '\u12d3':
		case '\u12d4':  case '\u12d5':  case '\u12d6':
		{
			matchRange('\u12C8','\u12D6');
			break;
		}
		case '\u12d8':  case '\u12d9':  case '\u12da':  case '\u12db':
		case '\u12dc':  case '\u12dd':  case '\u12de':  case '\u12df':
		case '\u12e0':  case '\u12e1':  case '\u12e2':  case '\u12e3':
		case '\u12e4':  case '\u12e5':  case '\u12e6':  case '\u12e7':
		case '\u12e8':  case '\u12e9':  case '\u12ea':  case '\u12eb':
		case '\u12ec':  case '\u12ed':  case '\u12ee':  case '\u12ef':
		case '\u12f0':  case '\u12f1':  case '\u12f2':  case '\u12f3':
		case '\u12f4':  case '\u12f5':  case '\u12f6':  case '\u12f7':
		case '\u12f8':  case '\u12f9':  case '\u12fa':  case '\u12fb':
		case '\u12fc':  case '\u12fd':  case '\u12fe':  case '\u12ff':
		case '\u1300':  case '\u1301':  case '\u1302':  case '\u1303':
		case '\u1304':  case '\u1305':  case '\u1306':  case '\u1307':
		case '\u1308':  case '\u1309':  case '\u130a':  case '\u130b':
		case '\u130c':  case '\u130d':  case '\u130e':  case '\u130f':
		case '\u1310':
		{
			matchRange('\u12D8','\u1310');
			break;
		}
		case '\u1312':  case '\u1313':  case '\u1314':  case '\u1315':
		{
			matchRange('\u1312','\u1315');
			break;
		}
		case '\u1318':  case '\u1319':  case '\u131a':  case '\u131b':
		case '\u131c':  case '\u131d':  case '\u131e':  case '\u131f':
		case '\u1320':  case '\u1321':  case '\u1322':  case '\u1323':
		case '\u1324':  case '\u1325':  case '\u1326':  case '\u1327':
		case '\u1328':  case '\u1329':  case '\u132a':  case '\u132b':
		case '\u132c':  case '\u132d':  case '\u132e':  case '\u132f':
		case '\u1330':  case '\u1331':  case '\u1332':  case '\u1333':
		case '\u1334':  case '\u1335':  case '\u1336':  case '\u1337':
		case '\u1338':  case '\u1339':  case '\u133a':  case '\u133b':
		case '\u133c':  case '\u133d':  case '\u133e':  case '\u133f':
		case '\u1340':  case '\u1341':  case '\u1342':  case '\u1343':
		case '\u1344':  case '\u1345':  case '\u1346':  case '\u1347':
		case '\u1348':  case '\u1349':  case '\u134a':  case '\u134b':
		case '\u134c':  case '\u134d':  case '\u134e':  case '\u134f':
		case '\u1350':  case '\u1351':  case '\u1352':  case '\u1353':
		case '\u1354':  case '\u1355':  case '\u1356':  case '\u1357':
		case '\u1358':  case '\u1359':  case '\u135a':
		{
			matchRange('\u1318','\u135A');
			break;
		}
		case '\u1380':  case '\u1381':  case '\u1382':  case '\u1383':
		case '\u1384':  case '\u1385':  case '\u1386':  case '\u1387':
		case '\u1388':  case '\u1389':  case '\u138a':  case '\u138b':
		case '\u138c':  case '\u138d':  case '\u138e':  case '\u138f':
		{
			matchRange('\u1380','\u138F');
			break;
		}
		case '\u13a0':  case '\u13a1':  case '\u13a2':  case '\u13a3':
		case '\u13a4':  case '\u13a5':  case '\u13a6':  case '\u13a7':
		case '\u13a8':  case '\u13a9':  case '\u13aa':  case '\u13ab':
		case '\u13ac':  case '\u13ad':  case '\u13ae':  case '\u13af':
		case '\u13b0':  case '\u13b1':  case '\u13b2':  case '\u13b3':
		case '\u13b4':  case '\u13b5':  case '\u13b6':  case '\u13b7':
		case '\u13b8':  case '\u13b9':  case '\u13ba':  case '\u13bb':
		case '\u13bc':  case '\u13bd':  case '\u13be':  case '\u13bf':
		case '\u13c0':  case '\u13c1':  case '\u13c2':  case '\u13c3':
		case '\u13c4':  case '\u13c5':  case '\u13c6':  case '\u13c7':
		case '\u13c8':  case '\u13c9':  case '\u13ca':  case '\u13cb':
		case '\u13cc':  case '\u13cd':  case '\u13ce':  case '\u13cf':
		case '\u13d0':  case '\u13d1':  case '\u13d2':  case '\u13d3':
		case '\u13d4':  case '\u13d5':  case '\u13d6':  case '\u13d7':
		case '\u13d8':  case '\u13d9':  case '\u13da':  case '\u13db':
		case '\u13dc':  case '\u13dd':  case '\u13de':  case '\u13df':
		case '\u13e0':  case '\u13e1':  case '\u13e2':  case '\u13e3':
		case '\u13e4':  case '\u13e5':  case '\u13e6':  case '\u13e7':
		case '\u13e8':  case '\u13e9':  case '\u13ea':  case '\u13eb':
		case '\u13ec':  case '\u13ed':  case '\u13ee':  case '\u13ef':
		case '\u13f0':  case '\u13f1':  case '\u13f2':  case '\u13f3':
		case '\u13f4':
		{
			matchRange('\u13A0','\u13F4');
			break;
		}
		case '\u166f':  case '\u1670':  case '\u1671':  case '\u1672':
		case '\u1673':  case '\u1674':  case '\u1675':  case '\u1676':
		case '\u1677':  case '\u1678':  case '\u1679':  case '\u167a':
		case '\u167b':  case '\u167c':  case '\u167d':  case '\u167e':
		case '\u167f':
		{
			matchRange('\u166F','\u167F');
			break;
		}
		case '\u1681':  case '\u1682':  case '\u1683':  case '\u1684':
		case '\u1685':  case '\u1686':  case '\u1687':  case '\u1688':
		case '\u1689':  case '\u168a':  case '\u168b':  case '\u168c':
		case '\u168d':  case '\u168e':  case '\u168f':  case '\u1690':
		case '\u1691':  case '\u1692':  case '\u1693':  case '\u1694':
		case '\u1695':  case '\u1696':  case '\u1697':  case '\u1698':
		case '\u1699':  case '\u169a':
		{
			matchRange('\u1681','\u169A');
			break;
		}
		case '\u16a0':  case '\u16a1':  case '\u16a2':  case '\u16a3':
		case '\u16a4':  case '\u16a5':  case '\u16a6':  case '\u16a7':
		case '\u16a8':  case '\u16a9':  case '\u16aa':  case '\u16ab':
		case '\u16ac':  case '\u16ad':  case '\u16ae':  case '\u16af':
		case '\u16b0':  case '\u16b1':  case '\u16b2':  case '\u16b3':
		case '\u16b4':  case '\u16b5':  case '\u16b6':  case '\u16b7':
		case '\u16b8':  case '\u16b9':  case '\u16ba':  case '\u16bb':
		case '\u16bc':  case '\u16bd':  case '\u16be':  case '\u16bf':
		case '\u16c0':  case '\u16c1':  case '\u16c2':  case '\u16c3':
		case '\u16c4':  case '\u16c5':  case '\u16c6':  case '\u16c7':
		case '\u16c8':  case '\u16c9':  case '\u16ca':  case '\u16cb':
		case '\u16cc':  case '\u16cd':  case '\u16ce':  case '\u16cf':
		case '\u16d0':  case '\u16d1':  case '\u16d2':  case '\u16d3':
		case '\u16d4':  case '\u16d5':  case '\u16d6':  case '\u16d7':
		case '\u16d8':  case '\u16d9':  case '\u16da':  case '\u16db':
		case '\u16dc':  case '\u16dd':  case '\u16de':  case '\u16df':
		case '\u16e0':  case '\u16e1':  case '\u16e2':  case '\u16e3':
		case '\u16e4':  case '\u16e5':  case '\u16e6':  case '\u16e7':
		case '\u16e8':  case '\u16e9':  case '\u16ea':
		{
			matchRange('\u16A0','\u16EA');
			break;
		}
		case '\u1700':  case '\u1701':  case '\u1702':  case '\u1703':
		case '\u1704':  case '\u1705':  case '\u1706':  case '\u1707':
		case '\u1708':  case '\u1709':  case '\u170a':  case '\u170b':
		case '\u170c':
		{
			matchRange('\u1700','\u170C');
			break;
		}
		case '\u170e':  case '\u170f':  case '\u1710':  case '\u1711':
		{
			matchRange('\u170E','\u1711');
			break;
		}
		case '\u1720':  case '\u1721':  case '\u1722':  case '\u1723':
		case '\u1724':  case '\u1725':  case '\u1726':  case '\u1727':
		case '\u1728':  case '\u1729':  case '\u172a':  case '\u172b':
		case '\u172c':  case '\u172d':  case '\u172e':  case '\u172f':
		case '\u1730':  case '\u1731':
		{
			matchRange('\u1720','\u1731');
			break;
		}
		case '\u1740':  case '\u1741':  case '\u1742':  case '\u1743':
		case '\u1744':  case '\u1745':  case '\u1746':  case '\u1747':
		case '\u1748':  case '\u1749':  case '\u174a':  case '\u174b':
		case '\u174c':  case '\u174d':  case '\u174e':  case '\u174f':
		case '\u1750':  case '\u1751':
		{
			matchRange('\u1740','\u1751');
			break;
		}
		case '\u1760':  case '\u1761':  case '\u1762':  case '\u1763':
		case '\u1764':  case '\u1765':  case '\u1766':  case '\u1767':
		case '\u1768':  case '\u1769':  case '\u176a':  case '\u176b':
		case '\u176c':
		{
			matchRange('\u1760','\u176C');
			break;
		}
		case '\u176e':  case '\u176f':  case '\u1770':
		{
			matchRange('\u176E','\u1770');
			break;
		}
		case '\u1780':  case '\u1781':  case '\u1782':  case '\u1783':
		case '\u1784':  case '\u1785':  case '\u1786':  case '\u1787':
		case '\u1788':  case '\u1789':  case '\u178a':  case '\u178b':
		case '\u178c':  case '\u178d':  case '\u178e':  case '\u178f':
		case '\u1790':  case '\u1791':  case '\u1792':  case '\u1793':
		case '\u1794':  case '\u1795':  case '\u1796':  case '\u1797':
		case '\u1798':  case '\u1799':  case '\u179a':  case '\u179b':
		case '\u179c':  case '\u179d':  case '\u179e':  case '\u179f':
		case '\u17a0':  case '\u17a1':  case '\u17a2':  case '\u17a3':
		case '\u17a4':  case '\u17a5':  case '\u17a6':  case '\u17a7':
		case '\u17a8':  case '\u17a9':  case '\u17aa':  case '\u17ab':
		case '\u17ac':  case '\u17ad':  case '\u17ae':  case '\u17af':
		case '\u17b0':  case '\u17b1':  case '\u17b2':  case '\u17b3':
		{
			matchRange('\u1780','\u17B3');
			break;
		}
		case '\u17dc':
		{
			match('\u17DC');
			break;
		}
		case '\u1820':  case '\u1821':  case '\u1822':  case '\u1823':
		case '\u1824':  case '\u1825':  case '\u1826':  case '\u1827':
		case '\u1828':  case '\u1829':  case '\u182a':  case '\u182b':
		case '\u182c':  case '\u182d':  case '\u182e':  case '\u182f':
		case '\u1830':  case '\u1831':  case '\u1832':  case '\u1833':
		case '\u1834':  case '\u1835':  case '\u1836':  case '\u1837':
		case '\u1838':  case '\u1839':  case '\u183a':  case '\u183b':
		case '\u183c':  case '\u183d':  case '\u183e':  case '\u183f':
		case '\u1840':  case '\u1841':  case '\u1842':
		{
			matchRange('\u1820','\u1842');
			break;
		}
		case '\u1844':  case '\u1845':  case '\u1846':  case '\u1847':
		case '\u1848':  case '\u1849':  case '\u184a':  case '\u184b':
		case '\u184c':  case '\u184d':  case '\u184e':  case '\u184f':
		case '\u1850':  case '\u1851':  case '\u1852':  case '\u1853':
		case '\u1854':  case '\u1855':  case '\u1856':  case '\u1857':
		case '\u1858':  case '\u1859':  case '\u185a':  case '\u185b':
		case '\u185c':  case '\u185d':  case '\u185e':  case '\u185f':
		case '\u1860':  case '\u1861':  case '\u1862':  case '\u1863':
		case '\u1864':  case '\u1865':  case '\u1866':  case '\u1867':
		case '\u1868':  case '\u1869':  case '\u186a':  case '\u186b':
		case '\u186c':  case '\u186d':  case '\u186e':  case '\u186f':
		case '\u1870':  case '\u1871':  case '\u1872':  case '\u1873':
		case '\u1874':  case '\u1875':  case '\u1876':  case '\u1877':
		{
			matchRange('\u1844','\u1877');
			break;
		}
		case '\u1880':  case '\u1881':  case '\u1882':  case '\u1883':
		case '\u1884':  case '\u1885':  case '\u1886':  case '\u1887':
		case '\u1888':  case '\u1889':  case '\u188a':  case '\u188b':
		case '\u188c':  case '\u188d':  case '\u188e':  case '\u188f':
		case '\u1890':  case '\u1891':  case '\u1892':  case '\u1893':
		case '\u1894':  case '\u1895':  case '\u1896':  case '\u1897':
		case '\u1898':  case '\u1899':  case '\u189a':  case '\u189b':
		case '\u189c':  case '\u189d':  case '\u189e':  case '\u189f':
		case '\u18a0':  case '\u18a1':  case '\u18a2':  case '\u18a3':
		case '\u18a4':  case '\u18a5':  case '\u18a6':  case '\u18a7':
		case '\u18a8':
		{
			matchRange('\u1880','\u18A8');
			break;
		}
		case '\u18aa':
		{
			match('\u18AA');
			break;
		}
		case '\u18b0':  case '\u18b1':  case '\u18b2':  case '\u18b3':
		case '\u18b4':  case '\u18b5':  case '\u18b6':  case '\u18b7':
		case '\u18b8':  case '\u18b9':  case '\u18ba':  case '\u18bb':
		case '\u18bc':  case '\u18bd':  case '\u18be':  case '\u18bf':
		case '\u18c0':  case '\u18c1':  case '\u18c2':  case '\u18c3':
		case '\u18c4':  case '\u18c5':  case '\u18c6':  case '\u18c7':
		case '\u18c8':  case '\u18c9':  case '\u18ca':  case '\u18cb':
		case '\u18cc':  case '\u18cd':  case '\u18ce':  case '\u18cf':
		case '\u18d0':  case '\u18d1':  case '\u18d2':  case '\u18d3':
		case '\u18d4':  case '\u18d5':  case '\u18d6':  case '\u18d7':
		case '\u18d8':  case '\u18d9':  case '\u18da':  case '\u18db':
		case '\u18dc':  case '\u18dd':  case '\u18de':  case '\u18df':
		case '\u18e0':  case '\u18e1':  case '\u18e2':  case '\u18e3':
		case '\u18e4':  case '\u18e5':  case '\u18e6':  case '\u18e7':
		case '\u18e8':  case '\u18e9':  case '\u18ea':  case '\u18eb':
		case '\u18ec':  case '\u18ed':  case '\u18ee':  case '\u18ef':
		case '\u18f0':  case '\u18f1':  case '\u18f2':  case '\u18f3':
		case '\u18f4':  case '\u18f5':
		{
			matchRange('\u18B0','\u18F5');
			break;
		}
		case '\u1900':  case '\u1901':  case '\u1902':  case '\u1903':
		case '\u1904':  case '\u1905':  case '\u1906':  case '\u1907':
		case '\u1908':  case '\u1909':  case '\u190a':  case '\u190b':
		case '\u190c':  case '\u190d':  case '\u190e':  case '\u190f':
		case '\u1910':  case '\u1911':  case '\u1912':  case '\u1913':
		case '\u1914':  case '\u1915':  case '\u1916':  case '\u1917':
		case '\u1918':  case '\u1919':  case '\u191a':  case '\u191b':
		case '\u191c':
		{
			matchRange('\u1900','\u191C');
			break;
		}
		case '\u1950':  case '\u1951':  case '\u1952':  case '\u1953':
		case '\u1954':  case '\u1955':  case '\u1956':  case '\u1957':
		case '\u1958':  case '\u1959':  case '\u195a':  case '\u195b':
		case '\u195c':  case '\u195d':  case '\u195e':  case '\u195f':
		case '\u1960':  case '\u1961':  case '\u1962':  case '\u1963':
		case '\u1964':  case '\u1965':  case '\u1966':  case '\u1967':
		case '\u1968':  case '\u1969':  case '\u196a':  case '\u196b':
		case '\u196c':  case '\u196d':
		{
			matchRange('\u1950','\u196D');
			break;
		}
		case '\u1970':  case '\u1971':  case '\u1972':  case '\u1973':
		case '\u1974':
		{
			matchRange('\u1970','\u1974');
			break;
		}
		case '\u1980':  case '\u1981':  case '\u1982':  case '\u1983':
		case '\u1984':  case '\u1985':  case '\u1986':  case '\u1987':
		case '\u1988':  case '\u1989':  case '\u198a':  case '\u198b':
		case '\u198c':  case '\u198d':  case '\u198e':  case '\u198f':
		case '\u1990':  case '\u1991':  case '\u1992':  case '\u1993':
		case '\u1994':  case '\u1995':  case '\u1996':  case '\u1997':
		case '\u1998':  case '\u1999':  case '\u199a':  case '\u199b':
		case '\u199c':  case '\u199d':  case '\u199e':  case '\u199f':
		case '\u19a0':  case '\u19a1':  case '\u19a2':  case '\u19a3':
		case '\u19a4':  case '\u19a5':  case '\u19a6':  case '\u19a7':
		case '\u19a8':  case '\u19a9':  case '\u19aa':  case '\u19ab':
		{
			matchRange('\u1980','\u19AB');
			break;
		}
		case '\u19c1':  case '\u19c2':  case '\u19c3':  case '\u19c4':
		case '\u19c5':  case '\u19c6':  case '\u19c7':
		{
			matchRange('\u19C1','\u19C7');
			break;
		}
		case '\u1a00':  case '\u1a01':  case '\u1a02':  case '\u1a03':
		case '\u1a04':  case '\u1a05':  case '\u1a06':  case '\u1a07':
		case '\u1a08':  case '\u1a09':  case '\u1a0a':  case '\u1a0b':
		case '\u1a0c':  case '\u1a0d':  case '\u1a0e':  case '\u1a0f':
		case '\u1a10':  case '\u1a11':  case '\u1a12':  case '\u1a13':
		case '\u1a14':  case '\u1a15':  case '\u1a16':
		{
			matchRange('\u1A00','\u1A16');
			break;
		}
		case '\u1a20':  case '\u1a21':  case '\u1a22':  case '\u1a23':
		case '\u1a24':  case '\u1a25':  case '\u1a26':  case '\u1a27':
		case '\u1a28':  case '\u1a29':  case '\u1a2a':  case '\u1a2b':
		case '\u1a2c':  case '\u1a2d':  case '\u1a2e':  case '\u1a2f':
		case '\u1a30':  case '\u1a31':  case '\u1a32':  case '\u1a33':
		case '\u1a34':  case '\u1a35':  case '\u1a36':  case '\u1a37':
		case '\u1a38':  case '\u1a39':  case '\u1a3a':  case '\u1a3b':
		case '\u1a3c':  case '\u1a3d':  case '\u1a3e':  case '\u1a3f':
		case '\u1a40':  case '\u1a41':  case '\u1a42':  case '\u1a43':
		case '\u1a44':  case '\u1a45':  case '\u1a46':  case '\u1a47':
		case '\u1a48':  case '\u1a49':  case '\u1a4a':  case '\u1a4b':
		case '\u1a4c':  case '\u1a4d':  case '\u1a4e':  case '\u1a4f':
		case '\u1a50':  case '\u1a51':  case '\u1a52':  case '\u1a53':
		case '\u1a54':
		{
			matchRange('\u1A20','\u1A54');
			break;
		}
		case '\u1b05':  case '\u1b06':  case '\u1b07':  case '\u1b08':
		case '\u1b09':  case '\u1b0a':  case '\u1b0b':  case '\u1b0c':
		case '\u1b0d':  case '\u1b0e':  case '\u1b0f':  case '\u1b10':
		case '\u1b11':  case '\u1b12':  case '\u1b13':  case '\u1b14':
		case '\u1b15':  case '\u1b16':  case '\u1b17':  case '\u1b18':
		case '\u1b19':  case '\u1b1a':  case '\u1b1b':  case '\u1b1c':
		case '\u1b1d':  case '\u1b1e':  case '\u1b1f':  case '\u1b20':
		case '\u1b21':  case '\u1b22':  case '\u1b23':  case '\u1b24':
		case '\u1b25':  case '\u1b26':  case '\u1b27':  case '\u1b28':
		case '\u1b29':  case '\u1b2a':  case '\u1b2b':  case '\u1b2c':
		case '\u1b2d':  case '\u1b2e':  case '\u1b2f':  case '\u1b30':
		case '\u1b31':  case '\u1b32':  case '\u1b33':
		{
			matchRange('\u1B05','\u1B33');
			break;
		}
		case '\u1b45':  case '\u1b46':  case '\u1b47':  case '\u1b48':
		case '\u1b49':  case '\u1b4a':  case '\u1b4b':
		{
			matchRange('\u1B45','\u1B4B');
			break;
		}
		case '\u1b83':  case '\u1b84':  case '\u1b85':  case '\u1b86':
		case '\u1b87':  case '\u1b88':  case '\u1b89':  case '\u1b8a':
		case '\u1b8b':  case '\u1b8c':  case '\u1b8d':  case '\u1b8e':
		case '\u1b8f':  case '\u1b90':  case '\u1b91':  case '\u1b92':
		case '\u1b93':  case '\u1b94':  case '\u1b95':  case '\u1b96':
		case '\u1b97':  case '\u1b98':  case '\u1b99':  case '\u1b9a':
		case '\u1b9b':  case '\u1b9c':  case '\u1b9d':  case '\u1b9e':
		case '\u1b9f':  case '\u1ba0':
		{
			matchRange('\u1B83','\u1BA0');
			break;
		}
		case '\u1bae':
		{
			match('\u1BAE');
			break;
		}
		case '\u1baf':
		{
			match('\u1BAF');
			break;
		}
		case '\u1bba':  case '\u1bbb':  case '\u1bbc':  case '\u1bbd':
		case '\u1bbe':  case '\u1bbf':  case '\u1bc0':  case '\u1bc1':
		case '\u1bc2':  case '\u1bc3':  case '\u1bc4':  case '\u1bc5':
		case '\u1bc6':  case '\u1bc7':  case '\u1bc8':  case '\u1bc9':
		case '\u1bca':  case '\u1bcb':  case '\u1bcc':  case '\u1bcd':
		case '\u1bce':  case '\u1bcf':  case '\u1bd0':  case '\u1bd1':
		case '\u1bd2':  case '\u1bd3':  case '\u1bd4':  case '\u1bd5':
		case '\u1bd6':  case '\u1bd7':  case '\u1bd8':  case '\u1bd9':
		case '\u1bda':  case '\u1bdb':  case '\u1bdc':  case '\u1bdd':
		case '\u1bde':  case '\u1bdf':  case '\u1be0':  case '\u1be1':
		case '\u1be2':  case '\u1be3':  case '\u1be4':  case '\u1be5':
		{
			matchRange('\u1BBA','\u1BE5');
			break;
		}
		case '\u1c00':  case '\u1c01':  case '\u1c02':  case '\u1c03':
		case '\u1c04':  case '\u1c05':  case '\u1c06':  case '\u1c07':
		case '\u1c08':  case '\u1c09':  case '\u1c0a':  case '\u1c0b':
		case '\u1c0c':  case '\u1c0d':  case '\u1c0e':  case '\u1c0f':
		case '\u1c10':  case '\u1c11':  case '\u1c12':  case '\u1c13':
		case '\u1c14':  case '\u1c15':  case '\u1c16':  case '\u1c17':
		case '\u1c18':  case '\u1c19':  case '\u1c1a':  case '\u1c1b':
		case '\u1c1c':  case '\u1c1d':  case '\u1c1e':  case '\u1c1f':
		case '\u1c20':  case '\u1c21':  case '\u1c22':  case '\u1c23':
		{
			matchRange('\u1C00','\u1C23');
			break;
		}
		case '\u1c4d':  case '\u1c4e':  case '\u1c4f':
		{
			matchRange('\u1C4D','\u1C4F');
			break;
		}
		case '\u1c5a':  case '\u1c5b':  case '\u1c5c':  case '\u1c5d':
		case '\u1c5e':  case '\u1c5f':  case '\u1c60':  case '\u1c61':
		case '\u1c62':  case '\u1c63':  case '\u1c64':  case '\u1c65':
		case '\u1c66':  case '\u1c67':  case '\u1c68':  case '\u1c69':
		case '\u1c6a':  case '\u1c6b':  case '\u1c6c':  case '\u1c6d':
		case '\u1c6e':  case '\u1c6f':  case '\u1c70':  case '\u1c71':
		case '\u1c72':  case '\u1c73':  case '\u1c74':  case '\u1c75':
		case '\u1c76':  case '\u1c77':
		{
			matchRange('\u1C5A','\u1C77');
			break;
		}
		case '\u1ce9':  case '\u1cea':  case '\u1ceb':  case '\u1cec':
		{
			matchRange('\u1CE9','\u1CEC');
			break;
		}
		case '\u1cee':  case '\u1cef':  case '\u1cf0':  case '\u1cf1':
		{
			matchRange('\u1CEE','\u1CF1');
			break;
		}
		case '\u1cf5':
		{
			match('\u1CF5');
			break;
		}
		case '\u1cf6':
		{
			match('\u1CF6');
			break;
		}
		case '\u2135':  case '\u2136':  case '\u2137':  case '\u2138':
		{
			matchRange('\u2135','\u2138');
			break;
		}
		case '\u2d30':  case '\u2d31':  case '\u2d32':  case '\u2d33':
		case '\u2d34':  case '\u2d35':  case '\u2d36':  case '\u2d37':
		case '\u2d38':  case '\u2d39':  case '\u2d3a':  case '\u2d3b':
		case '\u2d3c':  case '\u2d3d':  case '\u2d3e':  case '\u2d3f':
		case '\u2d40':  case '\u2d41':  case '\u2d42':  case '\u2d43':
		case '\u2d44':  case '\u2d45':  case '\u2d46':  case '\u2d47':
		case '\u2d48':  case '\u2d49':  case '\u2d4a':  case '\u2d4b':
		case '\u2d4c':  case '\u2d4d':  case '\u2d4e':  case '\u2d4f':
		case '\u2d50':  case '\u2d51':  case '\u2d52':  case '\u2d53':
		case '\u2d54':  case '\u2d55':  case '\u2d56':  case '\u2d57':
		case '\u2d58':  case '\u2d59':  case '\u2d5a':  case '\u2d5b':
		case '\u2d5c':  case '\u2d5d':  case '\u2d5e':  case '\u2d5f':
		case '\u2d60':  case '\u2d61':  case '\u2d62':  case '\u2d63':
		case '\u2d64':  case '\u2d65':  case '\u2d66':  case '\u2d67':
		{
			matchRange('\u2D30','\u2D67');
			break;
		}
		case '\u2d80':  case '\u2d81':  case '\u2d82':  case '\u2d83':
		case '\u2d84':  case '\u2d85':  case '\u2d86':  case '\u2d87':
		case '\u2d88':  case '\u2d89':  case '\u2d8a':  case '\u2d8b':
		case '\u2d8c':  case '\u2d8d':  case '\u2d8e':  case '\u2d8f':
		case '\u2d90':  case '\u2d91':  case '\u2d92':  case '\u2d93':
		case '\u2d94':  case '\u2d95':  case '\u2d96':
		{
			matchRange('\u2D80','\u2D96');
			break;
		}
		case '\u2da0':  case '\u2da1':  case '\u2da2':  case '\u2da3':
		case '\u2da4':  case '\u2da5':  case '\u2da6':
		{
			matchRange('\u2DA0','\u2DA6');
			break;
		}
		case '\u2da8':  case '\u2da9':  case '\u2daa':  case '\u2dab':
		case '\u2dac':  case '\u2dad':  case '\u2dae':
		{
			matchRange('\u2DA8','\u2DAE');
			break;
		}
		case '\u2db0':  case '\u2db1':  case '\u2db2':  case '\u2db3':
		case '\u2db4':  case '\u2db5':  case '\u2db6':
		{
			matchRange('\u2DB0','\u2DB6');
			break;
		}
		case '\u2db8':  case '\u2db9':  case '\u2dba':  case '\u2dbb':
		case '\u2dbc':  case '\u2dbd':  case '\u2dbe':
		{
			matchRange('\u2DB8','\u2DBE');
			break;
		}
		case '\u2dc0':  case '\u2dc1':  case '\u2dc2':  case '\u2dc3':
		case '\u2dc4':  case '\u2dc5':  case '\u2dc6':
		{
			matchRange('\u2DC0','\u2DC6');
			break;
		}
		case '\u2dc8':  case '\u2dc9':  case '\u2dca':  case '\u2dcb':
		case '\u2dcc':  case '\u2dcd':  case '\u2dce':
		{
			matchRange('\u2DC8','\u2DCE');
			break;
		}
		case '\u2dd0':  case '\u2dd1':  case '\u2dd2':  case '\u2dd3':
		case '\u2dd4':  case '\u2dd5':  case '\u2dd6':
		{
			matchRange('\u2DD0','\u2DD6');
			break;
		}
		case '\u2dd8':  case '\u2dd9':  case '\u2dda':  case '\u2ddb':
		case '\u2ddc':  case '\u2ddd':  case '\u2dde':
		{
			matchRange('\u2DD8','\u2DDE');
			break;
		}
		case '\u3006':
		{
			match('\u3006');
			break;
		}
		case '\u303c':
		{
			match('\u303C');
			break;
		}
		case '\u3041':  case '\u3042':  case '\u3043':  case '\u3044':
		case '\u3045':  case '\u3046':  case '\u3047':  case '\u3048':
		case '\u3049':  case '\u304a':  case '\u304b':  case '\u304c':
		case '\u304d':  case '\u304e':  case '\u304f':  case '\u3050':
		case '\u3051':  case '\u3052':  case '\u3053':  case '\u3054':
		case '\u3055':  case '\u3056':  case '\u3057':  case '\u3058':
		case '\u3059':  case '\u305a':  case '\u305b':  case '\u305c':
		case '\u305d':  case '\u305e':  case '\u305f':  case '\u3060':
		case '\u3061':  case '\u3062':  case '\u3063':  case '\u3064':
		case '\u3065':  case '\u3066':  case '\u3067':  case '\u3068':
		case '\u3069':  case '\u306a':  case '\u306b':  case '\u306c':
		case '\u306d':  case '\u306e':  case '\u306f':  case '\u3070':
		case '\u3071':  case '\u3072':  case '\u3073':  case '\u3074':
		case '\u3075':  case '\u3076':  case '\u3077':  case '\u3078':
		case '\u3079':  case '\u307a':  case '\u307b':  case '\u307c':
		case '\u307d':  case '\u307e':  case '\u307f':  case '\u3080':
		case '\u3081':  case '\u3082':  case '\u3083':  case '\u3084':
		case '\u3085':  case '\u3086':  case '\u3087':  case '\u3088':
		case '\u3089':  case '\u308a':  case '\u308b':  case '\u308c':
		case '\u308d':  case '\u308e':  case '\u308f':  case '\u3090':
		case '\u3091':  case '\u3092':  case '\u3093':  case '\u3094':
		case '\u3095':  case '\u3096':
		{
			matchRange('\u3041','\u3096');
			break;
		}
		case '\u309f':
		{
			match('\u309F');
			break;
		}
		case '\u30a1':  case '\u30a2':  case '\u30a3':  case '\u30a4':
		case '\u30a5':  case '\u30a6':  case '\u30a7':  case '\u30a8':
		case '\u30a9':  case '\u30aa':  case '\u30ab':  case '\u30ac':
		case '\u30ad':  case '\u30ae':  case '\u30af':  case '\u30b0':
		case '\u30b1':  case '\u30b2':  case '\u30b3':  case '\u30b4':
		case '\u30b5':  case '\u30b6':  case '\u30b7':  case '\u30b8':
		case '\u30b9':  case '\u30ba':  case '\u30bb':  case '\u30bc':
		case '\u30bd':  case '\u30be':  case '\u30bf':  case '\u30c0':
		case '\u30c1':  case '\u30c2':  case '\u30c3':  case '\u30c4':
		case '\u30c5':  case '\u30c6':  case '\u30c7':  case '\u30c8':
		case '\u30c9':  case '\u30ca':  case '\u30cb':  case '\u30cc':
		case '\u30cd':  case '\u30ce':  case '\u30cf':  case '\u30d0':
		case '\u30d1':  case '\u30d2':  case '\u30d3':  case '\u30d4':
		case '\u30d5':  case '\u30d6':  case '\u30d7':  case '\u30d8':
		case '\u30d9':  case '\u30da':  case '\u30db':  case '\u30dc':
		case '\u30dd':  case '\u30de':  case '\u30df':  case '\u30e0':
		case '\u30e1':  case '\u30e2':  case '\u30e3':  case '\u30e4':
		case '\u30e5':  case '\u30e6':  case '\u30e7':  case '\u30e8':
		case '\u30e9':  case '\u30ea':  case '\u30eb':  case '\u30ec':
		case '\u30ed':  case '\u30ee':  case '\u30ef':  case '\u30f0':
		case '\u30f1':  case '\u30f2':  case '\u30f3':  case '\u30f4':
		case '\u30f5':  case '\u30f6':  case '\u30f7':  case '\u30f8':
		case '\u30f9':  case '\u30fa':
		{
			matchRange('\u30A1','\u30FA');
			break;
		}
		case '\u30ff':
		{
			match('\u30FF');
			break;
		}
		case '\u3105':  case '\u3106':  case '\u3107':  case '\u3108':
		case '\u3109':  case '\u310a':  case '\u310b':  case '\u310c':
		case '\u310d':  case '\u310e':  case '\u310f':  case '\u3110':
		case '\u3111':  case '\u3112':  case '\u3113':  case '\u3114':
		case '\u3115':  case '\u3116':  case '\u3117':  case '\u3118':
		case '\u3119':  case '\u311a':  case '\u311b':  case '\u311c':
		case '\u311d':  case '\u311e':  case '\u311f':  case '\u3120':
		case '\u3121':  case '\u3122':  case '\u3123':  case '\u3124':
		case '\u3125':  case '\u3126':  case '\u3127':  case '\u3128':
		case '\u3129':  case '\u312a':  case '\u312b':  case '\u312c':
		case '\u312d':
		{
			matchRange('\u3105','\u312D');
			break;
		}
		case '\u3131':  case '\u3132':  case '\u3133':  case '\u3134':
		case '\u3135':  case '\u3136':  case '\u3137':  case '\u3138':
		case '\u3139':  case '\u313a':  case '\u313b':  case '\u313c':
		case '\u313d':  case '\u313e':  case '\u313f':  case '\u3140':
		case '\u3141':  case '\u3142':  case '\u3143':  case '\u3144':
		case '\u3145':  case '\u3146':  case '\u3147':  case '\u3148':
		case '\u3149':  case '\u314a':  case '\u314b':  case '\u314c':
		case '\u314d':  case '\u314e':  case '\u314f':  case '\u3150':
		case '\u3151':  case '\u3152':  case '\u3153':  case '\u3154':
		case '\u3155':  case '\u3156':  case '\u3157':  case '\u3158':
		case '\u3159':  case '\u315a':  case '\u315b':  case '\u315c':
		case '\u315d':  case '\u315e':  case '\u315f':  case '\u3160':
		case '\u3161':  case '\u3162':  case '\u3163':  case '\u3164':
		case '\u3165':  case '\u3166':  case '\u3167':  case '\u3168':
		case '\u3169':  case '\u316a':  case '\u316b':  case '\u316c':
		case '\u316d':  case '\u316e':  case '\u316f':  case '\u3170':
		case '\u3171':  case '\u3172':  case '\u3173':  case '\u3174':
		case '\u3175':  case '\u3176':  case '\u3177':  case '\u3178':
		case '\u3179':  case '\u317a':  case '\u317b':  case '\u317c':
		case '\u317d':  case '\u317e':  case '\u317f':  case '\u3180':
		case '\u3181':  case '\u3182':  case '\u3183':  case '\u3184':
		case '\u3185':  case '\u3186':  case '\u3187':  case '\u3188':
		case '\u3189':  case '\u318a':  case '\u318b':  case '\u318c':
		case '\u318d':  case '\u318e':
		{
			matchRange('\u3131','\u318E');
			break;
		}
		case '\u31a0':  case '\u31a1':  case '\u31a2':  case '\u31a3':
		case '\u31a4':  case '\u31a5':  case '\u31a6':  case '\u31a7':
		case '\u31a8':  case '\u31a9':  case '\u31aa':  case '\u31ab':
		case '\u31ac':  case '\u31ad':  case '\u31ae':  case '\u31af':
		case '\u31b0':  case '\u31b1':  case '\u31b2':  case '\u31b3':
		case '\u31b4':  case '\u31b5':  case '\u31b6':  case '\u31b7':
		case '\u31b8':  case '\u31b9':  case '\u31ba':
		{
			matchRange('\u31A0','\u31BA');
			break;
		}
		case '\u31f0':  case '\u31f1':  case '\u31f2':  case '\u31f3':
		case '\u31f4':  case '\u31f5':  case '\u31f6':  case '\u31f7':
		case '\u31f8':  case '\u31f9':  case '\u31fa':  case '\u31fb':
		case '\u31fc':  case '\u31fd':  case '\u31fe':  case '\u31ff':
		{
			matchRange('\u31F0','\u31FF');
			break;
		}
		case '\u3400':
		{
			match('\u3400');
			break;
		}
		case '\u4db5':
		{
			match('\u4DB5');
			break;
		}
		case '\u4e00':
		{
			match('\u4E00');
			break;
		}
		case '\u9fcc':
		{
			match('\u9FCC');
			break;
		}
		case '\ua000':  case '\ua001':  case '\ua002':  case '\ua003':
		case '\ua004':  case '\ua005':  case '\ua006':  case '\ua007':
		case '\ua008':  case '\ua009':  case '\ua00a':  case '\ua00b':
		case '\ua00c':  case '\ua00d':  case '\ua00e':  case '\ua00f':
		case '\ua010':  case '\ua011':  case '\ua012':  case '\ua013':
		case '\ua014':
		{
			matchRange('\uA000','\uA014');
			break;
		}
		case '\ua4d0':  case '\ua4d1':  case '\ua4d2':  case '\ua4d3':
		case '\ua4d4':  case '\ua4d5':  case '\ua4d6':  case '\ua4d7':
		case '\ua4d8':  case '\ua4d9':  case '\ua4da':  case '\ua4db':
		case '\ua4dc':  case '\ua4dd':  case '\ua4de':  case '\ua4df':
		case '\ua4e0':  case '\ua4e1':  case '\ua4e2':  case '\ua4e3':
		case '\ua4e4':  case '\ua4e5':  case '\ua4e6':  case '\ua4e7':
		case '\ua4e8':  case '\ua4e9':  case '\ua4ea':  case '\ua4eb':
		case '\ua4ec':  case '\ua4ed':  case '\ua4ee':  case '\ua4ef':
		case '\ua4f0':  case '\ua4f1':  case '\ua4f2':  case '\ua4f3':
		case '\ua4f4':  case '\ua4f5':  case '\ua4f6':  case '\ua4f7':
		{
			matchRange('\uA4D0','\uA4F7');
			break;
		}
		case '\ua610':  case '\ua611':  case '\ua612':  case '\ua613':
		case '\ua614':  case '\ua615':  case '\ua616':  case '\ua617':
		case '\ua618':  case '\ua619':  case '\ua61a':  case '\ua61b':
		case '\ua61c':  case '\ua61d':  case '\ua61e':  case '\ua61f':
		{
			matchRange('\uA610','\uA61F');
			break;
		}
		case '\ua62a':
		{
			match('\uA62A');
			break;
		}
		case '\ua62b':
		{
			match('\uA62B');
			break;
		}
		case '\ua66e':
		{
			match('\uA66E');
			break;
		}
		case '\ua6a0':  case '\ua6a1':  case '\ua6a2':  case '\ua6a3':
		case '\ua6a4':  case '\ua6a5':  case '\ua6a6':  case '\ua6a7':
		case '\ua6a8':  case '\ua6a9':  case '\ua6aa':  case '\ua6ab':
		case '\ua6ac':  case '\ua6ad':  case '\ua6ae':  case '\ua6af':
		case '\ua6b0':  case '\ua6b1':  case '\ua6b2':  case '\ua6b3':
		case '\ua6b4':  case '\ua6b5':  case '\ua6b6':  case '\ua6b7':
		case '\ua6b8':  case '\ua6b9':  case '\ua6ba':  case '\ua6bb':
		case '\ua6bc':  case '\ua6bd':  case '\ua6be':  case '\ua6bf':
		case '\ua6c0':  case '\ua6c1':  case '\ua6c2':  case '\ua6c3':
		case '\ua6c4':  case '\ua6c5':  case '\ua6c6':  case '\ua6c7':
		case '\ua6c8':  case '\ua6c9':  case '\ua6ca':  case '\ua6cb':
		case '\ua6cc':  case '\ua6cd':  case '\ua6ce':  case '\ua6cf':
		case '\ua6d0':  case '\ua6d1':  case '\ua6d2':  case '\ua6d3':
		case '\ua6d4':  case '\ua6d5':  case '\ua6d6':  case '\ua6d7':
		case '\ua6d8':  case '\ua6d9':  case '\ua6da':  case '\ua6db':
		case '\ua6dc':  case '\ua6dd':  case '\ua6de':  case '\ua6df':
		case '\ua6e0':  case '\ua6e1':  case '\ua6e2':  case '\ua6e3':
		case '\ua6e4':  case '\ua6e5':
		{
			matchRange('\uA6A0','\uA6E5');
			break;
		}
		case '\ua7fb':  case '\ua7fc':  case '\ua7fd':  case '\ua7fe':
		case '\ua7ff':  case '\ua800':  case '\ua801':
		{
			matchRange('\uA7FB','\uA801');
			break;
		}
		case '\ua803':  case '\ua804':  case '\ua805':
		{
			matchRange('\uA803','\uA805');
			break;
		}
		case '\ua807':  case '\ua808':  case '\ua809':  case '\ua80a':
		{
			matchRange('\uA807','\uA80A');
			break;
		}
		case '\ua80c':  case '\ua80d':  case '\ua80e':  case '\ua80f':
		case '\ua810':  case '\ua811':  case '\ua812':  case '\ua813':
		case '\ua814':  case '\ua815':  case '\ua816':  case '\ua817':
		case '\ua818':  case '\ua819':  case '\ua81a':  case '\ua81b':
		case '\ua81c':  case '\ua81d':  case '\ua81e':  case '\ua81f':
		case '\ua820':  case '\ua821':  case '\ua822':
		{
			matchRange('\uA80C','\uA822');
			break;
		}
		case '\ua840':  case '\ua841':  case '\ua842':  case '\ua843':
		case '\ua844':  case '\ua845':  case '\ua846':  case '\ua847':
		case '\ua848':  case '\ua849':  case '\ua84a':  case '\ua84b':
		case '\ua84c':  case '\ua84d':  case '\ua84e':  case '\ua84f':
		case '\ua850':  case '\ua851':  case '\ua852':  case '\ua853':
		case '\ua854':  case '\ua855':  case '\ua856':  case '\ua857':
		case '\ua858':  case '\ua859':  case '\ua85a':  case '\ua85b':
		case '\ua85c':  case '\ua85d':  case '\ua85e':  case '\ua85f':
		case '\ua860':  case '\ua861':  case '\ua862':  case '\ua863':
		case '\ua864':  case '\ua865':  case '\ua866':  case '\ua867':
		case '\ua868':  case '\ua869':  case '\ua86a':  case '\ua86b':
		case '\ua86c':  case '\ua86d':  case '\ua86e':  case '\ua86f':
		case '\ua870':  case '\ua871':  case '\ua872':  case '\ua873':
		{
			matchRange('\uA840','\uA873');
			break;
		}
		case '\ua882':  case '\ua883':  case '\ua884':  case '\ua885':
		case '\ua886':  case '\ua887':  case '\ua888':  case '\ua889':
		case '\ua88a':  case '\ua88b':  case '\ua88c':  case '\ua88d':
		case '\ua88e':  case '\ua88f':  case '\ua890':  case '\ua891':
		case '\ua892':  case '\ua893':  case '\ua894':  case '\ua895':
		case '\ua896':  case '\ua897':  case '\ua898':  case '\ua899':
		case '\ua89a':  case '\ua89b':  case '\ua89c':  case '\ua89d':
		case '\ua89e':  case '\ua89f':  case '\ua8a0':  case '\ua8a1':
		case '\ua8a2':  case '\ua8a3':  case '\ua8a4':  case '\ua8a5':
		case '\ua8a6':  case '\ua8a7':  case '\ua8a8':  case '\ua8a9':
		case '\ua8aa':  case '\ua8ab':  case '\ua8ac':  case '\ua8ad':
		case '\ua8ae':  case '\ua8af':  case '\ua8b0':  case '\ua8b1':
		case '\ua8b2':  case '\ua8b3':
		{
			matchRange('\uA882','\uA8B3');
			break;
		}
		case '\ua8f2':  case '\ua8f3':  case '\ua8f4':  case '\ua8f5':
		case '\ua8f6':  case '\ua8f7':
		{
			matchRange('\uA8F2','\uA8F7');
			break;
		}
		case '\ua8fb':
		{
			match('\uA8FB');
			break;
		}
		case '\ua90a':  case '\ua90b':  case '\ua90c':  case '\ua90d':
		case '\ua90e':  case '\ua90f':  case '\ua910':  case '\ua911':
		case '\ua912':  case '\ua913':  case '\ua914':  case '\ua915':
		case '\ua916':  case '\ua917':  case '\ua918':  case '\ua919':
		case '\ua91a':  case '\ua91b':  case '\ua91c':  case '\ua91d':
		case '\ua91e':  case '\ua91f':  case '\ua920':  case '\ua921':
		case '\ua922':  case '\ua923':  case '\ua924':  case '\ua925':
		{
			matchRange('\uA90A','\uA925');
			break;
		}
		case '\ua930':  case '\ua931':  case '\ua932':  case '\ua933':
		case '\ua934':  case '\ua935':  case '\ua936':  case '\ua937':
		case '\ua938':  case '\ua939':  case '\ua93a':  case '\ua93b':
		case '\ua93c':  case '\ua93d':  case '\ua93e':  case '\ua93f':
		case '\ua940':  case '\ua941':  case '\ua942':  case '\ua943':
		case '\ua944':  case '\ua945':  case '\ua946':
		{
			matchRange('\uA930','\uA946');
			break;
		}
		case '\ua960':  case '\ua961':  case '\ua962':  case '\ua963':
		case '\ua964':  case '\ua965':  case '\ua966':  case '\ua967':
		case '\ua968':  case '\ua969':  case '\ua96a':  case '\ua96b':
		case '\ua96c':  case '\ua96d':  case '\ua96e':  case '\ua96f':
		case '\ua970':  case '\ua971':  case '\ua972':  case '\ua973':
		case '\ua974':  case '\ua975':  case '\ua976':  case '\ua977':
		case '\ua978':  case '\ua979':  case '\ua97a':  case '\ua97b':
		case '\ua97c':
		{
			matchRange('\uA960','\uA97C');
			break;
		}
		case '\ua984':  case '\ua985':  case '\ua986':  case '\ua987':
		case '\ua988':  case '\ua989':  case '\ua98a':  case '\ua98b':
		case '\ua98c':  case '\ua98d':  case '\ua98e':  case '\ua98f':
		case '\ua990':  case '\ua991':  case '\ua992':  case '\ua993':
		case '\ua994':  case '\ua995':  case '\ua996':  case '\ua997':
		case '\ua998':  case '\ua999':  case '\ua99a':  case '\ua99b':
		case '\ua99c':  case '\ua99d':  case '\ua99e':  case '\ua99f':
		case '\ua9a0':  case '\ua9a1':  case '\ua9a2':  case '\ua9a3':
		case '\ua9a4':  case '\ua9a5':  case '\ua9a6':  case '\ua9a7':
		case '\ua9a8':  case '\ua9a9':  case '\ua9aa':  case '\ua9ab':
		case '\ua9ac':  case '\ua9ad':  case '\ua9ae':  case '\ua9af':
		case '\ua9b0':  case '\ua9b1':  case '\ua9b2':
		{
			matchRange('\uA984','\uA9B2');
			break;
		}
		case '\uaa00':  case '\uaa01':  case '\uaa02':  case '\uaa03':
		case '\uaa04':  case '\uaa05':  case '\uaa06':  case '\uaa07':
		case '\uaa08':  case '\uaa09':  case '\uaa0a':  case '\uaa0b':
		case '\uaa0c':  case '\uaa0d':  case '\uaa0e':  case '\uaa0f':
		case '\uaa10':  case '\uaa11':  case '\uaa12':  case '\uaa13':
		case '\uaa14':  case '\uaa15':  case '\uaa16':  case '\uaa17':
		case '\uaa18':  case '\uaa19':  case '\uaa1a':  case '\uaa1b':
		case '\uaa1c':  case '\uaa1d':  case '\uaa1e':  case '\uaa1f':
		case '\uaa20':  case '\uaa21':  case '\uaa22':  case '\uaa23':
		case '\uaa24':  case '\uaa25':  case '\uaa26':  case '\uaa27':
		case '\uaa28':
		{
			matchRange('\uAA00','\uAA28');
			break;
		}
		case '\uaa40':  case '\uaa41':  case '\uaa42':
		{
			matchRange('\uAA40','\uAA42');
			break;
		}
		case '\uaa44':  case '\uaa45':  case '\uaa46':  case '\uaa47':
		case '\uaa48':  case '\uaa49':  case '\uaa4a':  case '\uaa4b':
		{
			matchRange('\uAA44','\uAA4B');
			break;
		}
		case '\uaa60':  case '\uaa61':  case '\uaa62':  case '\uaa63':
		case '\uaa64':  case '\uaa65':  case '\uaa66':  case '\uaa67':
		case '\uaa68':  case '\uaa69':  case '\uaa6a':  case '\uaa6b':
		case '\uaa6c':  case '\uaa6d':  case '\uaa6e':  case '\uaa6f':
		{
			matchRange('\uAA60','\uAA6F');
			break;
		}
		case '\uaa71':  case '\uaa72':  case '\uaa73':  case '\uaa74':
		case '\uaa75':  case '\uaa76':
		{
			matchRange('\uAA71','\uAA76');
			break;
		}
		case '\uaa7a':
		{
			match('\uAA7A');
			break;
		}
		case '\uaa80':  case '\uaa81':  case '\uaa82':  case '\uaa83':
		case '\uaa84':  case '\uaa85':  case '\uaa86':  case '\uaa87':
		case '\uaa88':  case '\uaa89':  case '\uaa8a':  case '\uaa8b':
		case '\uaa8c':  case '\uaa8d':  case '\uaa8e':  case '\uaa8f':
		case '\uaa90':  case '\uaa91':  case '\uaa92':  case '\uaa93':
		case '\uaa94':  case '\uaa95':  case '\uaa96':  case '\uaa97':
		case '\uaa98':  case '\uaa99':  case '\uaa9a':  case '\uaa9b':
		case '\uaa9c':  case '\uaa9d':  case '\uaa9e':  case '\uaa9f':
		case '\uaaa0':  case '\uaaa1':  case '\uaaa2':  case '\uaaa3':
		case '\uaaa4':  case '\uaaa5':  case '\uaaa6':  case '\uaaa7':
		case '\uaaa8':  case '\uaaa9':  case '\uaaaa':  case '\uaaab':
		case '\uaaac':  case '\uaaad':  case '\uaaae':  case '\uaaaf':
		{
			matchRange('\uAA80','\uAAAF');
			break;
		}
		case '\uaab1':
		{
			match('\uAAB1');
			break;
		}
		case '\uaab5':
		{
			match('\uAAB5');
			break;
		}
		case '\uaab6':
		{
			match('\uAAB6');
			break;
		}
		case '\uaab9':  case '\uaaba':  case '\uaabb':  case '\uaabc':
		case '\uaabd':
		{
			matchRange('\uAAB9','\uAABD');
			break;
		}
		case '\uaac0':
		{
			match('\uAAC0');
			break;
		}
		case '\uaac2':
		{
			match('\uAAC2');
			break;
		}
		case '\uaadb':
		{
			match('\uAADB');
			break;
		}
		case '\uaadc':
		{
			match('\uAADC');
			break;
		}
		case '\uaae0':  case '\uaae1':  case '\uaae2':  case '\uaae3':
		case '\uaae4':  case '\uaae5':  case '\uaae6':  case '\uaae7':
		case '\uaae8':  case '\uaae9':  case '\uaaea':
		{
			matchRange('\uAAE0','\uAAEA');
			break;
		}
		case '\uaaf2':
		{
			match('\uAAF2');
			break;
		}
		case '\uab01':  case '\uab02':  case '\uab03':  case '\uab04':
		case '\uab05':  case '\uab06':
		{
			matchRange('\uAB01','\uAB06');
			break;
		}
		case '\uab09':  case '\uab0a':  case '\uab0b':  case '\uab0c':
		case '\uab0d':  case '\uab0e':
		{
			matchRange('\uAB09','\uAB0E');
			break;
		}
		case '\uab11':  case '\uab12':  case '\uab13':  case '\uab14':
		case '\uab15':  case '\uab16':
		{
			matchRange('\uAB11','\uAB16');
			break;
		}
		case '\uab20':  case '\uab21':  case '\uab22':  case '\uab23':
		case '\uab24':  case '\uab25':  case '\uab26':
		{
			matchRange('\uAB20','\uAB26');
			break;
		}
		case '\uab28':  case '\uab29':  case '\uab2a':  case '\uab2b':
		case '\uab2c':  case '\uab2d':  case '\uab2e':
		{
			matchRange('\uAB28','\uAB2E');
			break;
		}
		case '\uabc0':  case '\uabc1':  case '\uabc2':  case '\uabc3':
		case '\uabc4':  case '\uabc5':  case '\uabc6':  case '\uabc7':
		case '\uabc8':  case '\uabc9':  case '\uabca':  case '\uabcb':
		case '\uabcc':  case '\uabcd':  case '\uabce':  case '\uabcf':
		case '\uabd0':  case '\uabd1':  case '\uabd2':  case '\uabd3':
		case '\uabd4':  case '\uabd5':  case '\uabd6':  case '\uabd7':
		case '\uabd8':  case '\uabd9':  case '\uabda':  case '\uabdb':
		case '\uabdc':  case '\uabdd':  case '\uabde':  case '\uabdf':
		case '\uabe0':  case '\uabe1':  case '\uabe2':
		{
			matchRange('\uABC0','\uABE2');
			break;
		}
		case '\uac00':
		{
			match('\uAC00');
			break;
		}
		case '\ud7a3':
		{
			match('\uD7A3');
			break;
		}
		case '\ud7b0':  case '\ud7b1':  case '\ud7b2':  case '\ud7b3':
		case '\ud7b4':  case '\ud7b5':  case '\ud7b6':  case '\ud7b7':
		case '\ud7b8':  case '\ud7b9':  case '\ud7ba':  case '\ud7bb':
		case '\ud7bc':  case '\ud7bd':  case '\ud7be':  case '\ud7bf':
		case '\ud7c0':  case '\ud7c1':  case '\ud7c2':  case '\ud7c3':
		case '\ud7c4':  case '\ud7c5':  case '\ud7c6':
		{
			matchRange('\uD7B0','\uD7C6');
			break;
		}
		case '\ud7cb':  case '\ud7cc':  case '\ud7cd':  case '\ud7ce':
		case '\ud7cf':  case '\ud7d0':  case '\ud7d1':  case '\ud7d2':
		case '\ud7d3':  case '\ud7d4':  case '\ud7d5':  case '\ud7d6':
		case '\ud7d7':  case '\ud7d8':  case '\ud7d9':  case '\ud7da':
		case '\ud7db':  case '\ud7dc':  case '\ud7dd':  case '\ud7de':
		case '\ud7df':  case '\ud7e0':  case '\ud7e1':  case '\ud7e2':
		case '\ud7e3':  case '\ud7e4':  case '\ud7e5':  case '\ud7e6':
		case '\ud7e7':  case '\ud7e8':  case '\ud7e9':  case '\ud7ea':
		case '\ud7eb':  case '\ud7ec':  case '\ud7ed':  case '\ud7ee':
		case '\ud7ef':  case '\ud7f0':  case '\ud7f1':  case '\ud7f2':
		case '\ud7f3':  case '\ud7f4':  case '\ud7f5':  case '\ud7f6':
		case '\ud7f7':  case '\ud7f8':  case '\ud7f9':  case '\ud7fa':
		case '\ud7fb':
		{
			matchRange('\uD7CB','\uD7FB');
			break;
		}
		case '\ufa70':  case '\ufa71':  case '\ufa72':  case '\ufa73':
		case '\ufa74':  case '\ufa75':  case '\ufa76':  case '\ufa77':
		case '\ufa78':  case '\ufa79':  case '\ufa7a':  case '\ufa7b':
		case '\ufa7c':  case '\ufa7d':  case '\ufa7e':  case '\ufa7f':
		case '\ufa80':  case '\ufa81':  case '\ufa82':  case '\ufa83':
		case '\ufa84':  case '\ufa85':  case '\ufa86':  case '\ufa87':
		case '\ufa88':  case '\ufa89':  case '\ufa8a':  case '\ufa8b':
		case '\ufa8c':  case '\ufa8d':  case '\ufa8e':  case '\ufa8f':
		case '\ufa90':  case '\ufa91':  case '\ufa92':  case '\ufa93':
		case '\ufa94':  case '\ufa95':  case '\ufa96':  case '\ufa97':
		case '\ufa98':  case '\ufa99':  case '\ufa9a':  case '\ufa9b':
		case '\ufa9c':  case '\ufa9d':  case '\ufa9e':  case '\ufa9f':
		case '\ufaa0':  case '\ufaa1':  case '\ufaa2':  case '\ufaa3':
		case '\ufaa4':  case '\ufaa5':  case '\ufaa6':  case '\ufaa7':
		case '\ufaa8':  case '\ufaa9':  case '\ufaaa':  case '\ufaab':
		case '\ufaac':  case '\ufaad':  case '\ufaae':  case '\ufaaf':
		case '\ufab0':  case '\ufab1':  case '\ufab2':  case '\ufab3':
		case '\ufab4':  case '\ufab5':  case '\ufab6':  case '\ufab7':
		case '\ufab8':  case '\ufab9':  case '\ufaba':  case '\ufabb':
		case '\ufabc':  case '\ufabd':  case '\ufabe':  case '\ufabf':
		case '\ufac0':  case '\ufac1':  case '\ufac2':  case '\ufac3':
		case '\ufac4':  case '\ufac5':  case '\ufac6':  case '\ufac7':
		case '\ufac8':  case '\ufac9':  case '\ufaca':  case '\ufacb':
		case '\ufacc':  case '\ufacd':  case '\uface':  case '\ufacf':
		case '\ufad0':  case '\ufad1':  case '\ufad2':  case '\ufad3':
		case '\ufad4':  case '\ufad5':  case '\ufad6':  case '\ufad7':
		case '\ufad8':  case '\ufad9':
		{
			matchRange('\uFA70','\uFAD9');
			break;
		}
		case '\ufb1d':
		{
			match('\uFB1D');
			break;
		}
		case '\ufb1f':  case '\ufb20':  case '\ufb21':  case '\ufb22':
		case '\ufb23':  case '\ufb24':  case '\ufb25':  case '\ufb26':
		case '\ufb27':  case '\ufb28':
		{
			matchRange('\uFB1F','\uFB28');
			break;
		}
		case '\ufb2a':  case '\ufb2b':  case '\ufb2c':  case '\ufb2d':
		case '\ufb2e':  case '\ufb2f':  case '\ufb30':  case '\ufb31':
		case '\ufb32':  case '\ufb33':  case '\ufb34':  case '\ufb35':
		case '\ufb36':
		{
			matchRange('\uFB2A','\uFB36');
			break;
		}
		case '\ufb38':  case '\ufb39':  case '\ufb3a':  case '\ufb3b':
		case '\ufb3c':
		{
			matchRange('\uFB38','\uFB3C');
			break;
		}
		case '\ufb3e':
		{
			match('\uFB3E');
			break;
		}
		case '\ufb40':
		{
			match('\uFB40');
			break;
		}
		case '\ufb41':
		{
			match('\uFB41');
			break;
		}
		case '\ufb43':
		{
			match('\uFB43');
			break;
		}
		case '\ufb44':
		{
			match('\uFB44');
			break;
		}
		case '\ufb46':  case '\ufb47':  case '\ufb48':  case '\ufb49':
		case '\ufb4a':  case '\ufb4b':  case '\ufb4c':  case '\ufb4d':
		case '\ufb4e':  case '\ufb4f':  case '\ufb50':  case '\ufb51':
		case '\ufb52':  case '\ufb53':  case '\ufb54':  case '\ufb55':
		case '\ufb56':  case '\ufb57':  case '\ufb58':  case '\ufb59':
		case '\ufb5a':  case '\ufb5b':  case '\ufb5c':  case '\ufb5d':
		case '\ufb5e':  case '\ufb5f':  case '\ufb60':  case '\ufb61':
		case '\ufb62':  case '\ufb63':  case '\ufb64':  case '\ufb65':
		case '\ufb66':  case '\ufb67':  case '\ufb68':  case '\ufb69':
		case '\ufb6a':  case '\ufb6b':  case '\ufb6c':  case '\ufb6d':
		case '\ufb6e':  case '\ufb6f':  case '\ufb70':  case '\ufb71':
		case '\ufb72':  case '\ufb73':  case '\ufb74':  case '\ufb75':
		case '\ufb76':  case '\ufb77':  case '\ufb78':  case '\ufb79':
		case '\ufb7a':  case '\ufb7b':  case '\ufb7c':  case '\ufb7d':
		case '\ufb7e':  case '\ufb7f':  case '\ufb80':  case '\ufb81':
		case '\ufb82':  case '\ufb83':  case '\ufb84':  case '\ufb85':
		case '\ufb86':  case '\ufb87':  case '\ufb88':  case '\ufb89':
		case '\ufb8a':  case '\ufb8b':  case '\ufb8c':  case '\ufb8d':
		case '\ufb8e':  case '\ufb8f':  case '\ufb90':  case '\ufb91':
		case '\ufb92':  case '\ufb93':  case '\ufb94':  case '\ufb95':
		case '\ufb96':  case '\ufb97':  case '\ufb98':  case '\ufb99':
		case '\ufb9a':  case '\ufb9b':  case '\ufb9c':  case '\ufb9d':
		case '\ufb9e':  case '\ufb9f':  case '\ufba0':  case '\ufba1':
		case '\ufba2':  case '\ufba3':  case '\ufba4':  case '\ufba5':
		case '\ufba6':  case '\ufba7':  case '\ufba8':  case '\ufba9':
		case '\ufbaa':  case '\ufbab':  case '\ufbac':  case '\ufbad':
		case '\ufbae':  case '\ufbaf':  case '\ufbb0':  case '\ufbb1':
		{
			matchRange('\uFB46','\uFBB1');
			break;
		}
		case '\ufd50':  case '\ufd51':  case '\ufd52':  case '\ufd53':
		case '\ufd54':  case '\ufd55':  case '\ufd56':  case '\ufd57':
		case '\ufd58':  case '\ufd59':  case '\ufd5a':  case '\ufd5b':
		case '\ufd5c':  case '\ufd5d':  case '\ufd5e':  case '\ufd5f':
		case '\ufd60':  case '\ufd61':  case '\ufd62':  case '\ufd63':
		case '\ufd64':  case '\ufd65':  case '\ufd66':  case '\ufd67':
		case '\ufd68':  case '\ufd69':  case '\ufd6a':  case '\ufd6b':
		case '\ufd6c':  case '\ufd6d':  case '\ufd6e':  case '\ufd6f':
		case '\ufd70':  case '\ufd71':  case '\ufd72':  case '\ufd73':
		case '\ufd74':  case '\ufd75':  case '\ufd76':  case '\ufd77':
		case '\ufd78':  case '\ufd79':  case '\ufd7a':  case '\ufd7b':
		case '\ufd7c':  case '\ufd7d':  case '\ufd7e':  case '\ufd7f':
		case '\ufd80':  case '\ufd81':  case '\ufd82':  case '\ufd83':
		case '\ufd84':  case '\ufd85':  case '\ufd86':  case '\ufd87':
		case '\ufd88':  case '\ufd89':  case '\ufd8a':  case '\ufd8b':
		case '\ufd8c':  case '\ufd8d':  case '\ufd8e':  case '\ufd8f':
		{
			matchRange('\uFD50','\uFD8F');
			break;
		}
		case '\ufd92':  case '\ufd93':  case '\ufd94':  case '\ufd95':
		case '\ufd96':  case '\ufd97':  case '\ufd98':  case '\ufd99':
		case '\ufd9a':  case '\ufd9b':  case '\ufd9c':  case '\ufd9d':
		case '\ufd9e':  case '\ufd9f':  case '\ufda0':  case '\ufda1':
		case '\ufda2':  case '\ufda3':  case '\ufda4':  case '\ufda5':
		case '\ufda6':  case '\ufda7':  case '\ufda8':  case '\ufda9':
		case '\ufdaa':  case '\ufdab':  case '\ufdac':  case '\ufdad':
		case '\ufdae':  case '\ufdaf':  case '\ufdb0':  case '\ufdb1':
		case '\ufdb2':  case '\ufdb3':  case '\ufdb4':  case '\ufdb5':
		case '\ufdb6':  case '\ufdb7':  case '\ufdb8':  case '\ufdb9':
		case '\ufdba':  case '\ufdbb':  case '\ufdbc':  case '\ufdbd':
		case '\ufdbe':  case '\ufdbf':  case '\ufdc0':  case '\ufdc1':
		case '\ufdc2':  case '\ufdc3':  case '\ufdc4':  case '\ufdc5':
		case '\ufdc6':  case '\ufdc7':
		{
			matchRange('\uFD92','\uFDC7');
			break;
		}
		case '\ufdf0':  case '\ufdf1':  case '\ufdf2':  case '\ufdf3':
		case '\ufdf4':  case '\ufdf5':  case '\ufdf6':  case '\ufdf7':
		case '\ufdf8':  case '\ufdf9':  case '\ufdfa':  case '\ufdfb':
		{
			matchRange('\uFDF0','\uFDFB');
			break;
		}
		case '\ufe70':  case '\ufe71':  case '\ufe72':  case '\ufe73':
		case '\ufe74':
		{
			matchRange('\uFE70','\uFE74');
			break;
		}
		case '\uff66':  case '\uff67':  case '\uff68':  case '\uff69':
		case '\uff6a':  case '\uff6b':  case '\uff6c':  case '\uff6d':
		case '\uff6e':  case '\uff6f':
		{
			matchRange('\uFF66','\uFF6F');
			break;
		}
		case '\uff71':  case '\uff72':  case '\uff73':  case '\uff74':
		case '\uff75':  case '\uff76':  case '\uff77':  case '\uff78':
		case '\uff79':  case '\uff7a':  case '\uff7b':  case '\uff7c':
		case '\uff7d':  case '\uff7e':  case '\uff7f':  case '\uff80':
		case '\uff81':  case '\uff82':  case '\uff83':  case '\uff84':
		case '\uff85':  case '\uff86':  case '\uff87':  case '\uff88':
		case '\uff89':  case '\uff8a':  case '\uff8b':  case '\uff8c':
		case '\uff8d':  case '\uff8e':  case '\uff8f':  case '\uff90':
		case '\uff91':  case '\uff92':  case '\uff93':  case '\uff94':
		case '\uff95':  case '\uff96':  case '\uff97':  case '\uff98':
		case '\uff99':  case '\uff9a':  case '\uff9b':  case '\uff9c':
		case '\uff9d':
		{
			matchRange('\uFF71','\uFF9D');
			break;
		}
		case '\uffa0':  case '\uffa1':  case '\uffa2':  case '\uffa3':
		case '\uffa4':  case '\uffa5':  case '\uffa6':  case '\uffa7':
		case '\uffa8':  case '\uffa9':  case '\uffaa':  case '\uffab':
		case '\uffac':  case '\uffad':  case '\uffae':  case '\uffaf':
		case '\uffb0':  case '\uffb1':  case '\uffb2':  case '\uffb3':
		case '\uffb4':  case '\uffb5':  case '\uffb6':  case '\uffb7':
		case '\uffb8':  case '\uffb9':  case '\uffba':  case '\uffbb':
		case '\uffbc':  case '\uffbd':  case '\uffbe':
		{
			matchRange('\uFFA0','\uFFBE');
			break;
		}
		case '\uffc2':  case '\uffc3':  case '\uffc4':  case '\uffc5':
		case '\uffc6':  case '\uffc7':
		{
			matchRange('\uFFC2','\uFFC7');
			break;
		}
		case '\uffca':  case '\uffcb':  case '\uffcc':  case '\uffcd':
		case '\uffce':  case '\uffcf':
		{
			matchRange('\uFFCA','\uFFCF');
			break;
		}
		case '\uffd2':  case '\uffd3':  case '\uffd4':  case '\uffd5':
		case '\uffd6':  case '\uffd7':
		{
			matchRange('\uFFD2','\uFFD7');
			break;
		}
		case '\uffda':  case '\uffdb':  case '\uffdc':
		{
			matchRange('\uFFDA','\uFFDC');
			break;
		}
		default:
			if (((LA(1) >= '\u10fd' && LA(1) <= '\u1248'))) {
				matchRange('\u10FD','\u1248');
			}
			else if (((LA(1) >= '\u1401' && LA(1) <= '\u166c'))) {
				matchRange('\u1401','\u166C');
			}
			else if (((LA(1) >= '\ua016' && LA(1) <= '\ua48c'))) {
				matchRange('\uA016','\uA48C');
			}
			else if (((LA(1) >= '\ua500' && LA(1) <= '\ua60b'))) {
				matchRange('\uA500','\uA60B');
			}
			else if (((LA(1) >= '\uf900' && LA(1) <= '\ufa6d'))) {
				matchRange('\uF900','\uFA6D');
			}
			else if (((LA(1) >= '\ufbd3' && LA(1) <= '\ufd3d'))) {
				matchRange('\uFBD3','\uFD3D');
			}
			else if (((LA(1) >= '\ufe76' && LA(1) <= '\ufefc'))) {
				matchRange('\uFE76','\uFEFC');
			}
		else {
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mUNICODE_CLASS_NL(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = UNICODE_CLASS_NL;
		int _saveIndex;
		
		switch ( LA(1)) {
		case '\u16ee':  case '\u16ef':  case '\u16f0':
		{
			matchRange('\u16EE','\u16F0');
			break;
		}
		case '\u2160':  case '\u2161':  case '\u2162':  case '\u2163':
		case '\u2164':  case '\u2165':  case '\u2166':  case '\u2167':
		case '\u2168':  case '\u2169':  case '\u216a':  case '\u216b':
		case '\u216c':  case '\u216d':  case '\u216e':  case '\u216f':
		case '\u2170':  case '\u2171':  case '\u2172':  case '\u2173':
		case '\u2174':  case '\u2175':  case '\u2176':  case '\u2177':
		case '\u2178':  case '\u2179':  case '\u217a':  case '\u217b':
		case '\u217c':  case '\u217d':  case '\u217e':  case '\u217f':
		case '\u2180':  case '\u2181':  case '\u2182':
		{
			matchRange('\u2160','\u2182');
			break;
		}
		case '\u2185':  case '\u2186':  case '\u2187':  case '\u2188':
		{
			matchRange('\u2185','\u2188');
			break;
		}
		case '\u3007':
		{
			match('\u3007');
			break;
		}
		case '\u3021':  case '\u3022':  case '\u3023':  case '\u3024':
		case '\u3025':  case '\u3026':  case '\u3027':  case '\u3028':
		case '\u3029':
		{
			matchRange('\u3021','\u3029');
			break;
		}
		case '\u3038':  case '\u3039':  case '\u303a':
		{
			matchRange('\u3038','\u303A');
			break;
		}
		case '\ua6e6':  case '\ua6e7':  case '\ua6e8':  case '\ua6e9':
		case '\ua6ea':  case '\ua6eb':  case '\ua6ec':  case '\ua6ed':
		case '\ua6ee':  case '\ua6ef':
		{
			matchRange('\uA6E6','\uA6EF');
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mUNICODE_CLASS_LU(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = UNICODE_CLASS_LU;
		int _saveIndex;
		
		switch ( LA(1)) {
		case 'A':  case 'B':  case 'C':  case 'D':
		case 'E':  case 'F':  case 'G':  case 'H':
		case 'I':  case 'J':  case 'K':  case 'L':
		case 'M':  case 'N':  case 'O':  case 'P':
		case 'Q':  case 'R':  case 'S':  case 'T':
		case 'U':  case 'V':  case 'W':  case 'X':
		case 'Y':  case 'Z':
		{
			matchRange('\u0041','\u005A');
			break;
		}
		case '\u00c0':  case '\u00c1':  case '\u00c2':  case '\u00c3':
		case '\u00c4':  case '\u00c5':  case '\u00c6':  case '\u00c7':
		case '\u00c8':  case '\u00c9':  case '\u00ca':  case '\u00cb':
		case '\u00cc':  case '\u00cd':  case '\u00ce':  case '\u00cf':
		case '\u00d0':  case '\u00d1':  case '\u00d2':  case '\u00d3':
		case '\u00d4':  case '\u00d5':  case '\u00d6':
		{
			matchRange('\u00C0','\u00D6');
			break;
		}
		case '\u00d8':  case '\u00d9':  case '\u00da':  case '\u00db':
		case '\u00dc':  case '\u00dd':  case '\u00de':
		{
			matchRange('\u00D8','\u00DE');
			break;
		}
		case '\u0100':
		{
			match('\u0100');
			break;
		}
		case '\u0102':
		{
			match('\u0102');
			break;
		}
		case '\u0104':
		{
			match('\u0104');
			break;
		}
		case '\u0106':
		{
			match('\u0106');
			break;
		}
		case '\u0108':
		{
			match('\u0108');
			break;
		}
		case '\u010a':
		{
			match('\u010A');
			break;
		}
		case '\u010c':
		{
			match('\u010C');
			break;
		}
		case '\u010e':
		{
			match('\u010E');
			break;
		}
		case '\u0110':
		{
			match('\u0110');
			break;
		}
		case '\u0112':
		{
			match('\u0112');
			break;
		}
		case '\u0114':
		{
			match('\u0114');
			break;
		}
		case '\u0116':
		{
			match('\u0116');
			break;
		}
		case '\u0118':
		{
			match('\u0118');
			break;
		}
		case '\u011a':
		{
			match('\u011A');
			break;
		}
		case '\u011c':
		{
			match('\u011C');
			break;
		}
		case '\u011e':
		{
			match('\u011E');
			break;
		}
		case '\u0120':
		{
			match('\u0120');
			break;
		}
		case '\u0122':
		{
			match('\u0122');
			break;
		}
		case '\u0124':
		{
			match('\u0124');
			break;
		}
		case '\u0126':
		{
			match('\u0126');
			break;
		}
		case '\u0128':
		{
			match('\u0128');
			break;
		}
		case '\u012a':
		{
			match('\u012A');
			break;
		}
		case '\u012c':
		{
			match('\u012C');
			break;
		}
		case '\u012e':
		{
			match('\u012E');
			break;
		}
		case '\u0130':
		{
			match('\u0130');
			break;
		}
		case '\u0132':
		{
			match('\u0132');
			break;
		}
		case '\u0134':
		{
			match('\u0134');
			break;
		}
		case '\u0136':
		{
			match('\u0136');
			break;
		}
		case '\u0139':
		{
			match('\u0139');
			break;
		}
		case '\u013b':
		{
			match('\u013B');
			break;
		}
		case '\u013d':
		{
			match('\u013D');
			break;
		}
		case '\u013f':
		{
			match('\u013F');
			break;
		}
		case '\u0141':
		{
			match('\u0141');
			break;
		}
		case '\u0143':
		{
			match('\u0143');
			break;
		}
		case '\u0145':
		{
			match('\u0145');
			break;
		}
		case '\u0147':
		{
			match('\u0147');
			break;
		}
		case '\u014a':
		{
			match('\u014A');
			break;
		}
		case '\u014c':
		{
			match('\u014C');
			break;
		}
		case '\u014e':
		{
			match('\u014E');
			break;
		}
		case '\u0150':
		{
			match('\u0150');
			break;
		}
		case '\u0152':
		{
			match('\u0152');
			break;
		}
		case '\u0154':
		{
			match('\u0154');
			break;
		}
		case '\u0156':
		{
			match('\u0156');
			break;
		}
		case '\u0158':
		{
			match('\u0158');
			break;
		}
		case '\u015a':
		{
			match('\u015A');
			break;
		}
		case '\u015c':
		{
			match('\u015C');
			break;
		}
		case '\u015e':
		{
			match('\u015E');
			break;
		}
		case '\u0160':
		{
			match('\u0160');
			break;
		}
		case '\u0162':
		{
			match('\u0162');
			break;
		}
		case '\u0164':
		{
			match('\u0164');
			break;
		}
		case '\u0166':
		{
			match('\u0166');
			break;
		}
		case '\u0168':
		{
			match('\u0168');
			break;
		}
		case '\u016a':
		{
			match('\u016A');
			break;
		}
		case '\u016c':
		{
			match('\u016C');
			break;
		}
		case '\u016e':
		{
			match('\u016E');
			break;
		}
		case '\u0170':
		{
			match('\u0170');
			break;
		}
		case '\u0172':
		{
			match('\u0172');
			break;
		}
		case '\u0174':
		{
			match('\u0174');
			break;
		}
		case '\u0176':
		{
			match('\u0176');
			break;
		}
		case '\u0178':
		{
			match('\u0178');
			break;
		}
		case '\u0179':
		{
			match('\u0179');
			break;
		}
		case '\u017b':
		{
			match('\u017B');
			break;
		}
		case '\u017d':
		{
			match('\u017D');
			break;
		}
		case '\u0181':
		{
			match('\u0181');
			break;
		}
		case '\u0182':
		{
			match('\u0182');
			break;
		}
		case '\u0184':
		{
			match('\u0184');
			break;
		}
		case '\u0186':
		{
			match('\u0186');
			break;
		}
		case '\u0187':
		{
			match('\u0187');
			break;
		}
		case '\u0189':  case '\u018a':  case '\u018b':
		{
			matchRange('\u0189','\u018B');
			break;
		}
		case '\u018e':  case '\u018f':  case '\u0190':  case '\u0191':
		{
			matchRange('\u018E','\u0191');
			break;
		}
		case '\u0193':
		{
			match('\u0193');
			break;
		}
		case '\u0194':
		{
			match('\u0194');
			break;
		}
		case '\u0196':  case '\u0197':  case '\u0198':
		{
			matchRange('\u0196','\u0198');
			break;
		}
		case '\u019c':
		{
			match('\u019C');
			break;
		}
		case '\u019d':
		{
			match('\u019D');
			break;
		}
		case '\u019f':
		{
			match('\u019F');
			break;
		}
		case '\u01a0':
		{
			match('\u01A0');
			break;
		}
		case '\u01a2':
		{
			match('\u01A2');
			break;
		}
		case '\u01a4':
		{
			match('\u01A4');
			break;
		}
		case '\u01a6':
		{
			match('\u01A6');
			break;
		}
		case '\u01a7':
		{
			match('\u01A7');
			break;
		}
		case '\u01a9':
		{
			match('\u01A9');
			break;
		}
		case '\u01ac':
		{
			match('\u01AC');
			break;
		}
		case '\u01ae':
		{
			match('\u01AE');
			break;
		}
		case '\u01af':
		{
			match('\u01AF');
			break;
		}
		case '\u01b1':  case '\u01b2':  case '\u01b3':
		{
			matchRange('\u01B1','\u01B3');
			break;
		}
		case '\u01b5':
		{
			match('\u01B5');
			break;
		}
		case '\u01b7':
		{
			match('\u01B7');
			break;
		}
		case '\u01b8':
		{
			match('\u01B8');
			break;
		}
		case '\u01bc':
		{
			match('\u01BC');
			break;
		}
		case '\u01c4':
		{
			match('\u01C4');
			break;
		}
		case '\u01c7':
		{
			match('\u01C7');
			break;
		}
		case '\u01ca':
		{
			match('\u01CA');
			break;
		}
		case '\u01cd':
		{
			match('\u01CD');
			break;
		}
		case '\u01cf':
		{
			match('\u01CF');
			break;
		}
		case '\u01d1':
		{
			match('\u01D1');
			break;
		}
		case '\u01d3':
		{
			match('\u01D3');
			break;
		}
		case '\u01d5':
		{
			match('\u01D5');
			break;
		}
		case '\u01d7':
		{
			match('\u01D7');
			break;
		}
		case '\u01d9':
		{
			match('\u01D9');
			break;
		}
		case '\u01db':
		{
			match('\u01DB');
			break;
		}
		case '\u01de':
		{
			match('\u01DE');
			break;
		}
		case '\u01e0':
		{
			match('\u01E0');
			break;
		}
		case '\u01e2':
		{
			match('\u01E2');
			break;
		}
		case '\u01e4':
		{
			match('\u01E4');
			break;
		}
		case '\u01e6':
		{
			match('\u01E6');
			break;
		}
		case '\u01e8':
		{
			match('\u01E8');
			break;
		}
		case '\u01ea':
		{
			match('\u01EA');
			break;
		}
		case '\u01ec':
		{
			match('\u01EC');
			break;
		}
		case '\u01ee':
		{
			match('\u01EE');
			break;
		}
		case '\u01f1':
		{
			match('\u01F1');
			break;
		}
		case '\u01f4':
		{
			match('\u01F4');
			break;
		}
		case '\u01f6':  case '\u01f7':  case '\u01f8':
		{
			matchRange('\u01F6','\u01F8');
			break;
		}
		case '\u01fa':
		{
			match('\u01FA');
			break;
		}
		case '\u01fc':
		{
			match('\u01FC');
			break;
		}
		case '\u01fe':
		{
			match('\u01FE');
			break;
		}
		case '\u0200':
		{
			match('\u0200');
			break;
		}
		case '\u0202':
		{
			match('\u0202');
			break;
		}
		case '\u0204':
		{
			match('\u0204');
			break;
		}
		case '\u0206':
		{
			match('\u0206');
			break;
		}
		case '\u0208':
		{
			match('\u0208');
			break;
		}
		case '\u020a':
		{
			match('\u020A');
			break;
		}
		case '\u020c':
		{
			match('\u020C');
			break;
		}
		case '\u020e':
		{
			match('\u020E');
			break;
		}
		case '\u0210':
		{
			match('\u0210');
			break;
		}
		case '\u0212':
		{
			match('\u0212');
			break;
		}
		case '\u0214':
		{
			match('\u0214');
			break;
		}
		case '\u0216':
		{
			match('\u0216');
			break;
		}
		case '\u0218':
		{
			match('\u0218');
			break;
		}
		case '\u021a':
		{
			match('\u021A');
			break;
		}
		case '\u021c':
		{
			match('\u021C');
			break;
		}
		case '\u021e':
		{
			match('\u021E');
			break;
		}
		case '\u0220':
		{
			match('\u0220');
			break;
		}
		case '\u0222':
		{
			match('\u0222');
			break;
		}
		case '\u0224':
		{
			match('\u0224');
			break;
		}
		case '\u0226':
		{
			match('\u0226');
			break;
		}
		case '\u0228':
		{
			match('\u0228');
			break;
		}
		case '\u022a':
		{
			match('\u022A');
			break;
		}
		case '\u022c':
		{
			match('\u022C');
			break;
		}
		case '\u022e':
		{
			match('\u022E');
			break;
		}
		case '\u0230':
		{
			match('\u0230');
			break;
		}
		case '\u0232':
		{
			match('\u0232');
			break;
		}
		case '\u023a':
		{
			match('\u023A');
			break;
		}
		case '\u023b':
		{
			match('\u023B');
			break;
		}
		case '\u023d':
		{
			match('\u023D');
			break;
		}
		case '\u023e':
		{
			match('\u023E');
			break;
		}
		case '\u0241':
		{
			match('\u0241');
			break;
		}
		case '\u0243':  case '\u0244':  case '\u0245':  case '\u0246':
		{
			matchRange('\u0243','\u0246');
			break;
		}
		case '\u0248':
		{
			match('\u0248');
			break;
		}
		case '\u024a':
		{
			match('\u024A');
			break;
		}
		case '\u024c':
		{
			match('\u024C');
			break;
		}
		case '\u024e':
		{
			match('\u024E');
			break;
		}
		case '\u0370':
		{
			match('\u0370');
			break;
		}
		case '\u0372':
		{
			match('\u0372');
			break;
		}
		case '\u0376':
		{
			match('\u0376');
			break;
		}
		case '\u0386':
		{
			match('\u0386');
			break;
		}
		case '\u0388':  case '\u0389':  case '\u038a':
		{
			matchRange('\u0388','\u038A');
			break;
		}
		case '\u038c':
		{
			match('\u038C');
			break;
		}
		case '\u038e':
		{
			match('\u038E');
			break;
		}
		case '\u038f':
		{
			match('\u038F');
			break;
		}
		case '\u0391':  case '\u0392':  case '\u0393':  case '\u0394':
		case '\u0395':  case '\u0396':  case '\u0397':  case '\u0398':
		case '\u0399':  case '\u039a':  case '\u039b':  case '\u039c':
		case '\u039d':  case '\u039e':  case '\u039f':  case '\u03a0':
		case '\u03a1':
		{
			matchRange('\u0391','\u03A1');
			break;
		}
		case '\u03a3':  case '\u03a4':  case '\u03a5':  case '\u03a6':
		case '\u03a7':  case '\u03a8':  case '\u03a9':  case '\u03aa':
		case '\u03ab':
		{
			matchRange('\u03A3','\u03AB');
			break;
		}
		case '\u03cf':
		{
			match('\u03CF');
			break;
		}
		case '\u03d2':  case '\u03d3':  case '\u03d4':
		{
			matchRange('\u03D2','\u03D4');
			break;
		}
		case '\u03d8':
		{
			match('\u03D8');
			break;
		}
		case '\u03da':
		{
			match('\u03DA');
			break;
		}
		case '\u03dc':
		{
			match('\u03DC');
			break;
		}
		case '\u03de':
		{
			match('\u03DE');
			break;
		}
		case '\u03e0':
		{
			match('\u03E0');
			break;
		}
		case '\u03e2':
		{
			match('\u03E2');
			break;
		}
		case '\u03e4':
		{
			match('\u03E4');
			break;
		}
		case '\u03e6':
		{
			match('\u03E6');
			break;
		}
		case '\u03e8':
		{
			match('\u03E8');
			break;
		}
		case '\u03ea':
		{
			match('\u03EA');
			break;
		}
		case '\u03ec':
		{
			match('\u03EC');
			break;
		}
		case '\u03ee':
		{
			match('\u03EE');
			break;
		}
		case '\u03f4':
		{
			match('\u03F4');
			break;
		}
		case '\u03f7':
		{
			match('\u03F7');
			break;
		}
		case '\u03f9':
		{
			match('\u03F9');
			break;
		}
		case '\u03fa':
		{
			match('\u03FA');
			break;
		}
		case '\u03fd':  case '\u03fe':  case '\u03ff':  case '\u0400':
		case '\u0401':  case '\u0402':  case '\u0403':  case '\u0404':
		case '\u0405':  case '\u0406':  case '\u0407':  case '\u0408':
		case '\u0409':  case '\u040a':  case '\u040b':  case '\u040c':
		case '\u040d':  case '\u040e':  case '\u040f':  case '\u0410':
		case '\u0411':  case '\u0412':  case '\u0413':  case '\u0414':
		case '\u0415':  case '\u0416':  case '\u0417':  case '\u0418':
		case '\u0419':  case '\u041a':  case '\u041b':  case '\u041c':
		case '\u041d':  case '\u041e':  case '\u041f':  case '\u0420':
		case '\u0421':  case '\u0422':  case '\u0423':  case '\u0424':
		case '\u0425':  case '\u0426':  case '\u0427':  case '\u0428':
		case '\u0429':  case '\u042a':  case '\u042b':  case '\u042c':
		case '\u042d':  case '\u042e':  case '\u042f':
		{
			matchRange('\u03FD','\u042F');
			break;
		}
		case '\u0460':
		{
			match('\u0460');
			break;
		}
		case '\u0462':
		{
			match('\u0462');
			break;
		}
		case '\u0464':
		{
			match('\u0464');
			break;
		}
		case '\u0466':
		{
			match('\u0466');
			break;
		}
		case '\u0468':
		{
			match('\u0468');
			break;
		}
		case '\u046a':
		{
			match('\u046A');
			break;
		}
		case '\u046c':
		{
			match('\u046C');
			break;
		}
		case '\u046e':
		{
			match('\u046E');
			break;
		}
		case '\u0470':
		{
			match('\u0470');
			break;
		}
		case '\u0472':
		{
			match('\u0472');
			break;
		}
		case '\u0474':
		{
			match('\u0474');
			break;
		}
		case '\u0476':
		{
			match('\u0476');
			break;
		}
		case '\u0478':
		{
			match('\u0478');
			break;
		}
		case '\u047a':
		{
			match('\u047A');
			break;
		}
		case '\u047c':
		{
			match('\u047C');
			break;
		}
		case '\u047e':
		{
			match('\u047E');
			break;
		}
		case '\u0480':
		{
			match('\u0480');
			break;
		}
		case '\u048a':
		{
			match('\u048A');
			break;
		}
		case '\u048c':
		{
			match('\u048C');
			break;
		}
		case '\u048e':
		{
			match('\u048E');
			break;
		}
		case '\u0490':
		{
			match('\u0490');
			break;
		}
		case '\u0492':
		{
			match('\u0492');
			break;
		}
		case '\u0494':
		{
			match('\u0494');
			break;
		}
		case '\u0496':
		{
			match('\u0496');
			break;
		}
		case '\u0498':
		{
			match('\u0498');
			break;
		}
		case '\u049a':
		{
			match('\u049A');
			break;
		}
		case '\u049c':
		{
			match('\u049C');
			break;
		}
		case '\u049e':
		{
			match('\u049E');
			break;
		}
		case '\u04a0':
		{
			match('\u04A0');
			break;
		}
		case '\u04a2':
		{
			match('\u04A2');
			break;
		}
		case '\u04a4':
		{
			match('\u04A4');
			break;
		}
		case '\u04a6':
		{
			match('\u04A6');
			break;
		}
		case '\u04a8':
		{
			match('\u04A8');
			break;
		}
		case '\u04aa':
		{
			match('\u04AA');
			break;
		}
		case '\u04ac':
		{
			match('\u04AC');
			break;
		}
		case '\u04ae':
		{
			match('\u04AE');
			break;
		}
		case '\u04b0':
		{
			match('\u04B0');
			break;
		}
		case '\u04b2':
		{
			match('\u04B2');
			break;
		}
		case '\u04b4':
		{
			match('\u04B4');
			break;
		}
		case '\u04b6':
		{
			match('\u04B6');
			break;
		}
		case '\u04b8':
		{
			match('\u04B8');
			break;
		}
		case '\u04ba':
		{
			match('\u04BA');
			break;
		}
		case '\u04bc':
		{
			match('\u04BC');
			break;
		}
		case '\u04be':
		{
			match('\u04BE');
			break;
		}
		case '\u04c0':
		{
			match('\u04C0');
			break;
		}
		case '\u04c1':
		{
			match('\u04C1');
			break;
		}
		case '\u04c3':
		{
			match('\u04C3');
			break;
		}
		case '\u04c5':
		{
			match('\u04C5');
			break;
		}
		case '\u04c7':
		{
			match('\u04C7');
			break;
		}
		case '\u04c9':
		{
			match('\u04C9');
			break;
		}
		case '\u04cb':
		{
			match('\u04CB');
			break;
		}
		case '\u04cd':
		{
			match('\u04CD');
			break;
		}
		case '\u04d0':
		{
			match('\u04D0');
			break;
		}
		case '\u04d2':
		{
			match('\u04D2');
			break;
		}
		case '\u04d4':
		{
			match('\u04D4');
			break;
		}
		case '\u04d6':
		{
			match('\u04D6');
			break;
		}
		case '\u04d8':
		{
			match('\u04D8');
			break;
		}
		case '\u04da':
		{
			match('\u04DA');
			break;
		}
		case '\u04dc':
		{
			match('\u04DC');
			break;
		}
		case '\u04de':
		{
			match('\u04DE');
			break;
		}
		case '\u04e0':
		{
			match('\u04E0');
			break;
		}
		case '\u04e2':
		{
			match('\u04E2');
			break;
		}
		case '\u04e4':
		{
			match('\u04E4');
			break;
		}
		case '\u04e6':
		{
			match('\u04E6');
			break;
		}
		case '\u04e8':
		{
			match('\u04E8');
			break;
		}
		case '\u04ea':
		{
			match('\u04EA');
			break;
		}
		case '\u04ec':
		{
			match('\u04EC');
			break;
		}
		case '\u04ee':
		{
			match('\u04EE');
			break;
		}
		case '\u04f0':
		{
			match('\u04F0');
			break;
		}
		case '\u04f2':
		{
			match('\u04F2');
			break;
		}
		case '\u04f4':
		{
			match('\u04F4');
			break;
		}
		case '\u04f6':
		{
			match('\u04F6');
			break;
		}
		case '\u04f8':
		{
			match('\u04F8');
			break;
		}
		case '\u04fa':
		{
			match('\u04FA');
			break;
		}
		case '\u04fc':
		{
			match('\u04FC');
			break;
		}
		case '\u04fe':
		{
			match('\u04FE');
			break;
		}
		case '\u0500':
		{
			match('\u0500');
			break;
		}
		case '\u0502':
		{
			match('\u0502');
			break;
		}
		case '\u0504':
		{
			match('\u0504');
			break;
		}
		case '\u0506':
		{
			match('\u0506');
			break;
		}
		case '\u0508':
		{
			match('\u0508');
			break;
		}
		case '\u050a':
		{
			match('\u050A');
			break;
		}
		case '\u050c':
		{
			match('\u050C');
			break;
		}
		case '\u050e':
		{
			match('\u050E');
			break;
		}
		case '\u0510':
		{
			match('\u0510');
			break;
		}
		case '\u0512':
		{
			match('\u0512');
			break;
		}
		case '\u0514':
		{
			match('\u0514');
			break;
		}
		case '\u0516':
		{
			match('\u0516');
			break;
		}
		case '\u0518':
		{
			match('\u0518');
			break;
		}
		case '\u051a':
		{
			match('\u051A');
			break;
		}
		case '\u051c':
		{
			match('\u051C');
			break;
		}
		case '\u051e':
		{
			match('\u051E');
			break;
		}
		case '\u0520':
		{
			match('\u0520');
			break;
		}
		case '\u0522':
		{
			match('\u0522');
			break;
		}
		case '\u0524':
		{
			match('\u0524');
			break;
		}
		case '\u0526':
		{
			match('\u0526');
			break;
		}
		case '\u0531':  case '\u0532':  case '\u0533':  case '\u0534':
		case '\u0535':  case '\u0536':  case '\u0537':  case '\u0538':
		case '\u0539':  case '\u053a':  case '\u053b':  case '\u053c':
		case '\u053d':  case '\u053e':  case '\u053f':  case '\u0540':
		case '\u0541':  case '\u0542':  case '\u0543':  case '\u0544':
		case '\u0545':  case '\u0546':  case '\u0547':  case '\u0548':
		case '\u0549':  case '\u054a':  case '\u054b':  case '\u054c':
		case '\u054d':  case '\u054e':  case '\u054f':  case '\u0550':
		case '\u0551':  case '\u0552':  case '\u0553':  case '\u0554':
		case '\u0555':  case '\u0556':
		{
			matchRange('\u0531','\u0556');
			break;
		}
		case '\u10a0':  case '\u10a1':  case '\u10a2':  case '\u10a3':
		case '\u10a4':  case '\u10a5':  case '\u10a6':  case '\u10a7':
		case '\u10a8':  case '\u10a9':  case '\u10aa':  case '\u10ab':
		case '\u10ac':  case '\u10ad':  case '\u10ae':  case '\u10af':
		case '\u10b0':  case '\u10b1':  case '\u10b2':  case '\u10b3':
		case '\u10b4':  case '\u10b5':  case '\u10b6':  case '\u10b7':
		case '\u10b8':  case '\u10b9':  case '\u10ba':  case '\u10bb':
		case '\u10bc':  case '\u10bd':  case '\u10be':  case '\u10bf':
		case '\u10c0':  case '\u10c1':  case '\u10c2':  case '\u10c3':
		case '\u10c4':  case '\u10c5':
		{
			matchRange('\u10A0','\u10C5');
			break;
		}
		case '\u10c7':
		{
			match('\u10C7');
			break;
		}
		case '\u10cd':
		{
			match('\u10CD');
			break;
		}
		case '\u1e00':
		{
			match('\u1E00');
			break;
		}
		case '\u1e02':
		{
			match('\u1E02');
			break;
		}
		case '\u1e04':
		{
			match('\u1E04');
			break;
		}
		case '\u1e06':
		{
			match('\u1E06');
			break;
		}
		case '\u1e08':
		{
			match('\u1E08');
			break;
		}
		case '\u1e0a':
		{
			match('\u1E0A');
			break;
		}
		case '\u1e0c':
		{
			match('\u1E0C');
			break;
		}
		case '\u1e0e':
		{
			match('\u1E0E');
			break;
		}
		case '\u1e10':
		{
			match('\u1E10');
			break;
		}
		case '\u1e12':
		{
			match('\u1E12');
			break;
		}
		case '\u1e14':
		{
			match('\u1E14');
			break;
		}
		case '\u1e16':
		{
			match('\u1E16');
			break;
		}
		case '\u1e18':
		{
			match('\u1E18');
			break;
		}
		case '\u1e1a':
		{
			match('\u1E1A');
			break;
		}
		case '\u1e1c':
		{
			match('\u1E1C');
			break;
		}
		case '\u1e1e':
		{
			match('\u1E1E');
			break;
		}
		case '\u1e20':
		{
			match('\u1E20');
			break;
		}
		case '\u1e22':
		{
			match('\u1E22');
			break;
		}
		case '\u1e24':
		{
			match('\u1E24');
			break;
		}
		case '\u1e26':
		{
			match('\u1E26');
			break;
		}
		case '\u1e28':
		{
			match('\u1E28');
			break;
		}
		case '\u1e2a':
		{
			match('\u1E2A');
			break;
		}
		case '\u1e2c':
		{
			match('\u1E2C');
			break;
		}
		case '\u1e2e':
		{
			match('\u1E2E');
			break;
		}
		case '\u1e30':
		{
			match('\u1E30');
			break;
		}
		case '\u1e32':
		{
			match('\u1E32');
			break;
		}
		case '\u1e34':
		{
			match('\u1E34');
			break;
		}
		case '\u1e36':
		{
			match('\u1E36');
			break;
		}
		case '\u1e38':
		{
			match('\u1E38');
			break;
		}
		case '\u1e3a':
		{
			match('\u1E3A');
			break;
		}
		case '\u1e3c':
		{
			match('\u1E3C');
			break;
		}
		case '\u1e3e':
		{
			match('\u1E3E');
			break;
		}
		case '\u1e40':
		{
			match('\u1E40');
			break;
		}
		case '\u1e42':
		{
			match('\u1E42');
			break;
		}
		case '\u1e44':
		{
			match('\u1E44');
			break;
		}
		case '\u1e46':
		{
			match('\u1E46');
			break;
		}
		case '\u1e48':
		{
			match('\u1E48');
			break;
		}
		case '\u1e4a':
		{
			match('\u1E4A');
			break;
		}
		case '\u1e4c':
		{
			match('\u1E4C');
			break;
		}
		case '\u1e4e':
		{
			match('\u1E4E');
			break;
		}
		case '\u1e50':
		{
			match('\u1E50');
			break;
		}
		case '\u1e52':
		{
			match('\u1E52');
			break;
		}
		case '\u1e54':
		{
			match('\u1E54');
			break;
		}
		case '\u1e56':
		{
			match('\u1E56');
			break;
		}
		case '\u1e58':
		{
			match('\u1E58');
			break;
		}
		case '\u1e5a':
		{
			match('\u1E5A');
			break;
		}
		case '\u1e5c':
		{
			match('\u1E5C');
			break;
		}
		case '\u1e5e':
		{
			match('\u1E5E');
			break;
		}
		case '\u1e60':
		{
			match('\u1E60');
			break;
		}
		case '\u1e62':
		{
			match('\u1E62');
			break;
		}
		case '\u1e64':
		{
			match('\u1E64');
			break;
		}
		case '\u1e66':
		{
			match('\u1E66');
			break;
		}
		case '\u1e68':
		{
			match('\u1E68');
			break;
		}
		case '\u1e6a':
		{
			match('\u1E6A');
			break;
		}
		case '\u1e6c':
		{
			match('\u1E6C');
			break;
		}
		case '\u1e6e':
		{
			match('\u1E6E');
			break;
		}
		case '\u1e70':
		{
			match('\u1E70');
			break;
		}
		case '\u1e72':
		{
			match('\u1E72');
			break;
		}
		case '\u1e74':
		{
			match('\u1E74');
			break;
		}
		case '\u1e76':
		{
			match('\u1E76');
			break;
		}
		case '\u1e78':
		{
			match('\u1E78');
			break;
		}
		case '\u1e7a':
		{
			match('\u1E7A');
			break;
		}
		case '\u1e7c':
		{
			match('\u1E7C');
			break;
		}
		case '\u1e7e':
		{
			match('\u1E7E');
			break;
		}
		case '\u1e80':
		{
			match('\u1E80');
			break;
		}
		case '\u1e82':
		{
			match('\u1E82');
			break;
		}
		case '\u1e84':
		{
			match('\u1E84');
			break;
		}
		case '\u1e86':
		{
			match('\u1E86');
			break;
		}
		case '\u1e88':
		{
			match('\u1E88');
			break;
		}
		case '\u1e8a':
		{
			match('\u1E8A');
			break;
		}
		case '\u1e8c':
		{
			match('\u1E8C');
			break;
		}
		case '\u1e8e':
		{
			match('\u1E8E');
			break;
		}
		case '\u1e90':
		{
			match('\u1E90');
			break;
		}
		case '\u1e92':
		{
			match('\u1E92');
			break;
		}
		case '\u1e94':
		{
			match('\u1E94');
			break;
		}
		case '\u1e9e':
		{
			match('\u1E9E');
			break;
		}
		case '\u1ea0':
		{
			match('\u1EA0');
			break;
		}
		case '\u1ea2':
		{
			match('\u1EA2');
			break;
		}
		case '\u1ea4':
		{
			match('\u1EA4');
			break;
		}
		case '\u1ea6':
		{
			match('\u1EA6');
			break;
		}
		case '\u1ea8':
		{
			match('\u1EA8');
			break;
		}
		case '\u1eaa':
		{
			match('\u1EAA');
			break;
		}
		case '\u1eac':
		{
			match('\u1EAC');
			break;
		}
		case '\u1eae':
		{
			match('\u1EAE');
			break;
		}
		case '\u1eb0':
		{
			match('\u1EB0');
			break;
		}
		case '\u1eb2':
		{
			match('\u1EB2');
			break;
		}
		case '\u1eb4':
		{
			match('\u1EB4');
			break;
		}
		case '\u1eb6':
		{
			match('\u1EB6');
			break;
		}
		case '\u1eb8':
		{
			match('\u1EB8');
			break;
		}
		case '\u1eba':
		{
			match('\u1EBA');
			break;
		}
		case '\u1ebc':
		{
			match('\u1EBC');
			break;
		}
		case '\u1ebe':
		{
			match('\u1EBE');
			break;
		}
		case '\u1ec0':
		{
			match('\u1EC0');
			break;
		}
		case '\u1ec2':
		{
			match('\u1EC2');
			break;
		}
		case '\u1ec4':
		{
			match('\u1EC4');
			break;
		}
		case '\u1ec6':
		{
			match('\u1EC6');
			break;
		}
		case '\u1ec8':
		{
			match('\u1EC8');
			break;
		}
		case '\u1eca':
		{
			match('\u1ECA');
			break;
		}
		case '\u1ecc':
		{
			match('\u1ECC');
			break;
		}
		case '\u1ece':
		{
			match('\u1ECE');
			break;
		}
		case '\u1ed0':
		{
			match('\u1ED0');
			break;
		}
		case '\u1ed2':
		{
			match('\u1ED2');
			break;
		}
		case '\u1ed4':
		{
			match('\u1ED4');
			break;
		}
		case '\u1ed6':
		{
			match('\u1ED6');
			break;
		}
		case '\u1ed8':
		{
			match('\u1ED8');
			break;
		}
		case '\u1eda':
		{
			match('\u1EDA');
			break;
		}
		case '\u1edc':
		{
			match('\u1EDC');
			break;
		}
		case '\u1ede':
		{
			match('\u1EDE');
			break;
		}
		case '\u1ee0':
		{
			match('\u1EE0');
			break;
		}
		case '\u1ee2':
		{
			match('\u1EE2');
			break;
		}
		case '\u1ee4':
		{
			match('\u1EE4');
			break;
		}
		case '\u1ee6':
		{
			match('\u1EE6');
			break;
		}
		case '\u1ee8':
		{
			match('\u1EE8');
			break;
		}
		case '\u1eea':
		{
			match('\u1EEA');
			break;
		}
		case '\u1eec':
		{
			match('\u1EEC');
			break;
		}
		case '\u1eee':
		{
			match('\u1EEE');
			break;
		}
		case '\u1ef0':
		{
			match('\u1EF0');
			break;
		}
		case '\u1ef2':
		{
			match('\u1EF2');
			break;
		}
		case '\u1ef4':
		{
			match('\u1EF4');
			break;
		}
		case '\u1ef6':
		{
			match('\u1EF6');
			break;
		}
		case '\u1ef8':
		{
			match('\u1EF8');
			break;
		}
		case '\u1efa':
		{
			match('\u1EFA');
			break;
		}
		case '\u1efc':
		{
			match('\u1EFC');
			break;
		}
		case '\u1efe':
		{
			match('\u1EFE');
			break;
		}
		case '\u1f08':  case '\u1f09':  case '\u1f0a':  case '\u1f0b':
		case '\u1f0c':  case '\u1f0d':  case '\u1f0e':  case '\u1f0f':
		{
			matchRange('\u1F08','\u1F0F');
			break;
		}
		case '\u1f18':  case '\u1f19':  case '\u1f1a':  case '\u1f1b':
		case '\u1f1c':  case '\u1f1d':
		{
			matchRange('\u1F18','\u1F1D');
			break;
		}
		case '\u1f28':  case '\u1f29':  case '\u1f2a':  case '\u1f2b':
		case '\u1f2c':  case '\u1f2d':  case '\u1f2e':  case '\u1f2f':
		{
			matchRange('\u1F28','\u1F2F');
			break;
		}
		case '\u1f38':  case '\u1f39':  case '\u1f3a':  case '\u1f3b':
		case '\u1f3c':  case '\u1f3d':  case '\u1f3e':  case '\u1f3f':
		{
			matchRange('\u1F38','\u1F3F');
			break;
		}
		case '\u1f48':  case '\u1f49':  case '\u1f4a':  case '\u1f4b':
		case '\u1f4c':  case '\u1f4d':
		{
			matchRange('\u1F48','\u1F4D');
			break;
		}
		case '\u1f59':
		{
			match('\u1F59');
			break;
		}
		case '\u1f5b':
		{
			match('\u1F5B');
			break;
		}
		case '\u1f5d':
		{
			match('\u1F5D');
			break;
		}
		case '\u1f5f':
		{
			match('\u1F5F');
			break;
		}
		case '\u1f68':  case '\u1f69':  case '\u1f6a':  case '\u1f6b':
		case '\u1f6c':  case '\u1f6d':  case '\u1f6e':  case '\u1f6f':
		{
			matchRange('\u1F68','\u1F6F');
			break;
		}
		case '\u1fb8':  case '\u1fb9':  case '\u1fba':  case '\u1fbb':
		{
			matchRange('\u1FB8','\u1FBB');
			break;
		}
		case '\u1fc8':  case '\u1fc9':  case '\u1fca':  case '\u1fcb':
		{
			matchRange('\u1FC8','\u1FCB');
			break;
		}
		case '\u1fd8':  case '\u1fd9':  case '\u1fda':  case '\u1fdb':
		{
			matchRange('\u1FD8','\u1FDB');
			break;
		}
		case '\u1fe8':  case '\u1fe9':  case '\u1fea':  case '\u1feb':
		case '\u1fec':
		{
			matchRange('\u1FE8','\u1FEC');
			break;
		}
		case '\u1ff8':  case '\u1ff9':  case '\u1ffa':  case '\u1ffb':
		{
			matchRange('\u1FF8','\u1FFB');
			break;
		}
		case '\u2102':
		{
			match('\u2102');
			break;
		}
		case '\u2107':
		{
			match('\u2107');
			break;
		}
		case '\u210b':  case '\u210c':  case '\u210d':
		{
			matchRange('\u210B','\u210D');
			break;
		}
		case '\u2110':  case '\u2111':  case '\u2112':
		{
			matchRange('\u2110','\u2112');
			break;
		}
		case '\u2115':
		{
			match('\u2115');
			break;
		}
		case '\u2119':  case '\u211a':  case '\u211b':  case '\u211c':
		case '\u211d':
		{
			matchRange('\u2119','\u211D');
			break;
		}
		case '\u2124':
		{
			match('\u2124');
			break;
		}
		case '\u2126':
		{
			match('\u2126');
			break;
		}
		case '\u2128':
		{
			match('\u2128');
			break;
		}
		case '\u212a':  case '\u212b':  case '\u212c':  case '\u212d':
		{
			matchRange('\u212A','\u212D');
			break;
		}
		case '\u2130':  case '\u2131':  case '\u2132':  case '\u2133':
		{
			matchRange('\u2130','\u2133');
			break;
		}
		case '\u213e':
		{
			match('\u213E');
			break;
		}
		case '\u213f':
		{
			match('\u213F');
			break;
		}
		case '\u2145':
		{
			match('\u2145');
			break;
		}
		case '\u2183':
		{
			match('\u2183');
			break;
		}
		case '\u2c00':  case '\u2c01':  case '\u2c02':  case '\u2c03':
		case '\u2c04':  case '\u2c05':  case '\u2c06':  case '\u2c07':
		case '\u2c08':  case '\u2c09':  case '\u2c0a':  case '\u2c0b':
		case '\u2c0c':  case '\u2c0d':  case '\u2c0e':  case '\u2c0f':
		case '\u2c10':  case '\u2c11':  case '\u2c12':  case '\u2c13':
		case '\u2c14':  case '\u2c15':  case '\u2c16':  case '\u2c17':
		case '\u2c18':  case '\u2c19':  case '\u2c1a':  case '\u2c1b':
		case '\u2c1c':  case '\u2c1d':  case '\u2c1e':  case '\u2c1f':
		case '\u2c20':  case '\u2c21':  case '\u2c22':  case '\u2c23':
		case '\u2c24':  case '\u2c25':  case '\u2c26':  case '\u2c27':
		case '\u2c28':  case '\u2c29':  case '\u2c2a':  case '\u2c2b':
		case '\u2c2c':  case '\u2c2d':  case '\u2c2e':
		{
			matchRange('\u2C00','\u2C2E');
			break;
		}
		case '\u2c60':
		{
			match('\u2C60');
			break;
		}
		case '\u2c62':  case '\u2c63':  case '\u2c64':
		{
			matchRange('\u2C62','\u2C64');
			break;
		}
		case '\u2c67':
		{
			match('\u2C67');
			break;
		}
		case '\u2c69':
		{
			match('\u2C69');
			break;
		}
		case '\u2c6b':
		{
			match('\u2C6B');
			break;
		}
		case '\u2c6d':  case '\u2c6e':  case '\u2c6f':  case '\u2c70':
		{
			matchRange('\u2C6D','\u2C70');
			break;
		}
		case '\u2c72':
		{
			match('\u2C72');
			break;
		}
		case '\u2c75':
		{
			match('\u2C75');
			break;
		}
		case '\u2c7e':  case '\u2c7f':  case '\u2c80':
		{
			matchRange('\u2C7E','\u2C80');
			break;
		}
		case '\u2c82':
		{
			match('\u2C82');
			break;
		}
		case '\u2c84':
		{
			match('\u2C84');
			break;
		}
		case '\u2c86':
		{
			match('\u2C86');
			break;
		}
		case '\u2c88':
		{
			match('\u2C88');
			break;
		}
		case '\u2c8a':
		{
			match('\u2C8A');
			break;
		}
		case '\u2c8c':
		{
			match('\u2C8C');
			break;
		}
		case '\u2c8e':
		{
			match('\u2C8E');
			break;
		}
		case '\u2c90':
		{
			match('\u2C90');
			break;
		}
		case '\u2c92':
		{
			match('\u2C92');
			break;
		}
		case '\u2c94':
		{
			match('\u2C94');
			break;
		}
		case '\u2c96':
		{
			match('\u2C96');
			break;
		}
		case '\u2c98':
		{
			match('\u2C98');
			break;
		}
		case '\u2c9a':
		{
			match('\u2C9A');
			break;
		}
		case '\u2c9c':
		{
			match('\u2C9C');
			break;
		}
		case '\u2c9e':
		{
			match('\u2C9E');
			break;
		}
		case '\u2ca0':
		{
			match('\u2CA0');
			break;
		}
		case '\u2ca2':
		{
			match('\u2CA2');
			break;
		}
		case '\u2ca4':
		{
			match('\u2CA4');
			break;
		}
		case '\u2ca6':
		{
			match('\u2CA6');
			break;
		}
		case '\u2ca8':
		{
			match('\u2CA8');
			break;
		}
		case '\u2caa':
		{
			match('\u2CAA');
			break;
		}
		case '\u2cac':
		{
			match('\u2CAC');
			break;
		}
		case '\u2cae':
		{
			match('\u2CAE');
			break;
		}
		case '\u2cb0':
		{
			match('\u2CB0');
			break;
		}
		case '\u2cb2':
		{
			match('\u2CB2');
			break;
		}
		case '\u2cb4':
		{
			match('\u2CB4');
			break;
		}
		case '\u2cb6':
		{
			match('\u2CB6');
			break;
		}
		case '\u2cb8':
		{
			match('\u2CB8');
			break;
		}
		case '\u2cba':
		{
			match('\u2CBA');
			break;
		}
		case '\u2cbc':
		{
			match('\u2CBC');
			break;
		}
		case '\u2cbe':
		{
			match('\u2CBE');
			break;
		}
		case '\u2cc0':
		{
			match('\u2CC0');
			break;
		}
		case '\u2cc2':
		{
			match('\u2CC2');
			break;
		}
		case '\u2cc4':
		{
			match('\u2CC4');
			break;
		}
		case '\u2cc6':
		{
			match('\u2CC6');
			break;
		}
		case '\u2cc8':
		{
			match('\u2CC8');
			break;
		}
		case '\u2cca':
		{
			match('\u2CCA');
			break;
		}
		case '\u2ccc':
		{
			match('\u2CCC');
			break;
		}
		case '\u2cce':
		{
			match('\u2CCE');
			break;
		}
		case '\u2cd0':
		{
			match('\u2CD0');
			break;
		}
		case '\u2cd2':
		{
			match('\u2CD2');
			break;
		}
		case '\u2cd4':
		{
			match('\u2CD4');
			break;
		}
		case '\u2cd6':
		{
			match('\u2CD6');
			break;
		}
		case '\u2cd8':
		{
			match('\u2CD8');
			break;
		}
		case '\u2cda':
		{
			match('\u2CDA');
			break;
		}
		case '\u2cdc':
		{
			match('\u2CDC');
			break;
		}
		case '\u2cde':
		{
			match('\u2CDE');
			break;
		}
		case '\u2ce0':
		{
			match('\u2CE0');
			break;
		}
		case '\u2ce2':
		{
			match('\u2CE2');
			break;
		}
		case '\u2ceb':
		{
			match('\u2CEB');
			break;
		}
		case '\u2ced':
		{
			match('\u2CED');
			break;
		}
		case '\u2cf2':
		{
			match('\u2CF2');
			break;
		}
		case '\ua640':
		{
			match('\uA640');
			break;
		}
		case '\ua642':
		{
			match('\uA642');
			break;
		}
		case '\ua644':
		{
			match('\uA644');
			break;
		}
		case '\ua646':
		{
			match('\uA646');
			break;
		}
		case '\ua648':
		{
			match('\uA648');
			break;
		}
		case '\ua64a':
		{
			match('\uA64A');
			break;
		}
		case '\ua64c':
		{
			match('\uA64C');
			break;
		}
		case '\ua64e':
		{
			match('\uA64E');
			break;
		}
		case '\ua650':
		{
			match('\uA650');
			break;
		}
		case '\ua652':
		{
			match('\uA652');
			break;
		}
		case '\ua654':
		{
			match('\uA654');
			break;
		}
		case '\ua656':
		{
			match('\uA656');
			break;
		}
		case '\ua658':
		{
			match('\uA658');
			break;
		}
		case '\ua65a':
		{
			match('\uA65A');
			break;
		}
		case '\ua65c':
		{
			match('\uA65C');
			break;
		}
		case '\ua65e':
		{
			match('\uA65E');
			break;
		}
		case '\ua660':
		{
			match('\uA660');
			break;
		}
		case '\ua662':
		{
			match('\uA662');
			break;
		}
		case '\ua664':
		{
			match('\uA664');
			break;
		}
		case '\ua666':
		{
			match('\uA666');
			break;
		}
		case '\ua668':
		{
			match('\uA668');
			break;
		}
		case '\ua66a':
		{
			match('\uA66A');
			break;
		}
		case '\ua66c':
		{
			match('\uA66C');
			break;
		}
		case '\ua680':
		{
			match('\uA680');
			break;
		}
		case '\ua682':
		{
			match('\uA682');
			break;
		}
		case '\ua684':
		{
			match('\uA684');
			break;
		}
		case '\ua686':
		{
			match('\uA686');
			break;
		}
		case '\ua688':
		{
			match('\uA688');
			break;
		}
		case '\ua68a':
		{
			match('\uA68A');
			break;
		}
		case '\ua68c':
		{
			match('\uA68C');
			break;
		}
		case '\ua68e':
		{
			match('\uA68E');
			break;
		}
		case '\ua690':
		{
			match('\uA690');
			break;
		}
		case '\ua692':
		{
			match('\uA692');
			break;
		}
		case '\ua694':
		{
			match('\uA694');
			break;
		}
		case '\ua696':
		{
			match('\uA696');
			break;
		}
		case '\ua722':
		{
			match('\uA722');
			break;
		}
		case '\ua724':
		{
			match('\uA724');
			break;
		}
		case '\ua726':
		{
			match('\uA726');
			break;
		}
		case '\ua728':
		{
			match('\uA728');
			break;
		}
		case '\ua72a':
		{
			match('\uA72A');
			break;
		}
		case '\ua72c':
		{
			match('\uA72C');
			break;
		}
		case '\ua72e':
		{
			match('\uA72E');
			break;
		}
		case '\ua732':
		{
			match('\uA732');
			break;
		}
		case '\ua734':
		{
			match('\uA734');
			break;
		}
		case '\ua736':
		{
			match('\uA736');
			break;
		}
		case '\ua738':
		{
			match('\uA738');
			break;
		}
		case '\ua73a':
		{
			match('\uA73A');
			break;
		}
		case '\ua73c':
		{
			match('\uA73C');
			break;
		}
		case '\ua73e':
		{
			match('\uA73E');
			break;
		}
		case '\ua740':
		{
			match('\uA740');
			break;
		}
		case '\ua742':
		{
			match('\uA742');
			break;
		}
		case '\ua744':
		{
			match('\uA744');
			break;
		}
		case '\ua746':
		{
			match('\uA746');
			break;
		}
		case '\ua748':
		{
			match('\uA748');
			break;
		}
		case '\ua74a':
		{
			match('\uA74A');
			break;
		}
		case '\ua74c':
		{
			match('\uA74C');
			break;
		}
		case '\ua74e':
		{
			match('\uA74E');
			break;
		}
		case '\ua750':
		{
			match('\uA750');
			break;
		}
		case '\ua752':
		{
			match('\uA752');
			break;
		}
		case '\ua754':
		{
			match('\uA754');
			break;
		}
		case '\ua756':
		{
			match('\uA756');
			break;
		}
		case '\ua758':
		{
			match('\uA758');
			break;
		}
		case '\ua75a':
		{
			match('\uA75A');
			break;
		}
		case '\ua75c':
		{
			match('\uA75C');
			break;
		}
		case '\ua75e':
		{
			match('\uA75E');
			break;
		}
		case '\ua760':
		{
			match('\uA760');
			break;
		}
		case '\ua762':
		{
			match('\uA762');
			break;
		}
		case '\ua764':
		{
			match('\uA764');
			break;
		}
		case '\ua766':
		{
			match('\uA766');
			break;
		}
		case '\ua768':
		{
			match('\uA768');
			break;
		}
		case '\ua76a':
		{
			match('\uA76A');
			break;
		}
		case '\ua76c':
		{
			match('\uA76C');
			break;
		}
		case '\ua76e':
		{
			match('\uA76E');
			break;
		}
		case '\ua779':
		{
			match('\uA779');
			break;
		}
		case '\ua77b':
		{
			match('\uA77B');
			break;
		}
		case '\ua77d':
		{
			match('\uA77D');
			break;
		}
		case '\ua77e':
		{
			match('\uA77E');
			break;
		}
		case '\ua780':
		{
			match('\uA780');
			break;
		}
		case '\ua782':
		{
			match('\uA782');
			break;
		}
		case '\ua784':
		{
			match('\uA784');
			break;
		}
		case '\ua786':
		{
			match('\uA786');
			break;
		}
		case '\ua78b':
		{
			match('\uA78B');
			break;
		}
		case '\ua78d':
		{
			match('\uA78D');
			break;
		}
		case '\ua790':
		{
			match('\uA790');
			break;
		}
		case '\ua792':
		{
			match('\uA792');
			break;
		}
		case '\ua7a0':
		{
			match('\uA7A0');
			break;
		}
		case '\ua7a2':
		{
			match('\uA7A2');
			break;
		}
		case '\ua7a4':
		{
			match('\uA7A4');
			break;
		}
		case '\ua7a6':
		{
			match('\uA7A6');
			break;
		}
		case '\ua7a8':
		{
			match('\uA7A8');
			break;
		}
		case '\ua7aa':
		{
			match('\uA7AA');
			break;
		}
		case '\uff21':  case '\uff22':  case '\uff23':  case '\uff24':
		case '\uff25':  case '\uff26':  case '\uff27':  case '\uff28':
		case '\uff29':  case '\uff2a':  case '\uff2b':  case '\uff2c':
		case '\uff2d':  case '\uff2e':  case '\uff2f':  case '\uff30':
		case '\uff31':  case '\uff32':  case '\uff33':  case '\uff34':
		case '\uff35':  case '\uff36':  case '\uff37':  case '\uff38':
		case '\uff39':  case '\uff3a':
		{
			matchRange('\uFF21','\uFF3A');
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mUNICODE_CLASS_LL(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = UNICODE_CLASS_LL;
		int _saveIndex;
		
		switch ( LA(1)) {
		case 'a':  case 'b':  case 'c':  case 'd':
		case 'e':  case 'f':  case 'g':  case 'h':
		case 'i':  case 'j':  case 'k':  case 'l':
		case 'm':  case 'n':  case 'o':  case 'p':
		case 'q':  case 'r':  case 's':  case 't':
		case 'u':  case 'v':  case 'w':  case 'x':
		case 'y':  case 'z':
		{
			matchRange('\u0061','\u007A');
			break;
		}
		case '\u00b5':
		{
			match('\u00B5');
			break;
		}
		case '\u00df':  case '\u00e0':  case '\u00e1':  case '\u00e2':
		case '\u00e3':  case '\u00e4':  case '\u00e5':  case '\u00e6':
		case '\u00e7':  case '\u00e8':  case '\u00e9':  case '\u00ea':
		case '\u00eb':  case '\u00ec':  case '\u00ed':  case '\u00ee':
		case '\u00ef':  case '\u00f0':  case '\u00f1':  case '\u00f2':
		case '\u00f3':  case '\u00f4':  case '\u00f5':  case '\u00f6':
		{
			matchRange('\u00DF','\u00F6');
			break;
		}
		case '\u00f8':  case '\u00f9':  case '\u00fa':  case '\u00fb':
		case '\u00fc':  case '\u00fd':  case '\u00fe':  case '\u00ff':
		{
			matchRange('\u00F8','\u00FF');
			break;
		}
		case '\u0101':
		{
			match('\u0101');
			break;
		}
		case '\u0103':
		{
			match('\u0103');
			break;
		}
		case '\u0105':
		{
			match('\u0105');
			break;
		}
		case '\u0107':
		{
			match('\u0107');
			break;
		}
		case '\u0109':
		{
			match('\u0109');
			break;
		}
		case '\u010b':
		{
			match('\u010B');
			break;
		}
		case '\u010d':
		{
			match('\u010D');
			break;
		}
		case '\u010f':
		{
			match('\u010F');
			break;
		}
		case '\u0111':
		{
			match('\u0111');
			break;
		}
		case '\u0113':
		{
			match('\u0113');
			break;
		}
		case '\u0115':
		{
			match('\u0115');
			break;
		}
		case '\u0117':
		{
			match('\u0117');
			break;
		}
		case '\u0119':
		{
			match('\u0119');
			break;
		}
		case '\u011b':
		{
			match('\u011B');
			break;
		}
		case '\u011d':
		{
			match('\u011D');
			break;
		}
		case '\u011f':
		{
			match('\u011F');
			break;
		}
		case '\u0121':
		{
			match('\u0121');
			break;
		}
		case '\u0123':
		{
			match('\u0123');
			break;
		}
		case '\u0125':
		{
			match('\u0125');
			break;
		}
		case '\u0127':
		{
			match('\u0127');
			break;
		}
		case '\u0129':
		{
			match('\u0129');
			break;
		}
		case '\u012b':
		{
			match('\u012B');
			break;
		}
		case '\u012d':
		{
			match('\u012D');
			break;
		}
		case '\u012f':
		{
			match('\u012F');
			break;
		}
		case '\u0131':
		{
			match('\u0131');
			break;
		}
		case '\u0133':
		{
			match('\u0133');
			break;
		}
		case '\u0135':
		{
			match('\u0135');
			break;
		}
		case '\u0137':
		{
			match('\u0137');
			break;
		}
		case '\u0138':
		{
			match('\u0138');
			break;
		}
		case '\u013a':
		{
			match('\u013A');
			break;
		}
		case '\u013c':
		{
			match('\u013C');
			break;
		}
		case '\u013e':
		{
			match('\u013E');
			break;
		}
		case '\u0140':
		{
			match('\u0140');
			break;
		}
		case '\u0142':
		{
			match('\u0142');
			break;
		}
		case '\u0144':
		{
			match('\u0144');
			break;
		}
		case '\u0146':
		{
			match('\u0146');
			break;
		}
		case '\u0148':
		{
			match('\u0148');
			break;
		}
		case '\u0149':
		{
			match('\u0149');
			break;
		}
		case '\u014b':
		{
			match('\u014B');
			break;
		}
		case '\u014d':
		{
			match('\u014D');
			break;
		}
		case '\u014f':
		{
			match('\u014F');
			break;
		}
		case '\u0151':
		{
			match('\u0151');
			break;
		}
		case '\u0153':
		{
			match('\u0153');
			break;
		}
		case '\u0155':
		{
			match('\u0155');
			break;
		}
		case '\u0157':
		{
			match('\u0157');
			break;
		}
		case '\u0159':
		{
			match('\u0159');
			break;
		}
		case '\u015b':
		{
			match('\u015B');
			break;
		}
		case '\u015d':
		{
			match('\u015D');
			break;
		}
		case '\u015f':
		{
			match('\u015F');
			break;
		}
		case '\u0161':
		{
			match('\u0161');
			break;
		}
		case '\u0163':
		{
			match('\u0163');
			break;
		}
		case '\u0165':
		{
			match('\u0165');
			break;
		}
		case '\u0167':
		{
			match('\u0167');
			break;
		}
		case '\u0169':
		{
			match('\u0169');
			break;
		}
		case '\u016b':
		{
			match('\u016B');
			break;
		}
		case '\u016d':
		{
			match('\u016D');
			break;
		}
		case '\u016f':
		{
			match('\u016F');
			break;
		}
		case '\u0171':
		{
			match('\u0171');
			break;
		}
		case '\u0173':
		{
			match('\u0173');
			break;
		}
		case '\u0175':
		{
			match('\u0175');
			break;
		}
		case '\u0177':
		{
			match('\u0177');
			break;
		}
		case '\u017a':
		{
			match('\u017A');
			break;
		}
		case '\u017c':
		{
			match('\u017C');
			break;
		}
		case '\u017e':  case '\u017f':  case '\u0180':
		{
			matchRange('\u017E','\u0180');
			break;
		}
		case '\u0183':
		{
			match('\u0183');
			break;
		}
		case '\u0185':
		{
			match('\u0185');
			break;
		}
		case '\u0188':
		{
			match('\u0188');
			break;
		}
		case '\u018c':
		{
			match('\u018C');
			break;
		}
		case '\u018d':
		{
			match('\u018D');
			break;
		}
		case '\u0192':
		{
			match('\u0192');
			break;
		}
		case '\u0195':
		{
			match('\u0195');
			break;
		}
		case '\u0199':  case '\u019a':  case '\u019b':
		{
			matchRange('\u0199','\u019B');
			break;
		}
		case '\u019e':
		{
			match('\u019E');
			break;
		}
		case '\u01a1':
		{
			match('\u01A1');
			break;
		}
		case '\u01a3':
		{
			match('\u01A3');
			break;
		}
		case '\u01a5':
		{
			match('\u01A5');
			break;
		}
		case '\u01a8':
		{
			match('\u01A8');
			break;
		}
		case '\u01aa':
		{
			match('\u01AA');
			break;
		}
		case '\u01ab':
		{
			match('\u01AB');
			break;
		}
		case '\u01ad':
		{
			match('\u01AD');
			break;
		}
		case '\u01b0':
		{
			match('\u01B0');
			break;
		}
		case '\u01b4':
		{
			match('\u01B4');
			break;
		}
		case '\u01b6':
		{
			match('\u01B6');
			break;
		}
		case '\u01b9':
		{
			match('\u01B9');
			break;
		}
		case '\u01ba':
		{
			match('\u01BA');
			break;
		}
		case '\u01bd':  case '\u01be':  case '\u01bf':
		{
			matchRange('\u01BD','\u01BF');
			break;
		}
		case '\u01c6':
		{
			match('\u01C6');
			break;
		}
		case '\u01c9':
		{
			match('\u01C9');
			break;
		}
		case '\u01cc':
		{
			match('\u01CC');
			break;
		}
		case '\u01ce':
		{
			match('\u01CE');
			break;
		}
		case '\u01d0':
		{
			match('\u01D0');
			break;
		}
		case '\u01d2':
		{
			match('\u01D2');
			break;
		}
		case '\u01d4':
		{
			match('\u01D4');
			break;
		}
		case '\u01d6':
		{
			match('\u01D6');
			break;
		}
		case '\u01d8':
		{
			match('\u01D8');
			break;
		}
		case '\u01da':
		{
			match('\u01DA');
			break;
		}
		case '\u01dc':
		{
			match('\u01DC');
			break;
		}
		case '\u01dd':
		{
			match('\u01DD');
			break;
		}
		case '\u01df':
		{
			match('\u01DF');
			break;
		}
		case '\u01e1':
		{
			match('\u01E1');
			break;
		}
		case '\u01e3':
		{
			match('\u01E3');
			break;
		}
		case '\u01e5':
		{
			match('\u01E5');
			break;
		}
		case '\u01e7':
		{
			match('\u01E7');
			break;
		}
		case '\u01e9':
		{
			match('\u01E9');
			break;
		}
		case '\u01eb':
		{
			match('\u01EB');
			break;
		}
		case '\u01ed':
		{
			match('\u01ED');
			break;
		}
		case '\u01ef':
		{
			match('\u01EF');
			break;
		}
		case '\u01f0':
		{
			match('\u01F0');
			break;
		}
		case '\u01f3':
		{
			match('\u01F3');
			break;
		}
		case '\u01f5':
		{
			match('\u01F5');
			break;
		}
		case '\u01f9':
		{
			match('\u01F9');
			break;
		}
		case '\u01fb':
		{
			match('\u01FB');
			break;
		}
		case '\u01fd':
		{
			match('\u01FD');
			break;
		}
		case '\u01ff':
		{
			match('\u01FF');
			break;
		}
		case '\u0201':
		{
			match('\u0201');
			break;
		}
		case '\u0203':
		{
			match('\u0203');
			break;
		}
		case '\u0205':
		{
			match('\u0205');
			break;
		}
		case '\u0207':
		{
			match('\u0207');
			break;
		}
		case '\u0209':
		{
			match('\u0209');
			break;
		}
		case '\u020b':
		{
			match('\u020B');
			break;
		}
		case '\u020d':
		{
			match('\u020D');
			break;
		}
		case '\u020f':
		{
			match('\u020F');
			break;
		}
		case '\u0211':
		{
			match('\u0211');
			break;
		}
		case '\u0213':
		{
			match('\u0213');
			break;
		}
		case '\u0215':
		{
			match('\u0215');
			break;
		}
		case '\u0217':
		{
			match('\u0217');
			break;
		}
		case '\u0219':
		{
			match('\u0219');
			break;
		}
		case '\u021b':
		{
			match('\u021B');
			break;
		}
		case '\u021d':
		{
			match('\u021D');
			break;
		}
		case '\u021f':
		{
			match('\u021F');
			break;
		}
		case '\u0221':
		{
			match('\u0221');
			break;
		}
		case '\u0223':
		{
			match('\u0223');
			break;
		}
		case '\u0225':
		{
			match('\u0225');
			break;
		}
		case '\u0227':
		{
			match('\u0227');
			break;
		}
		case '\u0229':
		{
			match('\u0229');
			break;
		}
		case '\u022b':
		{
			match('\u022B');
			break;
		}
		case '\u022d':
		{
			match('\u022D');
			break;
		}
		case '\u022f':
		{
			match('\u022F');
			break;
		}
		case '\u0231':
		{
			match('\u0231');
			break;
		}
		case '\u0233':  case '\u0234':  case '\u0235':  case '\u0236':
		case '\u0237':  case '\u0238':  case '\u0239':
		{
			matchRange('\u0233','\u0239');
			break;
		}
		case '\u023c':
		{
			match('\u023C');
			break;
		}
		case '\u023f':
		{
			match('\u023F');
			break;
		}
		case '\u0240':
		{
			match('\u0240');
			break;
		}
		case '\u0242':
		{
			match('\u0242');
			break;
		}
		case '\u0247':
		{
			match('\u0247');
			break;
		}
		case '\u0249':
		{
			match('\u0249');
			break;
		}
		case '\u024b':
		{
			match('\u024B');
			break;
		}
		case '\u024d':
		{
			match('\u024D');
			break;
		}
		case '\u024f':  case '\u0250':  case '\u0251':  case '\u0252':
		case '\u0253':  case '\u0254':  case '\u0255':  case '\u0256':
		case '\u0257':  case '\u0258':  case '\u0259':  case '\u025a':
		case '\u025b':  case '\u025c':  case '\u025d':  case '\u025e':
		case '\u025f':  case '\u0260':  case '\u0261':  case '\u0262':
		case '\u0263':  case '\u0264':  case '\u0265':  case '\u0266':
		case '\u0267':  case '\u0268':  case '\u0269':  case '\u026a':
		case '\u026b':  case '\u026c':  case '\u026d':  case '\u026e':
		case '\u026f':  case '\u0270':  case '\u0271':  case '\u0272':
		case '\u0273':  case '\u0274':  case '\u0275':  case '\u0276':
		case '\u0277':  case '\u0278':  case '\u0279':  case '\u027a':
		case '\u027b':  case '\u027c':  case '\u027d':  case '\u027e':
		case '\u027f':  case '\u0280':  case '\u0281':  case '\u0282':
		case '\u0283':  case '\u0284':  case '\u0285':  case '\u0286':
		case '\u0287':  case '\u0288':  case '\u0289':  case '\u028a':
		case '\u028b':  case '\u028c':  case '\u028d':  case '\u028e':
		case '\u028f':  case '\u0290':  case '\u0291':  case '\u0292':
		case '\u0293':
		{
			matchRange('\u024F','\u0293');
			break;
		}
		case '\u0295':  case '\u0296':  case '\u0297':  case '\u0298':
		case '\u0299':  case '\u029a':  case '\u029b':  case '\u029c':
		case '\u029d':  case '\u029e':  case '\u029f':  case '\u02a0':
		case '\u02a1':  case '\u02a2':  case '\u02a3':  case '\u02a4':
		case '\u02a5':  case '\u02a6':  case '\u02a7':  case '\u02a8':
		case '\u02a9':  case '\u02aa':  case '\u02ab':  case '\u02ac':
		case '\u02ad':  case '\u02ae':  case '\u02af':
		{
			matchRange('\u0295','\u02AF');
			break;
		}
		case '\u0371':
		{
			match('\u0371');
			break;
		}
		case '\u0373':
		{
			match('\u0373');
			break;
		}
		case '\u0377':
		{
			match('\u0377');
			break;
		}
		case '\u037b':  case '\u037c':  case '\u037d':
		{
			matchRange('\u037B','\u037D');
			break;
		}
		case '\u0390':
		{
			match('\u0390');
			break;
		}
		case '\u03ac':  case '\u03ad':  case '\u03ae':  case '\u03af':
		case '\u03b0':  case '\u03b1':  case '\u03b2':  case '\u03b3':
		case '\u03b4':  case '\u03b5':  case '\u03b6':  case '\u03b7':
		case '\u03b8':  case '\u03b9':  case '\u03ba':  case '\u03bb':
		case '\u03bc':  case '\u03bd':  case '\u03be':  case '\u03bf':
		case '\u03c0':  case '\u03c1':  case '\u03c2':  case '\u03c3':
		case '\u03c4':  case '\u03c5':  case '\u03c6':  case '\u03c7':
		case '\u03c8':  case '\u03c9':  case '\u03ca':  case '\u03cb':
		case '\u03cc':  case '\u03cd':  case '\u03ce':
		{
			matchRange('\u03AC','\u03CE');
			break;
		}
		case '\u03d0':
		{
			match('\u03D0');
			break;
		}
		case '\u03d1':
		{
			match('\u03D1');
			break;
		}
		case '\u03d5':  case '\u03d6':  case '\u03d7':
		{
			matchRange('\u03D5','\u03D7');
			break;
		}
		case '\u03d9':
		{
			match('\u03D9');
			break;
		}
		case '\u03db':
		{
			match('\u03DB');
			break;
		}
		case '\u03dd':
		{
			match('\u03DD');
			break;
		}
		case '\u03df':
		{
			match('\u03DF');
			break;
		}
		case '\u03e1':
		{
			match('\u03E1');
			break;
		}
		case '\u03e3':
		{
			match('\u03E3');
			break;
		}
		case '\u03e5':
		{
			match('\u03E5');
			break;
		}
		case '\u03e7':
		{
			match('\u03E7');
			break;
		}
		case '\u03e9':
		{
			match('\u03E9');
			break;
		}
		case '\u03eb':
		{
			match('\u03EB');
			break;
		}
		case '\u03ed':
		{
			match('\u03ED');
			break;
		}
		case '\u03ef':  case '\u03f0':  case '\u03f1':  case '\u03f2':
		case '\u03f3':
		{
			matchRange('\u03EF','\u03F3');
			break;
		}
		case '\u03f5':
		{
			match('\u03F5');
			break;
		}
		case '\u03f8':
		{
			match('\u03F8');
			break;
		}
		case '\u03fb':
		{
			match('\u03FB');
			break;
		}
		case '\u03fc':
		{
			match('\u03FC');
			break;
		}
		case '\u0430':  case '\u0431':  case '\u0432':  case '\u0433':
		case '\u0434':  case '\u0435':  case '\u0436':  case '\u0437':
		case '\u0438':  case '\u0439':  case '\u043a':  case '\u043b':
		case '\u043c':  case '\u043d':  case '\u043e':  case '\u043f':
		case '\u0440':  case '\u0441':  case '\u0442':  case '\u0443':
		case '\u0444':  case '\u0445':  case '\u0446':  case '\u0447':
		case '\u0448':  case '\u0449':  case '\u044a':  case '\u044b':
		case '\u044c':  case '\u044d':  case '\u044e':  case '\u044f':
		case '\u0450':  case '\u0451':  case '\u0452':  case '\u0453':
		case '\u0454':  case '\u0455':  case '\u0456':  case '\u0457':
		case '\u0458':  case '\u0459':  case '\u045a':  case '\u045b':
		case '\u045c':  case '\u045d':  case '\u045e':  case '\u045f':
		{
			matchRange('\u0430','\u045F');
			break;
		}
		case '\u0461':
		{
			match('\u0461');
			break;
		}
		case '\u0463':
		{
			match('\u0463');
			break;
		}
		case '\u0465':
		{
			match('\u0465');
			break;
		}
		case '\u0467':
		{
			match('\u0467');
			break;
		}
		case '\u0469':
		{
			match('\u0469');
			break;
		}
		case '\u046b':
		{
			match('\u046B');
			break;
		}
		case '\u046d':
		{
			match('\u046D');
			break;
		}
		case '\u046f':
		{
			match('\u046F');
			break;
		}
		case '\u0471':
		{
			match('\u0471');
			break;
		}
		case '\u0473':
		{
			match('\u0473');
			break;
		}
		case '\u0475':
		{
			match('\u0475');
			break;
		}
		case '\u0477':
		{
			match('\u0477');
			break;
		}
		case '\u0479':
		{
			match('\u0479');
			break;
		}
		case '\u047b':
		{
			match('\u047B');
			break;
		}
		case '\u047d':
		{
			match('\u047D');
			break;
		}
		case '\u047f':
		{
			match('\u047F');
			break;
		}
		case '\u0481':
		{
			match('\u0481');
			break;
		}
		case '\u048b':
		{
			match('\u048B');
			break;
		}
		case '\u048d':
		{
			match('\u048D');
			break;
		}
		case '\u048f':
		{
			match('\u048F');
			break;
		}
		case '\u0491':
		{
			match('\u0491');
			break;
		}
		case '\u0493':
		{
			match('\u0493');
			break;
		}
		case '\u0495':
		{
			match('\u0495');
			break;
		}
		case '\u0497':
		{
			match('\u0497');
			break;
		}
		case '\u0499':
		{
			match('\u0499');
			break;
		}
		case '\u049b':
		{
			match('\u049B');
			break;
		}
		case '\u049d':
		{
			match('\u049D');
			break;
		}
		case '\u049f':
		{
			match('\u049F');
			break;
		}
		case '\u04a1':
		{
			match('\u04A1');
			break;
		}
		case '\u04a3':
		{
			match('\u04A3');
			break;
		}
		case '\u04a5':
		{
			match('\u04A5');
			break;
		}
		case '\u04a7':
		{
			match('\u04A7');
			break;
		}
		case '\u04a9':
		{
			match('\u04A9');
			break;
		}
		case '\u04ab':
		{
			match('\u04AB');
			break;
		}
		case '\u04ad':
		{
			match('\u04AD');
			break;
		}
		case '\u04af':
		{
			match('\u04AF');
			break;
		}
		case '\u04b1':
		{
			match('\u04B1');
			break;
		}
		case '\u04b3':
		{
			match('\u04B3');
			break;
		}
		case '\u04b5':
		{
			match('\u04B5');
			break;
		}
		case '\u04b7':
		{
			match('\u04B7');
			break;
		}
		case '\u04b9':
		{
			match('\u04B9');
			break;
		}
		case '\u04bb':
		{
			match('\u04BB');
			break;
		}
		case '\u04bd':
		{
			match('\u04BD');
			break;
		}
		case '\u04bf':
		{
			match('\u04BF');
			break;
		}
		case '\u04c2':
		{
			match('\u04C2');
			break;
		}
		case '\u04c4':
		{
			match('\u04C4');
			break;
		}
		case '\u04c6':
		{
			match('\u04C6');
			break;
		}
		case '\u04c8':
		{
			match('\u04C8');
			break;
		}
		case '\u04ca':
		{
			match('\u04CA');
			break;
		}
		case '\u04cc':
		{
			match('\u04CC');
			break;
		}
		case '\u04ce':
		{
			match('\u04CE');
			break;
		}
		case '\u04cf':
		{
			match('\u04CF');
			break;
		}
		case '\u04d1':
		{
			match('\u04D1');
			break;
		}
		case '\u04d3':
		{
			match('\u04D3');
			break;
		}
		case '\u04d5':
		{
			match('\u04D5');
			break;
		}
		case '\u04d7':
		{
			match('\u04D7');
			break;
		}
		case '\u04d9':
		{
			match('\u04D9');
			break;
		}
		case '\u04db':
		{
			match('\u04DB');
			break;
		}
		case '\u04dd':
		{
			match('\u04DD');
			break;
		}
		case '\u04df':
		{
			match('\u04DF');
			break;
		}
		case '\u04e1':
		{
			match('\u04E1');
			break;
		}
		case '\u04e3':
		{
			match('\u04E3');
			break;
		}
		case '\u04e5':
		{
			match('\u04E5');
			break;
		}
		case '\u04e7':
		{
			match('\u04E7');
			break;
		}
		case '\u04e9':
		{
			match('\u04E9');
			break;
		}
		case '\u04eb':
		{
			match('\u04EB');
			break;
		}
		case '\u04ed':
		{
			match('\u04ED');
			break;
		}
		case '\u04ef':
		{
			match('\u04EF');
			break;
		}
		case '\u04f1':
		{
			match('\u04F1');
			break;
		}
		case '\u04f3':
		{
			match('\u04F3');
			break;
		}
		case '\u04f5':
		{
			match('\u04F5');
			break;
		}
		case '\u04f7':
		{
			match('\u04F7');
			break;
		}
		case '\u04f9':
		{
			match('\u04F9');
			break;
		}
		case '\u04fb':
		{
			match('\u04FB');
			break;
		}
		case '\u04fd':
		{
			match('\u04FD');
			break;
		}
		case '\u04ff':
		{
			match('\u04FF');
			break;
		}
		case '\u0501':
		{
			match('\u0501');
			break;
		}
		case '\u0503':
		{
			match('\u0503');
			break;
		}
		case '\u0505':
		{
			match('\u0505');
			break;
		}
		case '\u0507':
		{
			match('\u0507');
			break;
		}
		case '\u0509':
		{
			match('\u0509');
			break;
		}
		case '\u050b':
		{
			match('\u050B');
			break;
		}
		case '\u050d':
		{
			match('\u050D');
			break;
		}
		case '\u050f':
		{
			match('\u050F');
			break;
		}
		case '\u0511':
		{
			match('\u0511');
			break;
		}
		case '\u0513':
		{
			match('\u0513');
			break;
		}
		case '\u0515':
		{
			match('\u0515');
			break;
		}
		case '\u0517':
		{
			match('\u0517');
			break;
		}
		case '\u0519':
		{
			match('\u0519');
			break;
		}
		case '\u051b':
		{
			match('\u051B');
			break;
		}
		case '\u051d':
		{
			match('\u051D');
			break;
		}
		case '\u051f':
		{
			match('\u051F');
			break;
		}
		case '\u0521':
		{
			match('\u0521');
			break;
		}
		case '\u0523':
		{
			match('\u0523');
			break;
		}
		case '\u0525':
		{
			match('\u0525');
			break;
		}
		case '\u0527':
		{
			match('\u0527');
			break;
		}
		case '\u0561':  case '\u0562':  case '\u0563':  case '\u0564':
		case '\u0565':  case '\u0566':  case '\u0567':  case '\u0568':
		case '\u0569':  case '\u056a':  case '\u056b':  case '\u056c':
		case '\u056d':  case '\u056e':  case '\u056f':  case '\u0570':
		case '\u0571':  case '\u0572':  case '\u0573':  case '\u0574':
		case '\u0575':  case '\u0576':  case '\u0577':  case '\u0578':
		case '\u0579':  case '\u057a':  case '\u057b':  case '\u057c':
		case '\u057d':  case '\u057e':  case '\u057f':  case '\u0580':
		case '\u0581':  case '\u0582':  case '\u0583':  case '\u0584':
		case '\u0585':  case '\u0586':  case '\u0587':
		{
			matchRange('\u0561','\u0587');
			break;
		}
		case '\u1d00':  case '\u1d01':  case '\u1d02':  case '\u1d03':
		case '\u1d04':  case '\u1d05':  case '\u1d06':  case '\u1d07':
		case '\u1d08':  case '\u1d09':  case '\u1d0a':  case '\u1d0b':
		case '\u1d0c':  case '\u1d0d':  case '\u1d0e':  case '\u1d0f':
		case '\u1d10':  case '\u1d11':  case '\u1d12':  case '\u1d13':
		case '\u1d14':  case '\u1d15':  case '\u1d16':  case '\u1d17':
		case '\u1d18':  case '\u1d19':  case '\u1d1a':  case '\u1d1b':
		case '\u1d1c':  case '\u1d1d':  case '\u1d1e':  case '\u1d1f':
		case '\u1d20':  case '\u1d21':  case '\u1d22':  case '\u1d23':
		case '\u1d24':  case '\u1d25':  case '\u1d26':  case '\u1d27':
		case '\u1d28':  case '\u1d29':  case '\u1d2a':  case '\u1d2b':
		{
			matchRange('\u1D00','\u1D2B');
			break;
		}
		case '\u1d6b':  case '\u1d6c':  case '\u1d6d':  case '\u1d6e':
		case '\u1d6f':  case '\u1d70':  case '\u1d71':  case '\u1d72':
		case '\u1d73':  case '\u1d74':  case '\u1d75':  case '\u1d76':
		case '\u1d77':
		{
			matchRange('\u1D6B','\u1D77');
			break;
		}
		case '\u1d79':  case '\u1d7a':  case '\u1d7b':  case '\u1d7c':
		case '\u1d7d':  case '\u1d7e':  case '\u1d7f':  case '\u1d80':
		case '\u1d81':  case '\u1d82':  case '\u1d83':  case '\u1d84':
		case '\u1d85':  case '\u1d86':  case '\u1d87':  case '\u1d88':
		case '\u1d89':  case '\u1d8a':  case '\u1d8b':  case '\u1d8c':
		case '\u1d8d':  case '\u1d8e':  case '\u1d8f':  case '\u1d90':
		case '\u1d91':  case '\u1d92':  case '\u1d93':  case '\u1d94':
		case '\u1d95':  case '\u1d96':  case '\u1d97':  case '\u1d98':
		case '\u1d99':  case '\u1d9a':
		{
			matchRange('\u1D79','\u1D9A');
			break;
		}
		case '\u1e01':
		{
			match('\u1E01');
			break;
		}
		case '\u1e03':
		{
			match('\u1E03');
			break;
		}
		case '\u1e05':
		{
			match('\u1E05');
			break;
		}
		case '\u1e07':
		{
			match('\u1E07');
			break;
		}
		case '\u1e09':
		{
			match('\u1E09');
			break;
		}
		case '\u1e0b':
		{
			match('\u1E0B');
			break;
		}
		case '\u1e0d':
		{
			match('\u1E0D');
			break;
		}
		case '\u1e0f':
		{
			match('\u1E0F');
			break;
		}
		case '\u1e11':
		{
			match('\u1E11');
			break;
		}
		case '\u1e13':
		{
			match('\u1E13');
			break;
		}
		case '\u1e15':
		{
			match('\u1E15');
			break;
		}
		case '\u1e17':
		{
			match('\u1E17');
			break;
		}
		case '\u1e19':
		{
			match('\u1E19');
			break;
		}
		case '\u1e1b':
		{
			match('\u1E1B');
			break;
		}
		case '\u1e1d':
		{
			match('\u1E1D');
			break;
		}
		case '\u1e1f':
		{
			match('\u1E1F');
			break;
		}
		case '\u1e21':
		{
			match('\u1E21');
			break;
		}
		case '\u1e23':
		{
			match('\u1E23');
			break;
		}
		case '\u1e25':
		{
			match('\u1E25');
			break;
		}
		case '\u1e27':
		{
			match('\u1E27');
			break;
		}
		case '\u1e29':
		{
			match('\u1E29');
			break;
		}
		case '\u1e2b':
		{
			match('\u1E2B');
			break;
		}
		case '\u1e2d':
		{
			match('\u1E2D');
			break;
		}
		case '\u1e2f':
		{
			match('\u1E2F');
			break;
		}
		case '\u1e31':
		{
			match('\u1E31');
			break;
		}
		case '\u1e33':
		{
			match('\u1E33');
			break;
		}
		case '\u1e35':
		{
			match('\u1E35');
			break;
		}
		case '\u1e37':
		{
			match('\u1E37');
			break;
		}
		case '\u1e39':
		{
			match('\u1E39');
			break;
		}
		case '\u1e3b':
		{
			match('\u1E3B');
			break;
		}
		case '\u1e3d':
		{
			match('\u1E3D');
			break;
		}
		case '\u1e3f':
		{
			match('\u1E3F');
			break;
		}
		case '\u1e41':
		{
			match('\u1E41');
			break;
		}
		case '\u1e43':
		{
			match('\u1E43');
			break;
		}
		case '\u1e45':
		{
			match('\u1E45');
			break;
		}
		case '\u1e47':
		{
			match('\u1E47');
			break;
		}
		case '\u1e49':
		{
			match('\u1E49');
			break;
		}
		case '\u1e4b':
		{
			match('\u1E4B');
			break;
		}
		case '\u1e4d':
		{
			match('\u1E4D');
			break;
		}
		case '\u1e4f':
		{
			match('\u1E4F');
			break;
		}
		case '\u1e51':
		{
			match('\u1E51');
			break;
		}
		case '\u1e53':
		{
			match('\u1E53');
			break;
		}
		case '\u1e55':
		{
			match('\u1E55');
			break;
		}
		case '\u1e57':
		{
			match('\u1E57');
			break;
		}
		case '\u1e59':
		{
			match('\u1E59');
			break;
		}
		case '\u1e5b':
		{
			match('\u1E5B');
			break;
		}
		case '\u1e5d':
		{
			match('\u1E5D');
			break;
		}
		case '\u1e5f':
		{
			match('\u1E5F');
			break;
		}
		case '\u1e61':
		{
			match('\u1E61');
			break;
		}
		case '\u1e63':
		{
			match('\u1E63');
			break;
		}
		case '\u1e65':
		{
			match('\u1E65');
			break;
		}
		case '\u1e67':
		{
			match('\u1E67');
			break;
		}
		case '\u1e69':
		{
			match('\u1E69');
			break;
		}
		case '\u1e6b':
		{
			match('\u1E6B');
			break;
		}
		case '\u1e6d':
		{
			match('\u1E6D');
			break;
		}
		case '\u1e6f':
		{
			match('\u1E6F');
			break;
		}
		case '\u1e71':
		{
			match('\u1E71');
			break;
		}
		case '\u1e73':
		{
			match('\u1E73');
			break;
		}
		case '\u1e75':
		{
			match('\u1E75');
			break;
		}
		case '\u1e77':
		{
			match('\u1E77');
			break;
		}
		case '\u1e79':
		{
			match('\u1E79');
			break;
		}
		case '\u1e7b':
		{
			match('\u1E7B');
			break;
		}
		case '\u1e7d':
		{
			match('\u1E7D');
			break;
		}
		case '\u1e7f':
		{
			match('\u1E7F');
			break;
		}
		case '\u1e81':
		{
			match('\u1E81');
			break;
		}
		case '\u1e83':
		{
			match('\u1E83');
			break;
		}
		case '\u1e85':
		{
			match('\u1E85');
			break;
		}
		case '\u1e87':
		{
			match('\u1E87');
			break;
		}
		case '\u1e89':
		{
			match('\u1E89');
			break;
		}
		case '\u1e8b':
		{
			match('\u1E8B');
			break;
		}
		case '\u1e8d':
		{
			match('\u1E8D');
			break;
		}
		case '\u1e8f':
		{
			match('\u1E8F');
			break;
		}
		case '\u1e91':
		{
			match('\u1E91');
			break;
		}
		case '\u1e93':
		{
			match('\u1E93');
			break;
		}
		case '\u1e95':  case '\u1e96':  case '\u1e97':  case '\u1e98':
		case '\u1e99':  case '\u1e9a':  case '\u1e9b':  case '\u1e9c':
		case '\u1e9d':
		{
			matchRange('\u1E95','\u1E9D');
			break;
		}
		case '\u1e9f':
		{
			match('\u1E9F');
			break;
		}
		case '\u1ea1':
		{
			match('\u1EA1');
			break;
		}
		case '\u1ea3':
		{
			match('\u1EA3');
			break;
		}
		case '\u1ea5':
		{
			match('\u1EA5');
			break;
		}
		case '\u1ea7':
		{
			match('\u1EA7');
			break;
		}
		case '\u1ea9':
		{
			match('\u1EA9');
			break;
		}
		case '\u1eab':
		{
			match('\u1EAB');
			break;
		}
		case '\u1ead':
		{
			match('\u1EAD');
			break;
		}
		case '\u1eaf':
		{
			match('\u1EAF');
			break;
		}
		case '\u1eb1':
		{
			match('\u1EB1');
			break;
		}
		case '\u1eb3':
		{
			match('\u1EB3');
			break;
		}
		case '\u1eb5':
		{
			match('\u1EB5');
			break;
		}
		case '\u1eb7':
		{
			match('\u1EB7');
			break;
		}
		case '\u1eb9':
		{
			match('\u1EB9');
			break;
		}
		case '\u1ebb':
		{
			match('\u1EBB');
			break;
		}
		case '\u1ebd':
		{
			match('\u1EBD');
			break;
		}
		case '\u1ebf':
		{
			match('\u1EBF');
			break;
		}
		case '\u1ec1':
		{
			match('\u1EC1');
			break;
		}
		case '\u1ec3':
		{
			match('\u1EC3');
			break;
		}
		case '\u1ec5':
		{
			match('\u1EC5');
			break;
		}
		case '\u1ec7':
		{
			match('\u1EC7');
			break;
		}
		case '\u1ec9':
		{
			match('\u1EC9');
			break;
		}
		case '\u1ecb':
		{
			match('\u1ECB');
			break;
		}
		case '\u1ecd':
		{
			match('\u1ECD');
			break;
		}
		case '\u1ecf':
		{
			match('\u1ECF');
			break;
		}
		case '\u1ed1':
		{
			match('\u1ED1');
			break;
		}
		case '\u1ed3':
		{
			match('\u1ED3');
			break;
		}
		case '\u1ed5':
		{
			match('\u1ED5');
			break;
		}
		case '\u1ed7':
		{
			match('\u1ED7');
			break;
		}
		case '\u1ed9':
		{
			match('\u1ED9');
			break;
		}
		case '\u1edb':
		{
			match('\u1EDB');
			break;
		}
		case '\u1edd':
		{
			match('\u1EDD');
			break;
		}
		case '\u1edf':
		{
			match('\u1EDF');
			break;
		}
		case '\u1ee1':
		{
			match('\u1EE1');
			break;
		}
		case '\u1ee3':
		{
			match('\u1EE3');
			break;
		}
		case '\u1ee5':
		{
			match('\u1EE5');
			break;
		}
		case '\u1ee7':
		{
			match('\u1EE7');
			break;
		}
		case '\u1ee9':
		{
			match('\u1EE9');
			break;
		}
		case '\u1eeb':
		{
			match('\u1EEB');
			break;
		}
		case '\u1eed':
		{
			match('\u1EED');
			break;
		}
		case '\u1eef':
		{
			match('\u1EEF');
			break;
		}
		case '\u1ef1':
		{
			match('\u1EF1');
			break;
		}
		case '\u1ef3':
		{
			match('\u1EF3');
			break;
		}
		case '\u1ef5':
		{
			match('\u1EF5');
			break;
		}
		case '\u1ef7':
		{
			match('\u1EF7');
			break;
		}
		case '\u1ef9':
		{
			match('\u1EF9');
			break;
		}
		case '\u1efb':
		{
			match('\u1EFB');
			break;
		}
		case '\u1efd':
		{
			match('\u1EFD');
			break;
		}
		case '\u1eff':  case '\u1f00':  case '\u1f01':  case '\u1f02':
		case '\u1f03':  case '\u1f04':  case '\u1f05':  case '\u1f06':
		case '\u1f07':
		{
			matchRange('\u1EFF','\u1F07');
			break;
		}
		case '\u1f10':  case '\u1f11':  case '\u1f12':  case '\u1f13':
		case '\u1f14':  case '\u1f15':
		{
			matchRange('\u1F10','\u1F15');
			break;
		}
		case '\u1f20':  case '\u1f21':  case '\u1f22':  case '\u1f23':
		case '\u1f24':  case '\u1f25':  case '\u1f26':  case '\u1f27':
		{
			matchRange('\u1F20','\u1F27');
			break;
		}
		case '\u1f30':  case '\u1f31':  case '\u1f32':  case '\u1f33':
		case '\u1f34':  case '\u1f35':  case '\u1f36':  case '\u1f37':
		{
			matchRange('\u1F30','\u1F37');
			break;
		}
		case '\u1f40':  case '\u1f41':  case '\u1f42':  case '\u1f43':
		case '\u1f44':  case '\u1f45':
		{
			matchRange('\u1F40','\u1F45');
			break;
		}
		case '\u1f50':  case '\u1f51':  case '\u1f52':  case '\u1f53':
		case '\u1f54':  case '\u1f55':  case '\u1f56':  case '\u1f57':
		{
			matchRange('\u1F50','\u1F57');
			break;
		}
		case '\u1f60':  case '\u1f61':  case '\u1f62':  case '\u1f63':
		case '\u1f64':  case '\u1f65':  case '\u1f66':  case '\u1f67':
		{
			matchRange('\u1F60','\u1F67');
			break;
		}
		case '\u1f70':  case '\u1f71':  case '\u1f72':  case '\u1f73':
		case '\u1f74':  case '\u1f75':  case '\u1f76':  case '\u1f77':
		case '\u1f78':  case '\u1f79':  case '\u1f7a':  case '\u1f7b':
		case '\u1f7c':  case '\u1f7d':
		{
			matchRange('\u1F70','\u1F7D');
			break;
		}
		case '\u1f80':  case '\u1f81':  case '\u1f82':  case '\u1f83':
		case '\u1f84':  case '\u1f85':  case '\u1f86':  case '\u1f87':
		{
			matchRange('\u1F80','\u1F87');
			break;
		}
		case '\u1f90':  case '\u1f91':  case '\u1f92':  case '\u1f93':
		case '\u1f94':  case '\u1f95':  case '\u1f96':  case '\u1f97':
		{
			matchRange('\u1F90','\u1F97');
			break;
		}
		case '\u1fa0':  case '\u1fa1':  case '\u1fa2':  case '\u1fa3':
		case '\u1fa4':  case '\u1fa5':  case '\u1fa6':  case '\u1fa7':
		{
			matchRange('\u1FA0','\u1FA7');
			break;
		}
		case '\u1fb0':  case '\u1fb1':  case '\u1fb2':  case '\u1fb3':
		case '\u1fb4':
		{
			matchRange('\u1FB0','\u1FB4');
			break;
		}
		case '\u1fb6':
		{
			match('\u1FB6');
			break;
		}
		case '\u1fb7':
		{
			match('\u1FB7');
			break;
		}
		case '\u1fbe':
		{
			match('\u1FBE');
			break;
		}
		case '\u1fc2':  case '\u1fc3':  case '\u1fc4':
		{
			matchRange('\u1FC2','\u1FC4');
			break;
		}
		case '\u1fc6':
		{
			match('\u1FC6');
			break;
		}
		case '\u1fc7':
		{
			match('\u1FC7');
			break;
		}
		case '\u1fd0':  case '\u1fd1':  case '\u1fd2':  case '\u1fd3':
		{
			matchRange('\u1FD0','\u1FD3');
			break;
		}
		case '\u1fd6':
		{
			match('\u1FD6');
			break;
		}
		case '\u1fd7':
		{
			match('\u1FD7');
			break;
		}
		case '\u1fe0':  case '\u1fe1':  case '\u1fe2':  case '\u1fe3':
		case '\u1fe4':  case '\u1fe5':  case '\u1fe6':  case '\u1fe7':
		{
			matchRange('\u1FE0','\u1FE7');
			break;
		}
		case '\u1ff2':  case '\u1ff3':  case '\u1ff4':
		{
			matchRange('\u1FF2','\u1FF4');
			break;
		}
		case '\u1ff6':
		{
			match('\u1FF6');
			break;
		}
		case '\u1ff7':
		{
			match('\u1FF7');
			break;
		}
		case '\u210a':
		{
			match('\u210A');
			break;
		}
		case '\u210e':
		{
			match('\u210E');
			break;
		}
		case '\u210f':
		{
			match('\u210F');
			break;
		}
		case '\u2113':
		{
			match('\u2113');
			break;
		}
		case '\u212f':
		{
			match('\u212F');
			break;
		}
		case '\u2134':
		{
			match('\u2134');
			break;
		}
		case '\u2139':
		{
			match('\u2139');
			break;
		}
		case '\u213c':
		{
			match('\u213C');
			break;
		}
		case '\u213d':
		{
			match('\u213D');
			break;
		}
		case '\u2146':  case '\u2147':  case '\u2148':  case '\u2149':
		{
			matchRange('\u2146','\u2149');
			break;
		}
		case '\u214e':
		{
			match('\u214E');
			break;
		}
		case '\u2184':
		{
			match('\u2184');
			break;
		}
		case '\u2c30':  case '\u2c31':  case '\u2c32':  case '\u2c33':
		case '\u2c34':  case '\u2c35':  case '\u2c36':  case '\u2c37':
		case '\u2c38':  case '\u2c39':  case '\u2c3a':  case '\u2c3b':
		case '\u2c3c':  case '\u2c3d':  case '\u2c3e':  case '\u2c3f':
		case '\u2c40':  case '\u2c41':  case '\u2c42':  case '\u2c43':
		case '\u2c44':  case '\u2c45':  case '\u2c46':  case '\u2c47':
		case '\u2c48':  case '\u2c49':  case '\u2c4a':  case '\u2c4b':
		case '\u2c4c':  case '\u2c4d':  case '\u2c4e':  case '\u2c4f':
		case '\u2c50':  case '\u2c51':  case '\u2c52':  case '\u2c53':
		case '\u2c54':  case '\u2c55':  case '\u2c56':  case '\u2c57':
		case '\u2c58':  case '\u2c59':  case '\u2c5a':  case '\u2c5b':
		case '\u2c5c':  case '\u2c5d':  case '\u2c5e':
		{
			matchRange('\u2C30','\u2C5E');
			break;
		}
		case '\u2c61':
		{
			match('\u2C61');
			break;
		}
		case '\u2c65':
		{
			match('\u2C65');
			break;
		}
		case '\u2c66':
		{
			match('\u2C66');
			break;
		}
		case '\u2c68':
		{
			match('\u2C68');
			break;
		}
		case '\u2c6a':
		{
			match('\u2C6A');
			break;
		}
		case '\u2c6c':
		{
			match('\u2C6C');
			break;
		}
		case '\u2c71':
		{
			match('\u2C71');
			break;
		}
		case '\u2c73':
		{
			match('\u2C73');
			break;
		}
		case '\u2c74':
		{
			match('\u2C74');
			break;
		}
		case '\u2c76':  case '\u2c77':  case '\u2c78':  case '\u2c79':
		case '\u2c7a':  case '\u2c7b':
		{
			matchRange('\u2C76','\u2C7B');
			break;
		}
		case '\u2c81':
		{
			match('\u2C81');
			break;
		}
		case '\u2c83':
		{
			match('\u2C83');
			break;
		}
		case '\u2c85':
		{
			match('\u2C85');
			break;
		}
		case '\u2c87':
		{
			match('\u2C87');
			break;
		}
		case '\u2c89':
		{
			match('\u2C89');
			break;
		}
		case '\u2c8b':
		{
			match('\u2C8B');
			break;
		}
		case '\u2c8d':
		{
			match('\u2C8D');
			break;
		}
		case '\u2c8f':
		{
			match('\u2C8F');
			break;
		}
		case '\u2c91':
		{
			match('\u2C91');
			break;
		}
		case '\u2c93':
		{
			match('\u2C93');
			break;
		}
		case '\u2c95':
		{
			match('\u2C95');
			break;
		}
		case '\u2c97':
		{
			match('\u2C97');
			break;
		}
		case '\u2c99':
		{
			match('\u2C99');
			break;
		}
		case '\u2c9b':
		{
			match('\u2C9B');
			break;
		}
		case '\u2c9d':
		{
			match('\u2C9D');
			break;
		}
		case '\u2c9f':
		{
			match('\u2C9F');
			break;
		}
		case '\u2ca1':
		{
			match('\u2CA1');
			break;
		}
		case '\u2ca3':
		{
			match('\u2CA3');
			break;
		}
		case '\u2ca5':
		{
			match('\u2CA5');
			break;
		}
		case '\u2ca7':
		{
			match('\u2CA7');
			break;
		}
		case '\u2ca9':
		{
			match('\u2CA9');
			break;
		}
		case '\u2cab':
		{
			match('\u2CAB');
			break;
		}
		case '\u2cad':
		{
			match('\u2CAD');
			break;
		}
		case '\u2caf':
		{
			match('\u2CAF');
			break;
		}
		case '\u2cb1':
		{
			match('\u2CB1');
			break;
		}
		case '\u2cb3':
		{
			match('\u2CB3');
			break;
		}
		case '\u2cb5':
		{
			match('\u2CB5');
			break;
		}
		case '\u2cb7':
		{
			match('\u2CB7');
			break;
		}
		case '\u2cb9':
		{
			match('\u2CB9');
			break;
		}
		case '\u2cbb':
		{
			match('\u2CBB');
			break;
		}
		case '\u2cbd':
		{
			match('\u2CBD');
			break;
		}
		case '\u2cbf':
		{
			match('\u2CBF');
			break;
		}
		case '\u2cc1':
		{
			match('\u2CC1');
			break;
		}
		case '\u2cc3':
		{
			match('\u2CC3');
			break;
		}
		case '\u2cc5':
		{
			match('\u2CC5');
			break;
		}
		case '\u2cc7':
		{
			match('\u2CC7');
			break;
		}
		case '\u2cc9':
		{
			match('\u2CC9');
			break;
		}
		case '\u2ccb':
		{
			match('\u2CCB');
			break;
		}
		case '\u2ccd':
		{
			match('\u2CCD');
			break;
		}
		case '\u2ccf':
		{
			match('\u2CCF');
			break;
		}
		case '\u2cd1':
		{
			match('\u2CD1');
			break;
		}
		case '\u2cd3':
		{
			match('\u2CD3');
			break;
		}
		case '\u2cd5':
		{
			match('\u2CD5');
			break;
		}
		case '\u2cd7':
		{
			match('\u2CD7');
			break;
		}
		case '\u2cd9':
		{
			match('\u2CD9');
			break;
		}
		case '\u2cdb':
		{
			match('\u2CDB');
			break;
		}
		case '\u2cdd':
		{
			match('\u2CDD');
			break;
		}
		case '\u2cdf':
		{
			match('\u2CDF');
			break;
		}
		case '\u2ce1':
		{
			match('\u2CE1');
			break;
		}
		case '\u2ce3':
		{
			match('\u2CE3');
			break;
		}
		case '\u2ce4':
		{
			match('\u2CE4');
			break;
		}
		case '\u2cec':
		{
			match('\u2CEC');
			break;
		}
		case '\u2cee':
		{
			match('\u2CEE');
			break;
		}
		case '\u2cf3':
		{
			match('\u2CF3');
			break;
		}
		case '\u2d00':  case '\u2d01':  case '\u2d02':  case '\u2d03':
		case '\u2d04':  case '\u2d05':  case '\u2d06':  case '\u2d07':
		case '\u2d08':  case '\u2d09':  case '\u2d0a':  case '\u2d0b':
		case '\u2d0c':  case '\u2d0d':  case '\u2d0e':  case '\u2d0f':
		case '\u2d10':  case '\u2d11':  case '\u2d12':  case '\u2d13':
		case '\u2d14':  case '\u2d15':  case '\u2d16':  case '\u2d17':
		case '\u2d18':  case '\u2d19':  case '\u2d1a':  case '\u2d1b':
		case '\u2d1c':  case '\u2d1d':  case '\u2d1e':  case '\u2d1f':
		case '\u2d20':  case '\u2d21':  case '\u2d22':  case '\u2d23':
		case '\u2d24':  case '\u2d25':
		{
			matchRange('\u2D00','\u2D25');
			break;
		}
		case '\u2d27':
		{
			match('\u2D27');
			break;
		}
		case '\u2d2d':
		{
			match('\u2D2D');
			break;
		}
		case '\ua641':
		{
			match('\uA641');
			break;
		}
		case '\ua643':
		{
			match('\uA643');
			break;
		}
		case '\ua645':
		{
			match('\uA645');
			break;
		}
		case '\ua647':
		{
			match('\uA647');
			break;
		}
		case '\ua649':
		{
			match('\uA649');
			break;
		}
		case '\ua64b':
		{
			match('\uA64B');
			break;
		}
		case '\ua64d':
		{
			match('\uA64D');
			break;
		}
		case '\ua64f':
		{
			match('\uA64F');
			break;
		}
		case '\ua651':
		{
			match('\uA651');
			break;
		}
		case '\ua653':
		{
			match('\uA653');
			break;
		}
		case '\ua655':
		{
			match('\uA655');
			break;
		}
		case '\ua657':
		{
			match('\uA657');
			break;
		}
		case '\ua659':
		{
			match('\uA659');
			break;
		}
		case '\ua65b':
		{
			match('\uA65B');
			break;
		}
		case '\ua65d':
		{
			match('\uA65D');
			break;
		}
		case '\ua65f':
		{
			match('\uA65F');
			break;
		}
		case '\ua661':
		{
			match('\uA661');
			break;
		}
		case '\ua663':
		{
			match('\uA663');
			break;
		}
		case '\ua665':
		{
			match('\uA665');
			break;
		}
		case '\ua667':
		{
			match('\uA667');
			break;
		}
		case '\ua669':
		{
			match('\uA669');
			break;
		}
		case '\ua66b':
		{
			match('\uA66B');
			break;
		}
		case '\ua66d':
		{
			match('\uA66D');
			break;
		}
		case '\ua681':
		{
			match('\uA681');
			break;
		}
		case '\ua683':
		{
			match('\uA683');
			break;
		}
		case '\ua685':
		{
			match('\uA685');
			break;
		}
		case '\ua687':
		{
			match('\uA687');
			break;
		}
		case '\ua689':
		{
			match('\uA689');
			break;
		}
		case '\ua68b':
		{
			match('\uA68B');
			break;
		}
		case '\ua68d':
		{
			match('\uA68D');
			break;
		}
		case '\ua68f':
		{
			match('\uA68F');
			break;
		}
		case '\ua691':
		{
			match('\uA691');
			break;
		}
		case '\ua693':
		{
			match('\uA693');
			break;
		}
		case '\ua695':
		{
			match('\uA695');
			break;
		}
		case '\ua697':
		{
			match('\uA697');
			break;
		}
		case '\ua723':
		{
			match('\uA723');
			break;
		}
		case '\ua725':
		{
			match('\uA725');
			break;
		}
		case '\ua727':
		{
			match('\uA727');
			break;
		}
		case '\ua729':
		{
			match('\uA729');
			break;
		}
		case '\ua72b':
		{
			match('\uA72B');
			break;
		}
		case '\ua72d':
		{
			match('\uA72D');
			break;
		}
		case '\ua72f':  case '\ua730':  case '\ua731':
		{
			matchRange('\uA72F','\uA731');
			break;
		}
		case '\ua733':
		{
			match('\uA733');
			break;
		}
		case '\ua735':
		{
			match('\uA735');
			break;
		}
		case '\ua737':
		{
			match('\uA737');
			break;
		}
		case '\ua739':
		{
			match('\uA739');
			break;
		}
		case '\ua73b':
		{
			match('\uA73B');
			break;
		}
		case '\ua73d':
		{
			match('\uA73D');
			break;
		}
		case '\ua73f':
		{
			match('\uA73F');
			break;
		}
		case '\ua741':
		{
			match('\uA741');
			break;
		}
		case '\ua743':
		{
			match('\uA743');
			break;
		}
		case '\ua745':
		{
			match('\uA745');
			break;
		}
		case '\ua747':
		{
			match('\uA747');
			break;
		}
		case '\ua749':
		{
			match('\uA749');
			break;
		}
		case '\ua74b':
		{
			match('\uA74B');
			break;
		}
		case '\ua74d':
		{
			match('\uA74D');
			break;
		}
		case '\ua74f':
		{
			match('\uA74F');
			break;
		}
		case '\ua751':
		{
			match('\uA751');
			break;
		}
		case '\ua753':
		{
			match('\uA753');
			break;
		}
		case '\ua755':
		{
			match('\uA755');
			break;
		}
		case '\ua757':
		{
			match('\uA757');
			break;
		}
		case '\ua759':
		{
			match('\uA759');
			break;
		}
		case '\ua75b':
		{
			match('\uA75B');
			break;
		}
		case '\ua75d':
		{
			match('\uA75D');
			break;
		}
		case '\ua75f':
		{
			match('\uA75F');
			break;
		}
		case '\ua761':
		{
			match('\uA761');
			break;
		}
		case '\ua763':
		{
			match('\uA763');
			break;
		}
		case '\ua765':
		{
			match('\uA765');
			break;
		}
		case '\ua767':
		{
			match('\uA767');
			break;
		}
		case '\ua769':
		{
			match('\uA769');
			break;
		}
		case '\ua76b':
		{
			match('\uA76B');
			break;
		}
		case '\ua76d':
		{
			match('\uA76D');
			break;
		}
		case '\ua76f':
		{
			match('\uA76F');
			break;
		}
		case '\ua771':  case '\ua772':  case '\ua773':  case '\ua774':
		case '\ua775':  case '\ua776':  case '\ua777':  case '\ua778':
		{
			matchRange('\uA771','\uA778');
			break;
		}
		case '\ua77a':
		{
			match('\uA77A');
			break;
		}
		case '\ua77c':
		{
			match('\uA77C');
			break;
		}
		case '\ua77f':
		{
			match('\uA77F');
			break;
		}
		case '\ua781':
		{
			match('\uA781');
			break;
		}
		case '\ua783':
		{
			match('\uA783');
			break;
		}
		case '\ua785':
		{
			match('\uA785');
			break;
		}
		case '\ua787':
		{
			match('\uA787');
			break;
		}
		case '\ua78c':
		{
			match('\uA78C');
			break;
		}
		case '\ua78e':
		{
			match('\uA78E');
			break;
		}
		case '\ua791':
		{
			match('\uA791');
			break;
		}
		case '\ua793':
		{
			match('\uA793');
			break;
		}
		case '\ua7a1':
		{
			match('\uA7A1');
			break;
		}
		case '\ua7a3':
		{
			match('\uA7A3');
			break;
		}
		case '\ua7a5':
		{
			match('\uA7A5');
			break;
		}
		case '\ua7a7':
		{
			match('\uA7A7');
			break;
		}
		case '\ua7a9':
		{
			match('\uA7A9');
			break;
		}
		case '\ua7fa':
		{
			match('\uA7FA');
			break;
		}
		case '\ufb00':  case '\ufb01':  case '\ufb02':  case '\ufb03':
		case '\ufb04':  case '\ufb05':  case '\ufb06':
		{
			matchRange('\uFB00','\uFB06');
			break;
		}
		case '\ufb13':  case '\ufb14':  case '\ufb15':  case '\ufb16':
		case '\ufb17':
		{
			matchRange('\uFB13','\uFB17');
			break;
		}
		case '\uff41':  case '\uff42':  case '\uff43':  case '\uff44':
		case '\uff45':  case '\uff46':  case '\uff47':  case '\uff48':
		case '\uff49':  case '\uff4a':  case '\uff4b':  case '\uff4c':
		case '\uff4d':  case '\uff4e':  case '\uff4f':  case '\uff50':
		case '\uff51':  case '\uff52':  case '\uff53':  case '\uff54':
		case '\uff55':  case '\uff56':  case '\uff57':  case '\uff58':
		case '\uff59':  case '\uff5a':
		{
			matchRange('\uFF41','\uFF5A');
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mNUMBERLITERAL(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = NUMBERLITERAL;
		int _saveIndex;
		
		if ((LA(1)=='0') && (LA(2)=='X'||LA(2)=='x')) {
			mHEX(false);
		}
		else if (((LA(1) >= '0' && LA(1) <= '9')) && (true)) {
			{
			int _cnt5934=0;
			_loop5934:
			do {
				if (((LA(1) >= '0' && LA(1) <= '9'))) {
					mDIGIT(false);
				}
				else {
					if ( _cnt5934>=1 ) { break _loop5934; } else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());}
				}
				
				_cnt5934++;
			} while (true);
			}
			{
			switch ( LA(1)) {
			case '.':
			{
				match('.');
				{
				_loop5937:
				do {
					if (((LA(1) >= '0' && LA(1) <= '9'))) {
						mDIGIT(false);
					}
					else {
						break _loop5937;
					}
					
				} while (true);
				}
				{
				if ((LA(1)=='E'||LA(1)=='e')) {
					mEXP(false);
				}
				else {
				}
				
				}
				{
				if ((_tokenSet_14.member(LA(1)))) {
					mFLOATTYPE(false);
				}
				else {
				}
				
				}
				break;
			}
			case 'D':  case 'E':  case 'F':  case 'd':
			case 'e':  case 'f':
			{
				mEXPFLOAT(false);
				break;
			}
			case 'L':  case 'l':
			{
				{
				switch ( LA(1)) {
				case 'L':
				{
					match('L');
					break;
				}
				case 'l':
				{
					match('l');
					break;
				}
				default:
				{
					throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
				}
				}
				}
				break;
			}
			default:
				{
				}
			}
			}
		}
		else if ((LA(1)=='.')) {
			match('.');
			{
			int _cnt5942=0;
			_loop5942:
			do {
				if (((LA(1) >= '0' && LA(1) <= '9'))) {
					mDIGIT(false);
				}
				else {
					if ( _cnt5942>=1 ) { break _loop5942; } else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());}
				}
				
				_cnt5942++;
			} while (true);
			}
			{
			if ((LA(1)=='E'||LA(1)=='e')) {
				mEXP(false);
			}
			else {
			}
			
			}
			{
			if ((_tokenSet_14.member(LA(1)))) {
				mFLOATTYPE(false);
			}
			else {
			}
			
			}
		}
		else {
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mEXP(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = EXP;
		int _saveIndex;
		
		{
		switch ( LA(1)) {
		case 'E':
		{
			match('E');
			break;
		}
		case 'e':
		{
			match('e');
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		}
		{
		switch ( LA(1)) {
		case '+':
		{
			match('+');
			break;
		}
		case '-':
		{
			match('-');
			break;
		}
		case '0':  case '1':  case '2':  case '3':
		case '4':  case '5':  case '6':  case '7':
		case '8':  case '9':
		{
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		}
		{
		int _cnt5953=0;
		_loop5953:
		do {
			if (((LA(1) >= '0' && LA(1) <= '9'))) {
				mDIGIT(false);
			}
			else {
				if ( _cnt5953>=1 ) { break _loop5953; } else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());}
			}
			
			_cnt5953++;
		} while (true);
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mFLOATTYPE(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = FLOATTYPE;
		int _saveIndex;
		
		switch ( LA(1)) {
		case 'F':
		{
			match('F');
			break;
		}
		case 'f':
		{
			match('f');
			break;
		}
		case 'D':
		{
			match('D');
			break;
		}
		case 'd':
		{
			match('d');
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mEXPFLOAT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = EXPFLOAT;
		int _saveIndex;
		
		switch ( LA(1)) {
		case 'E':  case 'e':
		{
			mEXP(false);
			{
			{
			if ((_tokenSet_14.member(LA(1)))) {
				mFLOATTYPE(false);
			}
			else {
			}
			
			}
			}
			break;
		}
		case 'D':  case 'F':  case 'd':  case 'f':
		{
			mFLOATTYPE(false);
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mHEX(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = HEX;
		int _saveIndex;
		
		{
		match('0');
		}
		{
		switch ( LA(1)) {
		case 'x':
		{
			match('x');
			break;
		}
		case 'X':
		{
			match('X');
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		}
		{
		int _cnt5965=0;
		_loop5965:
		do {
			if ((_tokenSet_15.member(LA(1)))) {
				mHEXDIGIT(false);
			}
			else {
				if ( _cnt5965>=1 ) { break _loop5965; } else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());}
			}
			
			_cnt5965++;
		} while (true);
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mOCTAL(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = OCTAL;
		int _saveIndex;
		
		match('0');
		{
		int _cnt5956=0;
		_loop5956:
		do {
			if (((LA(1) >= '0' && LA(1) <= '7'))) {
				mOCTALDIGIT(false);
			}
			else {
				if ( _cnt5956>=1 ) { break _loop5956; } else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());}
			}
			
			_cnt5956++;
		} while (true);
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mOCTALDIGIT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = OCTALDIGIT;
		int _saveIndex;
		
		{
		matchRange('0','7');
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mDECIMAL(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = DECIMAL;
		int _saveIndex;
		
		{
		mNONZERODIGIT(false);
		{
		_loop5960:
		do {
			if (((LA(1) >= '0' && LA(1) <= '9'))) {
				mDIGIT(false);
			}
			else {
				break _loop5960;
			}
			
		} while (true);
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mNONZERODIGIT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = NONZERODIGIT;
		int _saveIndex;
		
		{
		matchRange('1','9');
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	
	private static final long[] mk_tokenSet_0() {
		long[] data = new long[2044];
		data[0]=-864501557188624384L;
		data[1]=6341068274666569727L;
		data[2]=9007199254740992L;
		data[3]=-36028797027352577L;
		for (int i = 4; i<=5; i++) { data[i]=-1L; }
		data[6]=-576460752303423489L;
		data[7]=-1125899906844976L;
		for (int i = 8; i<=9; i++) { data[i]=-1L; }
		data[10]=281474975662079L;
		data[13]=4093490586303070208L;
		data[14]=-17179879616L;
		data[15]=-18014398509481985L;
		for (int i = 16; i<=17; i++) { data[i]=-1L; }
		data[18]=-1021L;
		data[19]=-1L;
		data[20]=-561850441793537L;
		data[21]=-8581545985L;
		data[22]=255L;
		data[66]=-4294967296L;
		data[67]=8383L;
		data[116]=17592186044415L;
		data[117]=-72066390130950144L;
		data[118]=134217727L;
		for (int i = 120; i<=123; i++) { data[i]=-1L; }
		data[124]=-3233808385L;
		data[125]=4611686017001275199L;
		data[126]=5755319944036155647L;
		data[127]=1142823585787613148L;
		data[132]=-999872439914595196L;
		data[133]=17376L;
		data[134]=24L;
		data[176]=-140737488355329L;
		data[177]=-3458764515968024577L;
		data[178]=-1L;
		data[179]=3509778554814463L;
		data[180]=36009005809663L;
		data[665]=70368744177663L;
		data[666]=16777215L;
		data[668]=-17179869184L;
		data[669]=-281474976710657L;
		data[670]=8791799068927L;
		data[671]=288230376151711744L;
		data[1004]=16253055L;
		data[1020]=576460743713488896L;
		data[1021]=134217726L;
		return data;
	}
	public static final BitSet _tokenSet_0 = new BitSet(mk_tokenSet_0());
	private static final long[] mk_tokenSet_1() {
		long[] data = new long[4088];
		data[2]=288234774198222848L;
		data[6]=576460752303423488L;
		data[7]=15L;
		data[10]=1048576L;
		data[23]=1979120929931264L;
		data[24]=-4294967296L;
		data[25]=-351843720886274L;
		data[26]=-1L;
		data[27]=-7205548297557114881L;
		data[28]=281474976514048L;
		data[29]=-8192L;
		data[30]=563224831328255L;
		data[31]=8796093021184L;
		data[32]=4194303L;
		data[33]=33554431L;
		data[34]=35171487186944L;
		data[36]=2594073385365405680L;
		data[37]=-73183476781613056L;
		data[38]=2577745637692514272L;
		data[39]=844440767840256L;
		data[40]=247132830528276448L;
		data[41]=7881300924956672L;
		data[42]=2589004636761079776L;
		data[43]=12884967424L;
		data[44]=2589004636760940512L;
		data[45]=562965791113216L;
		data[46]=288167810662516712L;
		data[47]=65536L;
		data[48]=2589567586714640352L;
		data[49]=12935233536L;
		data[50]=2589567586714640352L;
		data[51]=1688863818907648L;
		data[52]=2882303761516978144L;
		data[53]=-288230363266793472L;
		data[54]=3457638613854978016L;
		data[55]=127L;
		data[56]=3940649673949182L;
		data[57]=63L;
		data[58]=2309762420256548246L;
		data[59]=4026531871L;
		data[60]=1L;
		data[61]=35184372088575L;
		data[62]=7936L;
		data[64]=-9223363240761753601L;
		data[65]=-8514196127940608L;
		data[66]=16387L;
		data[67]=-1729382256910336000L;
		for (int i = 68; i<=72; i++) { data[i]=-1L; }
		data[73]=-3263218177L;
		data[74]=9168765891372858879L;
		data[75]=-8388803L;
		data[76]=-12713985L;
		data[77]=134217727L;
		data[78]=-4294901761L;
		data[79]=9007199254740991L;
		data[80]=-2L;
		for (int i = 81; i<=88; i++) { data[i]=-1L; }
		data[89]=-105553116266497L;
		data[90]=-4160749570L;
		data[91]=8796093022207L;
		data[92]=1125895612129279L;
		data[93]=527761286627327L;
		data[94]=4503599627370495L;
		data[95]=268435456L;
		data[96]=-4294967296L;
		data[97]=72057594037927927L;
		data[98]=-274877906944001L;
		data[99]=18014398509481983L;
		data[100]=536870911L;
		data[101]=8796093022142464L;
		data[102]=17592186044415L;
		data[103]=254L;
		data[104]=-4286578689L;
		data[105]=2097151L;
		data[108]=4503599627370464L;
		data[109]=4064L;
		data[110]=-288019261329244168L;
		data[111]=274877906943L;
		data[112]=68719476735L;
		data[113]=72057593970876416L;
		data[115]=28110114275721216L;
		data[132]=135107988821114880L;
		data[180]=-281474976710656L;
		data[181]=1099511627775L;
		data[182]=9187201948305063935L;
		data[183]=2139062143L;
		data[192]=1152921504606847040L;
		data[193]=-2L;
		data[194]=-6434062337L;
		data[195]=-8646911284551352321L;
		data[196]=-492581209243680L;
		data[197]=-1L;
		data[198]=576460748008488959L;
		data[199]=-281474976710656L;
		data[208]=1L;
		data[310]=9007199254740992L;
		data[312]=1L;
		data[639]=4096L;
		data[640]=-2097153L;
		for (int i = 641; i<=657; i++) { data[i]=-1L; }
		data[658]=8191L;
		data[659]=72057594037862400L;
		for (int i = 660; i<=663; i++) { data[i]=-1L; }
		data[664]=13198434439167L;
		data[665]=70368744177664L;
		data[666]=-4294967296L;
		data[667]=274877906943L;
		data[671]=-576460752303423488L;
		data[672]=34359736251L;
		data[673]=4503599627370495L;
		data[674]=4503599627370492L;
		data[675]=647392446434508800L;
		data[676]=-281200098804736L;
		data[677]=2305843004918726783L;
		data[678]=2251799813685232L;
		data[680]=2199023255551L;
		data[681]=323977693899001847L;
		data[682]=4495436853045886975L;
		data[683]=1134692107550725L;
		data[684]=140183445864062L;
		data[687]=34359738367L;
		data[688]=1L;
		data[862]=-281440616972288L;
		data[863]=1152921504606845055L;
		for (int i = 996; i<=1000; i++) { data[i]=-1L; }
		data[1001]=-211106232532993L;
		data[1002]=-1L;
		data[1003]=67108863L;
		data[1004]=6881498029988249600L;
		data[1005]=-37L;
		data[1006]=1125899906842623L;
		data[1007]=-524288L;
		for (int i = 1008; i<=1011; i++) { data[i]=-1L; }
		data[1012]=4611686018427387903L;
		data[1013]=-65536L;
		data[1014]=-196609L;
		data[1015]=1152640029630136575L;
		data[1017]=-9288674231451648L;
		data[1018]=-1L;
		data[1019]=2305843009213693951L;
		data[1021]=-281749854617600L;
		data[1022]=9223372033633550335L;
		data[1023]=486341884L;
		return data;
	}
	public static final BitSet _tokenSet_1 = new BitSet(mk_tokenSet_1());
	private static final long[] mk_tokenSet_2() {
		long[] data = new long[2042];
		data[1]=134217726L;
		data[3]=2139095039L;
		data[4]=-6172933889249159851L;
		data[5]=3122495741643543722L;
		data[6]=1274187559846268630L;
		data[7]=6184099063146390672L;
		data[8]=7783721355972007253L;
		data[9]=21882L;
		data[13]=19421773393035264L;
		data[14]=17575006099264L;
		data[15]=-1832871223351279616L;
		data[16]=281474976710655L;
		data[17]=6148914689804861440L;
		data[18]=6148914691236516865L;
		data[19]=6148914691236506283L;
		data[20]=-562583449545387L;
		data[21]=8388607L;
		data[66]=-4294967296L;
		data[67]=8383L;
		for (int i = 120; i<=121; i++) { data[i]=6148914691236517205L; }
		data[122]=6148914690880001365L;
		data[123]=6148914691236517205L;
		data[124]=-71777217515815168L;
		data[125]=280378317225728L;
		data[126]=1080863910568919040L;
		data[127]=1080897995681042176L;
		data[132]=-4607396478927292284L;
		data[133]=32L;
		data[134]=8L;
		data[176]=140737488355327L;
		data[177]=-4601013484258328576L;
		data[178]=6148914691236517205L;
		data[179]=1169903278445909L;
		data[665]=23456248059221L;
		data[666]=5592405L;
		data[668]=6148633210533183488L;
		data[669]=7638198793012598101L;
		data[670]=5862630697045L;
		data[1020]=576460743713488896L;
		return data;
	}
	public static final BitSet _tokenSet_2 = new BitSet(mk_tokenSet_2());
	private static final long[] mk_tokenSet_3() {
		long[] data = new long[2044];
		data[1]=576460743713488896L;
		data[2]=9007199254740992L;
		data[3]=-36028799166447616L;
		data[4]=6172933889249159850L;
		data[5]=-3122495741643543723L;
		data[6]=-1850648312149692119L;
		data[7]=-6185224963053235648L;
		data[8]=-7783721355972007254L;
		data[9]=-21883L;
		data[10]=281474975662079L;
		data[13]=4074068812910034944L;
		data[14]=-17592185978880L;
		data[15]=1814856824841797631L;
		data[16]=-281474976710656L;
		data[17]=-6148914689804861441L;
		data[18]=-6148914691236517886L;
		data[19]=-6148914691236506284L;
		data[20]=733007751850L;
		data[21]=-8589934592L;
		data[22]=255L;
		data[116]=17592186044415L;
		data[117]=-72066390130950144L;
		data[118]=134217727L;
		for (int i = 120; i<=121; i++) { data[i]=-6148914691236517206L; }
		data[122]=-6148914690880001366L;
		data[123]=-6148914691236517206L;
		data[124]=71777214282006783L;
		data[125]=4611405638684049471L;
		data[126]=4674456033467236607L;
		data[127]=61925590106570972L;
		data[132]=3607524039012697088L;
		data[133]=17344L;
		data[134]=16L;
		data[176]=-281474976710656L;
		data[177]=1142248968290303999L;
		data[178]=-6148914691236517206L;
		data[179]=2339875276368554L;
		data[180]=36009005809663L;
		data[665]=46912496118442L;
		data[666]=11184810L;
		data[668]=-6148633227713052672L;
		data[669]=-7638480267989308758L;
		data[670]=2929168371882L;
		data[671]=288230376151711744L;
		data[1004]=16253055L;
		data[1021]=134217726L;
		return data;
	}
	public static final BitSet _tokenSet_3 = new BitSet(mk_tokenSet_3());
	private static final long[] mk_tokenSet_4() {
		long[] data = new long[1333];
		data[1]=481036337264L;
		return data;
	}
	public static final BitSet _tokenSet_4 = new BitSet(mk_tokenSet_4());
	private static final long[] mk_tokenSet_5() {
		long[] data = new long[2664];
		for (int i = 0; i<=1; i++) { data[i]=-1L; }
		data[2]=297241973452963840L;
		data[3]=-36028797027352577L;
		for (int i = 4; i<=9; i++) { data[i]=-1L; }
		data[10]=281474976710655L;
		data[13]=4093490586303070208L;
		data[14]=-17179879616L;
		data[15]=-18014398509481985L;
		for (int i = 16; i<=17; i++) { data[i]=-1L; }
		data[18]=-1021L;
		data[19]=-1L;
		data[20]=-561850441793537L;
		data[21]=-8581545985L;
		data[22]=255L;
		data[23]=1979120929931264L;
		data[24]=-4294967296L;
		data[25]=-351843720886274L;
		data[26]=-1L;
		data[27]=-7205548297557114881L;
		data[28]=281474976514048L;
		data[29]=-8192L;
		data[30]=563224831328255L;
		data[31]=8796093021184L;
		data[32]=4194303L;
		data[33]=33554431L;
		data[34]=35171487186944L;
		data[36]=2594073385365405680L;
		data[37]=-73183476781613056L;
		data[38]=2577745637692514272L;
		data[39]=844440767840256L;
		data[40]=247132830528276448L;
		data[41]=7881300924956672L;
		data[42]=2589004636761079776L;
		data[43]=12884967424L;
		data[44]=2589004636760940512L;
		data[45]=562965791113216L;
		data[46]=288167810662516712L;
		data[47]=65536L;
		data[48]=2589567586714640352L;
		data[49]=12935233536L;
		data[50]=2589567586714640352L;
		data[51]=1688863818907648L;
		data[52]=2882303761516978144L;
		data[53]=-288230363266793472L;
		data[54]=3457638613854978016L;
		data[55]=127L;
		data[56]=3940649673949182L;
		data[57]=63L;
		data[58]=2309762420256548246L;
		data[59]=4026531871L;
		data[60]=1L;
		data[61]=35184372088575L;
		data[62]=7936L;
		data[64]=-9223363240761753601L;
		data[65]=-8514196127940608L;
		data[66]=-4294950909L;
		data[67]=-1729382256910327617L;
		for (int i = 68; i<=72; i++) { data[i]=-1L; }
		data[73]=-3263218177L;
		data[74]=9168765891372858879L;
		data[75]=-8388803L;
		data[76]=-12713985L;
		data[77]=134217727L;
		data[78]=-4294901761L;
		data[79]=9007199254740991L;
		data[80]=-2L;
		for (int i = 81; i<=88; i++) { data[i]=-1L; }
		data[89]=-105553116266497L;
		data[90]=-4160749570L;
		data[91]=501377302265855L;
		data[92]=1125895612129279L;
		data[93]=527761286627327L;
		data[94]=4503599627370495L;
		data[95]=268435456L;
		data[96]=-4294967296L;
		data[97]=72057594037927927L;
		data[98]=-274877906944001L;
		data[99]=18014398509481983L;
		data[100]=536870911L;
		data[101]=8796093022142464L;
		data[102]=17592186044415L;
		data[103]=254L;
		data[104]=-4286578689L;
		data[105]=2097151L;
		data[108]=4503599627370464L;
		data[109]=4064L;
		data[110]=-288019261329244168L;
		data[111]=274877906943L;
		data[112]=68719476735L;
		data[113]=72057593970876416L;
		data[115]=28110114275721216L;
		data[116]=17592186044415L;
		data[117]=-72066390130950144L;
		data[118]=134217727L;
		for (int i = 120; i<=123; i++) { data[i]=-1L; }
		data[124]=-3233808385L;
		data[125]=4611686017001275199L;
		data[126]=6908521828386340863L;
		data[127]=2295745090394464220L;
		data[132]=-864764451093480316L;
		data[133]=-4294949920L;
		data[134]=511L;
		data[176]=-140737488355329L;
		data[177]=-3458764515968024577L;
		data[178]=-1L;
		data[179]=3509778554814463L;
		data[180]=-245465970900993L;
		data[181]=1099511627775L;
		data[182]=9187201948305063935L;
		data[183]=2139062143L;
		data[192]=1657329052328919232L;
		data[193]=-2L;
		data[194]=-6434062337L;
		data[195]=-8646911284551352321L;
		data[196]=-492581209243680L;
		data[197]=-1L;
		data[198]=576460748008488959L;
		data[199]=-281474976710656L;
		data[208]=1L;
		data[310]=9007199254740992L;
		data[312]=1L;
		data[639]=4096L;
		data[640]=-2097153L;
		for (int i = 641; i<=657; i++) { data[i]=-1L; }
		data[658]=8191L;
		data[659]=72057594037862400L;
		for (int i = 660; i<=663; i++) { data[i]=-1L; }
		data[664]=13198434439167L;
		data[665]=140737488355327L;
		data[666]=-4278190081L;
		data[667]=281474976710655L;
		data[668]=-17179869184L;
		data[669]=-281474976710657L;
		data[670]=8791799068927L;
		data[671]=-288230376151711744L;
		data[672]=34359736251L;
		data[673]=4503599627370495L;
		data[674]=4503599627370492L;
		data[675]=647392446434508800L;
		data[676]=-281200098804736L;
		data[677]=2305843004918726783L;
		data[678]=2251799813685232L;
		data[680]=2199023255551L;
		data[681]=323977693899001847L;
		data[682]=4495436853045886975L;
		data[683]=1134692107550725L;
		data[684]=140183445864062L;
		data[687]=34359738367L;
		data[688]=1L;
		data[862]=-281440616972288L;
		data[863]=1152921504606845055L;
		for (int i = 996; i<=1000; i++) { data[i]=-1L; }
		data[1001]=-211106232532993L;
		data[1002]=-1L;
		data[1003]=67108863L;
		data[1004]=6881498030004502655L;
		data[1005]=-37L;
		data[1006]=1125899906842623L;
		data[1007]=-524288L;
		for (int i = 1008; i<=1011; i++) { data[i]=-1L; }
		data[1012]=4611686018427387903L;
		data[1013]=-65536L;
		data[1014]=-196609L;
		data[1015]=1152640029630136575L;
		data[1017]=-9288674231451648L;
		data[1018]=-1L;
		data[1019]=2305843009213693951L;
		data[1020]=576460743713488896L;
		data[1021]=-281749720399874L;
		data[1022]=9223372033633550335L;
		data[1023]=486341884L;
		return data;
	}
	public static final BitSet _tokenSet_5 = new BitSet(mk_tokenSet_5());
	private static final long[] mk_tokenSet_6() {
		long[] data = new long[2664];
		data[0]=-4398046520321L;
		data[1]=-1L;
		data[2]=297241973452963840L;
		data[3]=-36028797027352577L;
		for (int i = 4; i<=9; i++) { data[i]=-1L; }
		data[10]=281474976710655L;
		data[13]=4093490586303070208L;
		data[14]=-17179879616L;
		data[15]=-18014398509481985L;
		for (int i = 16; i<=17; i++) { data[i]=-1L; }
		data[18]=-1021L;
		data[19]=-1L;
		data[20]=-561850441793537L;
		data[21]=-8581545985L;
		data[22]=255L;
		data[23]=1979120929931264L;
		data[24]=-4294967296L;
		data[25]=-351843720886274L;
		data[26]=-1L;
		data[27]=-7205548297557114881L;
		data[28]=281474976514048L;
		data[29]=-8192L;
		data[30]=563224831328255L;
		data[31]=8796093021184L;
		data[32]=4194303L;
		data[33]=33554431L;
		data[34]=35171487186944L;
		data[36]=2594073385365405680L;
		data[37]=-73183476781613056L;
		data[38]=2577745637692514272L;
		data[39]=844440767840256L;
		data[40]=247132830528276448L;
		data[41]=7881300924956672L;
		data[42]=2589004636761079776L;
		data[43]=12884967424L;
		data[44]=2589004636760940512L;
		data[45]=562965791113216L;
		data[46]=288167810662516712L;
		data[47]=65536L;
		data[48]=2589567586714640352L;
		data[49]=12935233536L;
		data[50]=2589567586714640352L;
		data[51]=1688863818907648L;
		data[52]=2882303761516978144L;
		data[53]=-288230363266793472L;
		data[54]=3457638613854978016L;
		data[55]=127L;
		data[56]=3940649673949182L;
		data[57]=63L;
		data[58]=2309762420256548246L;
		data[59]=4026531871L;
		data[60]=1L;
		data[61]=35184372088575L;
		data[62]=7936L;
		data[64]=-9223363240761753601L;
		data[65]=-8514196127940608L;
		data[66]=-4294950909L;
		data[67]=-1729382256910327617L;
		for (int i = 68; i<=72; i++) { data[i]=-1L; }
		data[73]=-3263218177L;
		data[74]=9168765891372858879L;
		data[75]=-8388803L;
		data[76]=-12713985L;
		data[77]=134217727L;
		data[78]=-4294901761L;
		data[79]=9007199254740991L;
		data[80]=-2L;
		for (int i = 81; i<=88; i++) { data[i]=-1L; }
		data[89]=-105553116266497L;
		data[90]=-4160749570L;
		data[91]=501377302265855L;
		data[92]=1125895612129279L;
		data[93]=527761286627327L;
		data[94]=4503599627370495L;
		data[95]=268435456L;
		data[96]=-4294967296L;
		data[97]=72057594037927927L;
		data[98]=-274877906944001L;
		data[99]=18014398509481983L;
		data[100]=536870911L;
		data[101]=8796093022142464L;
		data[102]=17592186044415L;
		data[103]=254L;
		data[104]=-4286578689L;
		data[105]=2097151L;
		data[108]=4503599627370464L;
		data[109]=4064L;
		data[110]=-288019261329244168L;
		data[111]=274877906943L;
		data[112]=68719476735L;
		data[113]=72057593970876416L;
		data[115]=28110114275721216L;
		data[116]=17592186044415L;
		data[117]=-72066390130950144L;
		data[118]=134217727L;
		for (int i = 120; i<=123; i++) { data[i]=-1L; }
		data[124]=-3233808385L;
		data[125]=4611686017001275199L;
		data[126]=6908521828386340863L;
		data[127]=2295745090394464220L;
		data[132]=-864764451093480316L;
		data[133]=-4294949920L;
		data[134]=511L;
		data[176]=-140737488355329L;
		data[177]=-3458764515968024577L;
		data[178]=-1L;
		data[179]=3509778554814463L;
		data[180]=-245465970900993L;
		data[181]=1099511627775L;
		data[182]=9187201948305063935L;
		data[183]=2139062143L;
		data[192]=1657329052328919232L;
		data[193]=-2L;
		data[194]=-6434062337L;
		data[195]=-8646911284551352321L;
		data[196]=-492581209243680L;
		data[197]=-1L;
		data[198]=576460748008488959L;
		data[199]=-281474976710656L;
		data[208]=1L;
		data[310]=9007199254740992L;
		data[312]=1L;
		data[639]=4096L;
		data[640]=-2097153L;
		for (int i = 641; i<=657; i++) { data[i]=-1L; }
		data[658]=8191L;
		data[659]=72057594037862400L;
		for (int i = 660; i<=663; i++) { data[i]=-1L; }
		data[664]=13198434439167L;
		data[665]=140737488355327L;
		data[666]=-4278190081L;
		data[667]=281474976710655L;
		data[668]=-17179869184L;
		data[669]=-281474976710657L;
		data[670]=8791799068927L;
		data[671]=-288230376151711744L;
		data[672]=34359736251L;
		data[673]=4503599627370495L;
		data[674]=4503599627370492L;
		data[675]=647392446434508800L;
		data[676]=-281200098804736L;
		data[677]=2305843004918726783L;
		data[678]=2251799813685232L;
		data[680]=2199023255551L;
		data[681]=323977693899001847L;
		data[682]=4495436853045886975L;
		data[683]=1134692107550725L;
		data[684]=140183445864062L;
		data[687]=34359738367L;
		data[688]=1L;
		data[862]=-281440616972288L;
		data[863]=1152921504606845055L;
		for (int i = 996; i<=1000; i++) { data[i]=-1L; }
		data[1001]=-211106232532993L;
		data[1002]=-1L;
		data[1003]=67108863L;
		data[1004]=6881498030004502655L;
		data[1005]=-37L;
		data[1006]=1125899906842623L;
		data[1007]=-524288L;
		for (int i = 1008; i<=1011; i++) { data[i]=-1L; }
		data[1012]=4611686018427387903L;
		data[1013]=-65536L;
		data[1014]=-196609L;
		data[1015]=1152640029630136575L;
		data[1017]=-9288674231451648L;
		data[1018]=-1L;
		data[1019]=2305843009213693951L;
		data[1020]=576460743713488896L;
		data[1021]=-281749720399874L;
		data[1022]=9223372033633550335L;
		data[1023]=486341884L;
		return data;
	}
	public static final BitSet _tokenSet_6 = new BitSet(mk_tokenSet_6());
	private static final long[] mk_tokenSet_7() {
		long[] data = new long[2664];
		data[0]=-9217L;
		data[1]=-1L;
		data[2]=297241973452963840L;
		data[3]=-36028797027352577L;
		for (int i = 4; i<=9; i++) { data[i]=-1L; }
		data[10]=281474976710655L;
		data[13]=4093490586303070208L;
		data[14]=-17179879616L;
		data[15]=-18014398509481985L;
		for (int i = 16; i<=17; i++) { data[i]=-1L; }
		data[18]=-1021L;
		data[19]=-1L;
		data[20]=-561850441793537L;
		data[21]=-8581545985L;
		data[22]=255L;
		data[23]=1979120929931264L;
		data[24]=-4294967296L;
		data[25]=-351843720886274L;
		data[26]=-1L;
		data[27]=-7205548297557114881L;
		data[28]=281474976514048L;
		data[29]=-8192L;
		data[30]=563224831328255L;
		data[31]=8796093021184L;
		data[32]=4194303L;
		data[33]=33554431L;
		data[34]=35171487186944L;
		data[36]=2594073385365405680L;
		data[37]=-73183476781613056L;
		data[38]=2577745637692514272L;
		data[39]=844440767840256L;
		data[40]=247132830528276448L;
		data[41]=7881300924956672L;
		data[42]=2589004636761079776L;
		data[43]=12884967424L;
		data[44]=2589004636760940512L;
		data[45]=562965791113216L;
		data[46]=288167810662516712L;
		data[47]=65536L;
		data[48]=2589567586714640352L;
		data[49]=12935233536L;
		data[50]=2589567586714640352L;
		data[51]=1688863818907648L;
		data[52]=2882303761516978144L;
		data[53]=-288230363266793472L;
		data[54]=3457638613854978016L;
		data[55]=127L;
		data[56]=3940649673949182L;
		data[57]=63L;
		data[58]=2309762420256548246L;
		data[59]=4026531871L;
		data[60]=1L;
		data[61]=35184372088575L;
		data[62]=7936L;
		data[64]=-9223363240761753601L;
		data[65]=-8514196127940608L;
		data[66]=-4294950909L;
		data[67]=-1729382256910327617L;
		for (int i = 68; i<=72; i++) { data[i]=-1L; }
		data[73]=-3263218177L;
		data[74]=9168765891372858879L;
		data[75]=-8388803L;
		data[76]=-12713985L;
		data[77]=134217727L;
		data[78]=-4294901761L;
		data[79]=9007199254740991L;
		data[80]=-2L;
		for (int i = 81; i<=88; i++) { data[i]=-1L; }
		data[89]=-105553116266497L;
		data[90]=-4160749570L;
		data[91]=501377302265855L;
		data[92]=1125895612129279L;
		data[93]=527761286627327L;
		data[94]=4503599627370495L;
		data[95]=268435456L;
		data[96]=-4294967296L;
		data[97]=72057594037927927L;
		data[98]=-274877906944001L;
		data[99]=18014398509481983L;
		data[100]=536870911L;
		data[101]=8796093022142464L;
		data[102]=17592186044415L;
		data[103]=254L;
		data[104]=-4286578689L;
		data[105]=2097151L;
		data[108]=4503599627370464L;
		data[109]=4064L;
		data[110]=-288019261329244168L;
		data[111]=274877906943L;
		data[112]=68719476735L;
		data[113]=72057593970876416L;
		data[115]=28110114275721216L;
		data[116]=17592186044415L;
		data[117]=-72066390130950144L;
		data[118]=134217727L;
		for (int i = 120; i<=123; i++) { data[i]=-1L; }
		data[124]=-3233808385L;
		data[125]=4611686017001275199L;
		data[126]=6908521828386340863L;
		data[127]=2295745090394464220L;
		data[132]=-864764451093480316L;
		data[133]=-4294949920L;
		data[134]=511L;
		data[176]=-140737488355329L;
		data[177]=-3458764515968024577L;
		data[178]=-1L;
		data[179]=3509778554814463L;
		data[180]=-245465970900993L;
		data[181]=1099511627775L;
		data[182]=9187201948305063935L;
		data[183]=2139062143L;
		data[192]=1657329052328919232L;
		data[193]=-2L;
		data[194]=-6434062337L;
		data[195]=-8646911284551352321L;
		data[196]=-492581209243680L;
		data[197]=-1L;
		data[198]=576460748008488959L;
		data[199]=-281474976710656L;
		data[208]=1L;
		data[310]=9007199254740992L;
		data[312]=1L;
		data[639]=4096L;
		data[640]=-2097153L;
		for (int i = 641; i<=657; i++) { data[i]=-1L; }
		data[658]=8191L;
		data[659]=72057594037862400L;
		for (int i = 660; i<=663; i++) { data[i]=-1L; }
		data[664]=13198434439167L;
		data[665]=140737488355327L;
		data[666]=-4278190081L;
		data[667]=281474976710655L;
		data[668]=-17179869184L;
		data[669]=-281474976710657L;
		data[670]=8791799068927L;
		data[671]=-288230376151711744L;
		data[672]=34359736251L;
		data[673]=4503599627370495L;
		data[674]=4503599627370492L;
		data[675]=647392446434508800L;
		data[676]=-281200098804736L;
		data[677]=2305843004918726783L;
		data[678]=2251799813685232L;
		data[680]=2199023255551L;
		data[681]=323977693899001847L;
		data[682]=4495436853045886975L;
		data[683]=1134692107550725L;
		data[684]=140183445864062L;
		data[687]=34359738367L;
		data[688]=1L;
		data[862]=-281440616972288L;
		data[863]=1152921504606845055L;
		for (int i = 996; i<=1000; i++) { data[i]=-1L; }
		data[1001]=-211106232532993L;
		data[1002]=-1L;
		data[1003]=67108863L;
		data[1004]=6881498030004502655L;
		data[1005]=-37L;
		data[1006]=1125899906842623L;
		data[1007]=-524288L;
		for (int i = 1008; i<=1011; i++) { data[i]=-1L; }
		data[1012]=4611686018427387903L;
		data[1013]=-65536L;
		data[1014]=-196609L;
		data[1015]=1152640029630136575L;
		data[1017]=-9288674231451648L;
		data[1018]=-1L;
		data[1019]=2305843009213693951L;
		data[1020]=576460743713488896L;
		data[1021]=-281749720399874L;
		data[1022]=9223372033633550335L;
		data[1023]=486341884L;
		return data;
	}
	public static final BitSet _tokenSet_7 = new BitSet(mk_tokenSet_7());
	private static final long[] mk_tokenSet_8() {
		long[] data = new long[2044];
		data[0]=-864501557188624384L;
		data[1]=6341068270371602431L;
		data[2]=9007199254740992L;
		data[3]=-36028797027352577L;
		for (int i = 4; i<=5; i++) { data[i]=-1L; }
		data[6]=-576460752303423489L;
		data[7]=-1125899906844976L;
		for (int i = 8; i<=9; i++) { data[i]=-1L; }
		data[10]=281474975662079L;
		data[13]=4093490586303070208L;
		data[14]=-17179879616L;
		data[15]=-18014398509481985L;
		for (int i = 16; i<=17; i++) { data[i]=-1L; }
		data[18]=-1021L;
		data[19]=-1L;
		data[20]=-561850441793537L;
		data[21]=-8581545985L;
		data[22]=255L;
		data[66]=-4294967296L;
		data[67]=8383L;
		data[116]=17592186044415L;
		data[117]=-72066390130950144L;
		data[118]=134217727L;
		for (int i = 120; i<=123; i++) { data[i]=-1L; }
		data[124]=-3233808385L;
		data[125]=4611686017001275199L;
		data[126]=5755319944036155647L;
		data[127]=1142823585787613148L;
		data[132]=-999872439914595196L;
		data[133]=17376L;
		data[134]=24L;
		data[176]=-140737488355329L;
		data[177]=-3458764515968024577L;
		data[178]=-1L;
		data[179]=3509778554814463L;
		data[180]=36009005809663L;
		data[665]=70368744177663L;
		data[666]=16777215L;
		data[668]=-17179869184L;
		data[669]=-281474976710657L;
		data[670]=8791799068927L;
		data[671]=288230376151711744L;
		data[1004]=16253055L;
		data[1020]=576460743713488896L;
		data[1021]=134217726L;
		return data;
	}
	public static final BitSet _tokenSet_8 = new BitSet(mk_tokenSet_8());
	private static final long[] mk_tokenSet_9() {
		long[] data = new long[2044];
		data[1]=576460745995190270L;
		data[2]=9007199254740992L;
		data[3]=-36028797027352577L;
		for (int i = 4; i<=5; i++) { data[i]=-1L; }
		data[6]=-576460752303423489L;
		data[7]=-1125899906844976L;
		for (int i = 8; i<=9; i++) { data[i]=-1L; }
		data[10]=281474975662079L;
		data[13]=4093490586303070208L;
		data[14]=-17179879616L;
		data[15]=-18014398509481985L;
		for (int i = 16; i<=17; i++) { data[i]=-1L; }
		data[18]=-1021L;
		data[19]=-1L;
		data[20]=-561850441793537L;
		data[21]=-8581545985L;
		data[22]=255L;
		data[66]=-4294967296L;
		data[67]=8383L;
		data[116]=17592186044415L;
		data[117]=-72066390130950144L;
		data[118]=134217727L;
		for (int i = 120; i<=123; i++) { data[i]=-1L; }
		data[124]=-3233808385L;
		data[125]=4611686017001275199L;
		data[126]=5755319944036155647L;
		data[127]=1142823585787613148L;
		data[132]=-999872439914595196L;
		data[133]=17376L;
		data[134]=24L;
		data[176]=-140737488355329L;
		data[177]=-3458764515968024577L;
		data[178]=-1L;
		data[179]=3509778554814463L;
		data[180]=36009005809663L;
		data[665]=70368744177663L;
		data[666]=16777215L;
		data[668]=-17179869184L;
		data[669]=-281474976710657L;
		data[670]=8791799068927L;
		data[671]=288230376151711744L;
		data[1004]=16253055L;
		data[1020]=576460743713488896L;
		data[1021]=134217726L;
		return data;
	}
	public static final BitSet _tokenSet_9 = new BitSet(mk_tokenSet_9());
	private static final long[] mk_tokenSet_10() {
		long[] data = new long[1333];
		data[0]=-864501557188624384L;
		data[1]=5764607524376412161L;
		return data;
	}
	public static final BitSet _tokenSet_10 = new BitSet(mk_tokenSet_10());
	private static final long[] mk_tokenSet_11() {
		long[] data = new long[2664];
		data[0]=-17179869185L;
		data[1]=-268435457L;
		data[2]=297241973452963840L;
		data[3]=-36028797027352577L;
		for (int i = 4; i<=9; i++) { data[i]=-1L; }
		data[10]=281474976710655L;
		data[13]=4093490586303070208L;
		data[14]=-17179879616L;
		data[15]=-18014398509481985L;
		for (int i = 16; i<=17; i++) { data[i]=-1L; }
		data[18]=-1021L;
		data[19]=-1L;
		data[20]=-561850441793537L;
		data[21]=-8581545985L;
		data[22]=255L;
		data[23]=1979120929931264L;
		data[24]=-4294967296L;
		data[25]=-351843720886274L;
		data[26]=-1L;
		data[27]=-7205548297557114881L;
		data[28]=281474976514048L;
		data[29]=-8192L;
		data[30]=563224831328255L;
		data[31]=8796093021184L;
		data[32]=4194303L;
		data[33]=33554431L;
		data[34]=35171487186944L;
		data[36]=2594073385365405680L;
		data[37]=-73183476781613056L;
		data[38]=2577745637692514272L;
		data[39]=844440767840256L;
		data[40]=247132830528276448L;
		data[41]=7881300924956672L;
		data[42]=2589004636761079776L;
		data[43]=12884967424L;
		data[44]=2589004636760940512L;
		data[45]=562965791113216L;
		data[46]=288167810662516712L;
		data[47]=65536L;
		data[48]=2589567586714640352L;
		data[49]=12935233536L;
		data[50]=2589567586714640352L;
		data[51]=1688863818907648L;
		data[52]=2882303761516978144L;
		data[53]=-288230363266793472L;
		data[54]=3457638613854978016L;
		data[55]=127L;
		data[56]=3940649673949182L;
		data[57]=63L;
		data[58]=2309762420256548246L;
		data[59]=4026531871L;
		data[60]=1L;
		data[61]=35184372088575L;
		data[62]=7936L;
		data[64]=-9223363240761753601L;
		data[65]=-8514196127940608L;
		data[66]=-4294950909L;
		data[67]=-1729382256910327617L;
		for (int i = 68; i<=72; i++) { data[i]=-1L; }
		data[73]=-3263218177L;
		data[74]=9168765891372858879L;
		data[75]=-8388803L;
		data[76]=-12713985L;
		data[77]=134217727L;
		data[78]=-4294901761L;
		data[79]=9007199254740991L;
		data[80]=-2L;
		for (int i = 81; i<=88; i++) { data[i]=-1L; }
		data[89]=-105553116266497L;
		data[90]=-4160749570L;
		data[91]=501377302265855L;
		data[92]=1125895612129279L;
		data[93]=527761286627327L;
		data[94]=4503599627370495L;
		data[95]=268435456L;
		data[96]=-4294967296L;
		data[97]=72057594037927927L;
		data[98]=-274877906944001L;
		data[99]=18014398509481983L;
		data[100]=536870911L;
		data[101]=8796093022142464L;
		data[102]=17592186044415L;
		data[103]=254L;
		data[104]=-4286578689L;
		data[105]=2097151L;
		data[108]=4503599627370464L;
		data[109]=4064L;
		data[110]=-288019261329244168L;
		data[111]=274877906943L;
		data[112]=68719476735L;
		data[113]=72057593970876416L;
		data[115]=28110114275721216L;
		data[116]=17592186044415L;
		data[117]=-72066390130950144L;
		data[118]=134217727L;
		for (int i = 120; i<=123; i++) { data[i]=-1L; }
		data[124]=-3233808385L;
		data[125]=4611686017001275199L;
		data[126]=6908521828386340863L;
		data[127]=2295745090394464220L;
		data[132]=-864764451093480316L;
		data[133]=-4294949920L;
		data[134]=511L;
		data[176]=-140737488355329L;
		data[177]=-3458764515968024577L;
		data[178]=-1L;
		data[179]=3509778554814463L;
		data[180]=-245465970900993L;
		data[181]=1099511627775L;
		data[182]=9187201948305063935L;
		data[183]=2139062143L;
		data[192]=1657329052328919232L;
		data[193]=-2L;
		data[194]=-6434062337L;
		data[195]=-8646911284551352321L;
		data[196]=-492581209243680L;
		data[197]=-1L;
		data[198]=576460748008488959L;
		data[199]=-281474976710656L;
		data[208]=1L;
		data[310]=9007199254740992L;
		data[312]=1L;
		data[639]=4096L;
		data[640]=-2097153L;
		for (int i = 641; i<=657; i++) { data[i]=-1L; }
		data[658]=8191L;
		data[659]=72057594037862400L;
		for (int i = 660; i<=663; i++) { data[i]=-1L; }
		data[664]=13198434439167L;
		data[665]=140737488355327L;
		data[666]=-4278190081L;
		data[667]=281474976710655L;
		data[668]=-17179869184L;
		data[669]=-281474976710657L;
		data[670]=8791799068927L;
		data[671]=-288230376151711744L;
		data[672]=34359736251L;
		data[673]=4503599627370495L;
		data[674]=4503599627370492L;
		data[675]=647392446434508800L;
		data[676]=-281200098804736L;
		data[677]=2305843004918726783L;
		data[678]=2251799813685232L;
		data[680]=2199023255551L;
		data[681]=323977693899001847L;
		data[682]=4495436853045886975L;
		data[683]=1134692107550725L;
		data[684]=140183445864062L;
		data[687]=34359738367L;
		data[688]=1L;
		data[862]=-281440616972288L;
		data[863]=1152921504606845055L;
		for (int i = 996; i<=1000; i++) { data[i]=-1L; }
		data[1001]=-211106232532993L;
		data[1002]=-1L;
		data[1003]=67108863L;
		data[1004]=6881498030004502655L;
		data[1005]=-37L;
		data[1006]=1125899906842623L;
		data[1007]=-524288L;
		for (int i = 1008; i<=1011; i++) { data[i]=-1L; }
		data[1012]=4611686018427387903L;
		data[1013]=-65536L;
		data[1014]=-196609L;
		data[1015]=1152640029630136575L;
		data[1017]=-9288674231451648L;
		data[1018]=-1L;
		data[1019]=2305843009213693951L;
		data[1020]=576460743713488896L;
		data[1021]=-281749720399874L;
		data[1022]=9223372033633550335L;
		data[1023]=486341884L;
		return data;
	}
	public static final BitSet _tokenSet_11 = new BitSet(mk_tokenSet_11());
	private static final long[] mk_tokenSet_12() {
		long[] data = new long[1333];
		data[0]=566935683072L;
		data[1]=5700160604602368L;
		return data;
	}
	public static final BitSet _tokenSet_12 = new BitSet(mk_tokenSet_12());
	private static final long[] mk_tokenSet_13() {
		long[] data = new long[4088];
		data[1]=576460743847706622L;
		data[2]=297241973452963840L;
		data[3]=-36028797027352577L;
		for (int i = 4; i<=9; i++) { data[i]=-1L; }
		data[10]=281474976710655L;
		data[13]=4093490586303070208L;
		data[14]=-17179879616L;
		data[15]=-18014398509481985L;
		for (int i = 16; i<=17; i++) { data[i]=-1L; }
		data[18]=-1021L;
		data[19]=-1L;
		data[20]=-561850441793537L;
		data[21]=-8581545985L;
		data[22]=255L;
		data[23]=1979120929931264L;
		data[24]=-4294967296L;
		data[25]=-351843720886274L;
		data[26]=-1L;
		data[27]=-7205548297557114881L;
		data[28]=281474976514048L;
		data[29]=-8192L;
		data[30]=563224831328255L;
		data[31]=8796093021184L;
		data[32]=4194303L;
		data[33]=33554431L;
		data[34]=35171487186944L;
		data[36]=2594073385365405680L;
		data[37]=-73183476781613056L;
		data[38]=2577745637692514272L;
		data[39]=844440767840256L;
		data[40]=247132830528276448L;
		data[41]=7881300924956672L;
		data[42]=2589004636761079776L;
		data[43]=12884967424L;
		data[44]=2589004636760940512L;
		data[45]=562965791113216L;
		data[46]=288167810662516712L;
		data[47]=65536L;
		data[48]=2589567586714640352L;
		data[49]=12935233536L;
		data[50]=2589567586714640352L;
		data[51]=1688863818907648L;
		data[52]=2882303761516978144L;
		data[53]=-288230363266793472L;
		data[54]=3457638613854978016L;
		data[55]=127L;
		data[56]=3940649673949182L;
		data[57]=63L;
		data[58]=2309762420256548246L;
		data[59]=4026531871L;
		data[60]=1L;
		data[61]=35184372088575L;
		data[62]=7936L;
		data[64]=-9223363240761753601L;
		data[65]=-8514196127940608L;
		data[66]=-4294950909L;
		data[67]=-1729382256910327617L;
		for (int i = 68; i<=72; i++) { data[i]=-1L; }
		data[73]=-3263218177L;
		data[74]=9168765891372858879L;
		data[75]=-8388803L;
		data[76]=-12713985L;
		data[77]=134217727L;
		data[78]=-4294901761L;
		data[79]=9007199254740991L;
		data[80]=-2L;
		for (int i = 81; i<=88; i++) { data[i]=-1L; }
		data[89]=-105553116266497L;
		data[90]=-4160749570L;
		data[91]=501377302265855L;
		data[92]=1125895612129279L;
		data[93]=527761286627327L;
		data[94]=4503599627370495L;
		data[95]=268435456L;
		data[96]=-4294967296L;
		data[97]=72057594037927927L;
		data[98]=-274877906944001L;
		data[99]=18014398509481983L;
		data[100]=536870911L;
		data[101]=8796093022142464L;
		data[102]=17592186044415L;
		data[103]=254L;
		data[104]=-4286578689L;
		data[105]=2097151L;
		data[108]=4503599627370464L;
		data[109]=4064L;
		data[110]=-288019261329244168L;
		data[111]=274877906943L;
		data[112]=68719476735L;
		data[113]=72057593970876416L;
		data[115]=28110114275721216L;
		data[116]=17592186044415L;
		data[117]=-72066390130950144L;
		data[118]=134217727L;
		for (int i = 120; i<=123; i++) { data[i]=-1L; }
		data[124]=-3233808385L;
		data[125]=4611686017001275199L;
		data[126]=6908521828386340863L;
		data[127]=2295745090394464220L;
		data[132]=-864764451093480316L;
		data[133]=-4294949920L;
		data[134]=511L;
		data[176]=-140737488355329L;
		data[177]=-3458764515968024577L;
		data[178]=-1L;
		data[179]=3509778554814463L;
		data[180]=-245465970900993L;
		data[181]=1099511627775L;
		data[182]=9187201948305063935L;
		data[183]=2139062143L;
		data[192]=1657329052328919232L;
		data[193]=-2L;
		data[194]=-6434062337L;
		data[195]=-8646911284551352321L;
		data[196]=-492581209243680L;
		data[197]=-1L;
		data[198]=576460748008488959L;
		data[199]=-281474976710656L;
		data[208]=1L;
		data[310]=9007199254740992L;
		data[312]=1L;
		data[639]=4096L;
		data[640]=-2097153L;
		for (int i = 641; i<=657; i++) { data[i]=-1L; }
		data[658]=8191L;
		data[659]=72057594037862400L;
		for (int i = 660; i<=663; i++) { data[i]=-1L; }
		data[664]=13198434439167L;
		data[665]=140737488355327L;
		data[666]=-4278190081L;
		data[667]=281474976710655L;
		data[668]=-17179869184L;
		data[669]=-281474976710657L;
		data[670]=8791799068927L;
		data[671]=-288230376151711744L;
		data[672]=34359736251L;
		data[673]=4503599627370495L;
		data[674]=4503599627370492L;
		data[675]=647392446434508800L;
		data[676]=-281200098804736L;
		data[677]=2305843004918726783L;
		data[678]=2251799813685232L;
		data[680]=2199023255551L;
		data[681]=323977693899001847L;
		data[682]=4495436853045886975L;
		data[683]=1134692107550725L;
		data[684]=140183445864062L;
		data[687]=34359738367L;
		data[688]=1L;
		data[862]=-281440616972288L;
		data[863]=1152921504606845055L;
		for (int i = 996; i<=1000; i++) { data[i]=-1L; }
		data[1001]=-211106232532993L;
		data[1002]=-1L;
		data[1003]=67108863L;
		data[1004]=6881498030004502655L;
		data[1005]=-37L;
		data[1006]=1125899906842623L;
		data[1007]=-524288L;
		for (int i = 1008; i<=1011; i++) { data[i]=-1L; }
		data[1012]=4611686018427387903L;
		data[1013]=-65536L;
		data[1014]=-196609L;
		data[1015]=1152640029630136575L;
		data[1017]=-9288674231451648L;
		data[1018]=-1L;
		data[1019]=2305843009213693951L;
		data[1020]=576460743713488896L;
		data[1021]=-281749720399874L;
		data[1022]=9223372033633550335L;
		data[1023]=486341884L;
		return data;
	}
	public static final BitSet _tokenSet_13 = new BitSet(mk_tokenSet_13());
	private static final long[] mk_tokenSet_14() {
		long[] data = new long[1333];
		data[1]=343597383760L;
		return data;
	}
	public static final BitSet _tokenSet_14 = new BitSet(mk_tokenSet_14());
	private static final long[] mk_tokenSet_15() {
		long[] data = new long[1333];
		data[0]=287948901175001088L;
		data[1]=541165879422L;
		return data;
	}
	public static final BitSet _tokenSet_15 = new BitSet(mk_tokenSet_15());
	
	}
